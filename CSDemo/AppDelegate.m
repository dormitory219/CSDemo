//
//  AppDelegate.m
//  CSAppDelegateModule
//
//  Created by 余强 on 2018/5/10.
//  Copyright © 2018年 余强. All rights reserved.
//

#import "AppDelegate.h"
#import "CSGroundAPI.h"
#import "CSAppDelegateModuleAPI.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *) application didFinishLaunchingWithOptions:(NSDictionary *) launchOptions
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    return   [service application:application didFinishLaunchingWithOptions:launchOptions];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    return  [service application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}
#pragma clang diagnostic pop

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray * __nullable restorableObjects))restorationHandler
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    return  [service application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}

- (void)application:(UIApplication *) application performActionForShortcutItem:(UIApplicationShortcutItem *) shortcutItem completionHandler:(void (^)(BOOL succeeded)) completionHandler
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service application:application performActionForShortcutItem:shortcutItem completionHandler:completionHandler];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (void)application:(UIApplication *) application didReceiveRemoteNotification:(NSDictionary *) userInfo
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service application:application didReceiveRemoteNotification:userInfo];
}
#pragma clang diagnostic pop

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)applicationDidBecomeActive:(UIApplication *) application
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service applicationDidBecomeActive:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service applicationWillEnterForeground:application];
}

- (void)applicationWillResignActive:(UIApplication *) application
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service applicationWillResignActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service applicationDidEnterBackground:application];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  id<CSAppDelegateProxyProtocol> service =  [CSMediator serviceForProtocol:@protocol(CSAppDelegateProxyProtocol)];
    [service applicationWillTerminate:application];
}


@end
