//
//  CSGroupCommonDefine.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/6.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSGroupCommonDefine_h
#define CSGroupCommonDefine_h

#import "CSCommonDefine.h"

#define MAIN_THEME_COLOR UIColorFromRGB(0x1e2939)

#define SECOND_THEME_COLOR UIColorFromRGB(0x25c4a4)

#endif /* CSGroupCommonDefine_h */
