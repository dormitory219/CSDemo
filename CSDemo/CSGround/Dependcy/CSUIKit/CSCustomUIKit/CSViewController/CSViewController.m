//
//  CSViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSViewController.h"
#import "CSGroundAPI.h"

@interface CSViewController ()

@end

@implementation CSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = MAIN_THEME_COLOR;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
