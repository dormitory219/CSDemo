//
//  NSObject+Context.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/24.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSContext.h"

@interface NSObject (Context)

@property (nonatomic,strong) CSContext *context;

@end
