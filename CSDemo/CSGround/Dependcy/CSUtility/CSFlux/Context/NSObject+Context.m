//
//  NSObject+Context.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/24.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "NSObject+Context.h"
#import <objc/runtime.h>

@implementation NSObject (Context)

static char *contextKey;

- (void)setContext:(CSContext *)context
{
    objc_setAssociatedObject(self, contextKey, context, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CSContext *)context
{
    return objc_getAssociatedObject(self, contextKey);
}

@end
