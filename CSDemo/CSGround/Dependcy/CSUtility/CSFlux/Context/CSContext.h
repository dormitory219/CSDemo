//
//  CSContext.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/24.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 上下文背景对象，用于类似单例去管理模块共享的对象，比如store,业务视图view等
 */
@interface CSContext : NSObject

@end
