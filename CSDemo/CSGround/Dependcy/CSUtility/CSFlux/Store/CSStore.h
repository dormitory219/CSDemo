//
//  CSStore.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 类似flux单向数据流的设计模式，用来全局状态管理，但不一定是标准的单项数据流方案如action,dispatch等，可能只做状态的维护，无视图刷新，绑定机制
 */

@interface CSStore : NSObject

+ (instancetype)store;

@end
