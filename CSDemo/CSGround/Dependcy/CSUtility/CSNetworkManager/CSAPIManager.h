//
//  CSAPIManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/7.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSAPIManager : NSObject

+ (void)cancelRequest:(NSString *)identifier;

@end
