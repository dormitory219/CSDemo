//
//  CSAPIManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/7.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSAPIManager.h"
#import "CSFoundation.h"

@implementation CSAPIManager

+ (void)cancelRequest:(NSString *)identifier
{
    [XMCenter cancelRequest:identifier onCancel:^(XMRequest *request) {
        NSLog(@"onCancel:%@",identifier);
    }];
}

@end
