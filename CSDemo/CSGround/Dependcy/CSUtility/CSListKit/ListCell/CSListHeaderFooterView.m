//
//  CSListHeaderFooterView.m
//  CSListKit
//
//  Created by joy_yu on 2018/4/13.
//

#import "CSListHeaderFooterView.h"

@implementation CSListHeaderFooterView

- (void)awakeFromNib
{
    [super awakeFromNib];
}

+ (NSString *)headerReuseIdentifier
{
    return [NSString stringWithFormat:@"%@_%@",NSStringFromClass([self class]),@"header"];
}

+ (NSString *)footerReuseIdentifier
{
    return [NSString stringWithFormat:@"%@_%@",NSStringFromClass([self class]),@"footer"];
}

+ (instancetype)headerFooterViewWithCollectionView:(__kindof UICollectionView *)collectionView
                                         atIndex:(NSInteger)index
                        supplementaryElementOfKind:(NSString *)elementKind
{
    NSString *reuseIdentifier;
    if ([elementKind isEqualToString: UICollectionElementKindSectionHeader])
    {
        reuseIdentifier = [self headerReuseIdentifier];
    }
    else
    {
        reuseIdentifier = [self footerReuseIdentifier];
    }
    CSListHeaderFooterView *view =  (CSListHeaderFooterView *)[collectionView dequeueReusableSupplementaryViewOfKind :elementKind  withReuseIdentifier:reuseIdentifier forIndexPath:[NSIndexPath indexPathForItem:0 inSection:index]];
    view.collectionView = collectionView;
    return view;
}

@end
