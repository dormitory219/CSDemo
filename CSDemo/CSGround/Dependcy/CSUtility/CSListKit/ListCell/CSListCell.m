//
//  CSListCell.m
//  CSListKit
//
//  Created by joy_yu on 2018/4/13.
//

#import "CSListCell.h"

@implementation CSListCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

+ (NSString *)reuseIdentifier
{
    return NSStringFromClass([self class]);
}

+ (instancetype)cellWithCollectionView:(__kindof UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath
{
    CSListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self reuseIdentifier] forIndexPath:indexPath];
    cell.collectionView = collectionView;
    return cell;
}

@end
