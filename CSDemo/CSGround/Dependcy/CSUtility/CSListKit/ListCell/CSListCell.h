//
//  CSListCell.h
//  CSListKit
//
//  Created by joy_yu on 2018/4/13.
//

#import <UIKit/UIKit.h>

@interface CSListCell : UICollectionViewCell

@property (nonatomic,weak) UICollectionView *collectionView;

@property (nonatomic,strong) id data;

+ (instancetype)cellWithCollectionView:(__kindof UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

+ (NSString *)reuseIdentifier;

@end
