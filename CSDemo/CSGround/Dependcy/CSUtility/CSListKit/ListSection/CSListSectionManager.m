//
//  CSAdapterManager.m
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/12.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSListSectionManager.h"
#import "CSListSection.h"
#import "CSListAdapterProxy.h"

@interface CSListSectionManager() <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong, nullable) CSListAdapterProxy *delegateProxy;

@end

@implementation CSListSectionManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

#pragma mark -delegate/dataSource propertySet
- (void)setCollectionView:(UICollectionView *)collectionView
{
    if (_collectionView != collectionView)
    {
        _collectionView = collectionView;
        //bind delegate and dataSource
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        //set listSection collection/controller
        if (self.sectionMap.count)
        {
            for (CSListSection *section in self.sectionMap)
            {
                section.collectionView = collectionView;
                section.viewController = self.viewController;
            }
        }
    }
}

// reset and configure the delegate proxy whenever this property is set
- (void)setCollectionViewDelegate:(id<UICollectionViewDelegate>)collectionViewDelegate
{
    if (_collectionViewDelegate != collectionViewDelegate)
    {
        _collectionViewDelegate = collectionViewDelegate;
        [self createProxyAndUpdateCollectionViewDelegate];
    }
}

- (void)setScrollViewDelegate:(id<UIScrollViewDelegate>)scrollViewDelegate
{
    if (_scrollViewDelegate != scrollViewDelegate)
    {
        _scrollViewDelegate = scrollViewDelegate;
        [self createProxyAndUpdateCollectionViewDelegate];
    }
}

- (void)createProxyAndUpdateCollectionViewDelegate
{
    _collectionView.delegate = nil;
    self.delegateProxy = [[CSListAdapterProxy alloc] initWithCollectionViewTarget:_collectionViewDelegate
                                                                 scrollViewTarget:_scrollViewDelegate
                                                                      interceptor:self];
    _collectionView.delegate = (id<UICollectionViewDelegate>)self.delegateProxy ?: self;
}

#pragma mark - viewController
- (UIViewController *)viewController
{
    for (UIView* next = [self.collectionView superview]; next; next = next.superview)
    {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController *)nextResponder;
        }
    }
    if (_viewController) {
        return _viewController;
    }else{
        return nil;
    }
}


#pragma mark - add/remove listSection
- (void)addListSection:(__kindof CSListSection *)listSection
{
    if (listSection == nil)
    {
        return;
    }
    if ([self containListSection:listSection])
    {
        return;
    }
    [self.sectionMap addObject:listSection];
}

- (void)removeListSection:(__kindof CSListSection *)listSection
{
    if (listSection == nil)
    {
        return;
    }
    if ([self containListSection:listSection])
    {
        return;
    }
    [self.sectionMap removeObject:listSection];
}

- (BOOL)containListSection:(__kindof CSListSection *)listSection
{
    if (listSection == nil)
    {
        NO;
    }
    if ([self.sectionMap containsObject:listSection])
    {
        return YES;
    }
    return NO;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.sectionMap.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    if (listSection && [listSection respondsToSelector:@selector(numberOfItems)])
    {
        return [listSection numberOfItems];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(cellForItemAtIndexPath:)])
    {
        return [listSection cellForItemAtIndexPath:indexPath];
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(viewForSupplementaryElementOfKind:atIndex:)])
    {
        return [listSection viewForSupplementaryElementOfKind:kind atIndex:indexPath.section];
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    id<UICollectionViewDelegate> collectionViewDelegate = self.collectionViewDelegate;
    if ([collectionViewDelegate respondsToSelector:@selector(collectionView:didSelectItemAtIndexPath:)]) {
        [collectionViewDelegate collectionView:collectionView didSelectItemAtIndexPath:indexPath];
    }
   __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(didSelectItemAtIndex:)])
    {
        [listSection didSelectItemAtIndex:indexPath.item];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    id<UICollectionViewDelegate> collectionViewDelegate = self.collectionViewDelegate;
    if ([collectionViewDelegate respondsToSelector:@selector(collectionView:didDeselectItemAtIndexPath:)]) {
        [collectionViewDelegate collectionView:collectionView didDeselectItemAtIndexPath:indexPath];
    }
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(didDeselectItemAtIndex:)])
    {
        [listSection didDeselectItemAtIndex:indexPath.item];
    }
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
  
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
   
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(UICollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{

}



- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(didHighlightItemAtIndex:)])
    {
        [listSection didHighlightItemAtIndex:indexPath.item];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(didUnhighlightItemAtIndex:)])
    {
       [listSection didUnhighlightItemAtIndex:indexPath.item];
    }
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListSection *listSection = self.sectionMap[indexPath.section];
    if (listSection && [listSection respondsToSelector:@selector(sizeForItemAtIndex:)])
    {
        return [listSection sizeForItemAtIndex:indexPath.item];
    }
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    UIEdgeInsets inset = [listSection inset];
    return inset;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    CGFloat minimumLineSpacing = [listSection minimumLineSpacing];
    return minimumLineSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    CGFloat minimumInteritemSpacing = [listSection minimumInteritemSpacing];
    return minimumInteritemSpacing;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    if (listSection && [listSection respondsToSelector:@selector(sizeForSupplementaryViewOfKind:atIndex:)])
    {
        return [listSection sizeForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndex:section];
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    __kindof CSListSection *listSection = self.sectionMap[section];
    if (listSection && [listSection respondsToSelector:@selector(sizeForSupplementaryViewOfKind:atIndex:)])
    {
       return [listSection sizeForSupplementaryViewOfKind:UICollectionElementKindSectionFooter atIndex:section];
    }
    return CGSizeZero;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // forward this method to the delegate b/c this implementation will steal the message from the proxy
    id<UIScrollViewDelegate> scrollViewDelegate = self.scrollViewDelegate;
    if ([scrollViewDelegate respondsToSelector:@selector(scrollViewDidScroll:)])
    {
        [scrollViewDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    // forward this method to the delegate b/c this implementation will steal the message from the proxy
    id<UIScrollViewDelegate> scrollViewDelegate = self.scrollViewDelegate;
    if ([scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)])
    {
        [scrollViewDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    // forward this method to the delegate b/c this implementation will steal the message from the proxy
    id<UIScrollViewDelegate> scrollViewDelegate = self.scrollViewDelegate;
    if ([scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)])
    {
        [scrollViewDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (NSMutableArray *)sectionMap
{
    if (!_sectionMap)
    {
        _sectionMap = [[NSMutableArray alloc] init];
    }
    return _sectionMap;
}

@end
