//
//  CSListAdapter.m
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/12.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSListSection.h"
#import "CSListCell.h"
#import "CSListHeaderFooterView.h"

@interface CSListSection()

@end

@implementation CSListSection

+ (instancetype)sectionWithData:(id)data
{
    CSListSection *section = [[self alloc] init];
    section.items = data;
    return section;
}

- (instancetype)init
{
    if (self = [super init])
    {
        self.inset = UIEdgeInsetsZero;
        self.minimumLineSpacing = 0.f;
        self.minimumInteritemSpacing = 0.f;
    }
    return self;
}

//setCollectionView and register cell,header,footer
- (void)setCollectionView:(UICollectionView *)collectionView
{
    if (_collectionView != collectionView)
    {
        _collectionView = collectionView;
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSListCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([CSListCell class])];
        [collectionView registerClass:[CSListHeaderFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([CSListHeaderFooterView class])];
        [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([CSListHeaderFooterView class])];
    }
}

- (NSInteger)numberOfItems
{
    return self.items.count;
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CSListCell *cell = [CSListCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    cell.data = self.items[indexPath.item];
    return cell;
}

- (__kindof UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)elementKind
                                                                 atIndex:(NSInteger)index
{
    CSListHeaderFooterView *view =  [CSListHeaderFooterView headerFooterViewWithCollectionView:self.collectionView atIndex:index supplementaryElementOfKind:elementKind];
    if ([elementKind isEqualToString:UICollectionElementKindSectionHeader])
    {
      //headerView doStn
    }
    else if ([elementKind isEqualToString:UICollectionElementKindSectionFooter])
    {
      //footerView doStn
    }
    return view;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    // NSLog(@"didSelectItem at index:%ld,section:%ld",index,self.section);
}

- (void)didDeselectItemAtIndex:(NSInteger)index
{
   // NSLog(@"didDeselectItem at index:%ld,section:%ld",index,self.section);
}

- (void)didHighlightItemAtIndex:(NSInteger)index
{
      //NSLog(@"didHighlightItem at index:%ld,section:%ld",index,self.section);
}

- (void)didUnhighlightItemAtIndex:(NSInteger)index
{
     // NSLog(@"didUnhighlightItem:%ld,section:%ld",index,self.section);
}

@end
