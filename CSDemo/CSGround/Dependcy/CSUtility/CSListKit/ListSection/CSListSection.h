//
//  CSListAdapter.h
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/12.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CSAdapterManager;
@protocol CSListSectionProtocol

@required

- (NSInteger)numberOfItems;

- (CGSize)sizeForItemAtIndex:(NSInteger)index;

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (NSArray<NSString *> *)supportedElementKinds;

- (__kindof UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)elementKind
                                                                 atIndex:(NSInteger)index;

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index;


- (void)didSelectItemAtIndex:(NSInteger)index;

- (void)didDeselectItemAtIndex:(NSInteger)index;

- (void)didHighlightItemAtIndex:(NSInteger)index;

- (void)didUnhighlightItemAtIndex:(NSInteger)index;


@end

@interface CSListSection : NSObject<CSListSectionProtocol>

@property (nonatomic,assign) NSInteger section;

@property (nonatomic,weak) UIViewController *viewController;

@property (nonatomic,strong) NSMutableArray *items;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign) UIEdgeInsets inset;

@property (nonatomic, assign) CGFloat minimumLineSpacing;

@property (nonatomic, assign) CGFloat minimumInteritemSpacing;

+ (instancetype)sectionWithData:(id)data;

@end
