//
//  CSCommon.h
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSCommon_h
#define CSCommon_h

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define UIColorFromRGB(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]


#endif /* CSCommon_h */
