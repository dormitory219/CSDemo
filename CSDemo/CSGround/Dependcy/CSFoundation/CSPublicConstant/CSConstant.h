//
//  CSConstant.h
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const ApplicationDidBecomeActive;

@interface CSConstant : NSObject

@end
