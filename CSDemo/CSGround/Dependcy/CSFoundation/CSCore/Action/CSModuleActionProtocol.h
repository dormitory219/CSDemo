//
//  CSModuleActionProtocol.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2019/3/13.
//  Copyright © 2019 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSModuleActionProtocol_h
#define CSModuleActionProtocol_h

@class CSTransmissionData;
@protocol CSModuleActionProtocol <NSObject>

@property (nonatomic,assign) NSUInteger APIType;
@property (nonatomic,copy) void (^callBack)(CSTransmissionData *data);

@end

#endif /* CSModuleActionProtocol_h */
