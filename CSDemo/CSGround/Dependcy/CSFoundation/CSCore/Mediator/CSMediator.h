//
//  CSMediator.h
//  CSMediator_Example
//
//  Created by joy_yu on 2018/4/7.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSRouter.h"
#import "CSServiceManager.h"
#import "CSDynamicRouter.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMediator : NSObject

@end

@interface CSMediator(Router)

//register
+ (void)registerNativePath:(NSString *)path jump:(CSJumpRouteHandler)handler;

+ (void)registerNativePath:(NSString *)path view:(CSViewRouteHandler)handler;

+ (void)registerWebPathWithJump:(CSJumpRouteHandler)handler;

+ (void)registerWebPathWithView:(CSViewRouteHandler)handler;

+ (void)registerRNPathWithJump:(CSJumpRouteHandler)handler;

+ (void)registerRNPathWithView:(CSViewRouteHandler)handler;

//jump
+ (void)jumpTo:(NSString *)url fromViewController:(UIViewController *)vc;

+ (UIViewController *)viewForURL:(NSString *)url;

+ (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc;

+ (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data;

+ (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc jumpCallbackHandler:(CSJumpCallbackHandler)handler;

+ (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data viewRouteCallbackHandler:(CSViewRouteHandler)handler;

@end

@interface CSMediator(Service)

+ (void)registerService:(Protocol *)proto withImpl:(id)impl;

+ (id)serviceForProtocol:(Protocol *)serviceProtocol;

@end

NS_ASSUME_NONNULL_END
