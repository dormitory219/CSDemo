//
//  CSDynamicRouter.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSDynamicRouter : NSObject

+ (instancetype)router;

- (void)setupDynamicSetting:(NSDictionary *)setting;

@end
