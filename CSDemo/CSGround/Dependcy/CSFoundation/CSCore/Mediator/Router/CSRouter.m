//
//  CSRouter.m
//  CSMediator_Example
//
//  Created by joy_yu on 2018/4/7.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSRouter.h"
#import "CSRouteRequest.h"

static NSMutableDictionary <NSString *, id<CSRouterModuleProtocol>> *routerModuleMap = nil;

NSString *const webRouter = @"web";
NSString *const nativeRouter = @"native";
NSString *const rnRouter = @"rn";

NSString *const router = @"route";

@interface CSRouter()

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSJumpRouteHandler> *nativeJumpRouterMap;

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSJumpRouteHandler> *webJumpRouterMap;

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSJumpRouteHandler> *rnJumpRouterMap;

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSViewRouteHandler> *nativeJumpViewRouterMap;

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSViewRouteHandler> *webJumpViewRouterMap;

@property (nonatomic,strong) NSMutableDictionary <NSString *, CSViewRouteHandler> *rnJumpViewRouterMap;

@property (nonatomic,copy) CSDynamicRouteSettingBlock dynamicRouteSetting;

@end

@implementation CSRouter

- (instancetype)init
{
    self = [super init];
    if (self) {
        _nativeJumpRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
        _webJumpRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
        _rnJumpRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
        _nativeJumpViewRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
        _webJumpViewRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
        _rnJumpViewRouterMap = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return self;
}

+ (nonnull instancetype)router
{
    static CSRouter *router = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[self alloc] init];
    });
    return router;
}

- (void)registerNativePath:(NSString *)path jump:(CSJumpRouteHandler)handler
{
    NSParameterAssert([path isKindOfClass:[NSString class]]);
    NSParameterAssert(handler);
    NSAssert(![self.nativeJumpRouterMap objectForKey:path], @"当前路径 %@ 已被注册！",path);
    [self.nativeJumpRouterMap setObject:handler forKey:path];
}

- (void)registerNativePath:(NSString *)path view:(CSViewRouteHandler)handler
{
    NSParameterAssert([path isKindOfClass:[NSString class]]);
    NSParameterAssert(handler);
    NSAssert(![self.nativeJumpViewRouterMap objectForKey:path], @"当前路径 %@ 已被注册！",path);
    [self.nativeJumpViewRouterMap setObject:handler forKey:path];
}

- (void)registerWebPathWithJump:(CSJumpRouteHandler)handler
{
    [self.webJumpRouterMap setObject:handler forKey:webRouter];
}

- (void)registerWebPathWithView:(CSViewRouteHandler)handler
{
    [self.webJumpViewRouterMap setObject:handler forKey:webRouter];
}

- (void)registerRNPathWithJump:(CSJumpRouteHandler)handler
{
    [self.rnJumpRouterMap setObject:handler forKey:rnRouter];
}

- (void)registerRNPathWithView:(CSViewRouteHandler)handler
{
    [self.rnJumpViewRouterMap setObject:handler forKey:rnRouter];
}

- (void)jumpTo:(NSString *)url fromViewController:(UIViewController *)vc
{
    [self jumpTo:url parameter:nil fromViewController:vc];
}

- (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc
{
    [self jumpTo:url parameter:data fromViewController:vc jumpCallbackHandler:nil];
}

- (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc jumpCallbackHandler:(CSJumpCallbackHandler)handler
{
    NSParameterAssert([url isKindOfClass:[NSString class]]);
    NSParameterAssert([vc isKindOfClass:[UIViewController class]]);
    NSParameterAssert(!data || [data isKindOfClass:[CSTransmissionData class]]);

    CSRouteRequest *request = [CSRouteRequest requestWithURL:url parameter:data callback:handler fromVC:vc];
    
    //dynamatic set
    _dynamicRouteSetting ? _dynamicRouteSetting(request) : nil;
    
    if (_dynamicRouteSetting) {
        request.dynamicSetting = YES;
    }else{
        request.dynamicSetting = NO;
    }
    //handler request
    [self jumpToRequest:request];
}

- (void)jumpToRequest:(CSRouteRequest *)request
{
    CSRouteType type = request.routeType;
    NSDictionary <NSString *, CSJumpRouteHandler> *jumpRouteMap = nil;
    CSJumpRouteHandler jumpHandler  = nil;
    switch (type ) {
        case CSRouteTypeRN:
            jumpRouteMap = _rnJumpRouterMap;
            jumpHandler = jumpRouteMap[rnRouter];
            break;
        case CSRouteTypeWeb:
            jumpRouteMap = _webJumpRouterMap;
            jumpHandler = jumpRouteMap[webRouter];
            break;
        case CSRouteTypeNative:
        {
            jumpRouteMap = _nativeJumpRouterMap;
            jumpHandler = jumpRouteMap[request.path];
        }
            break;
        default:
            break;
    }
    jumpHandler ? jumpHandler(request) : nil;
}


- (UIViewController *)viewForURL:(NSString *)url
{
   return  [self viewForURL:url parameter:nil];
}

- (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data
{
    return [self viewForURL:url parameter:data viewRouteCallbackHandler:nil];
}

- (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data viewRouteCallbackHandler:(CSViewRouteHandler)handler
{
    NSParameterAssert([url isKindOfClass:[NSString class]]);
    NSParameterAssert(!data || [data isKindOfClass:[CSTransmissionData class]]);
    
    CSRouteRequest *request = [CSRouteRequest requestWithURL:url parameter:data callback:nil fromVC:nil];
    
    _dynamicRouteSetting ? _dynamicRouteSetting(request) : nil;
    if (_dynamicRouteSetting) {
        request.dynamicSetting = YES;
    }else{
        request.dynamicSetting = NO;
    }
    return [self viewToRequest:request];
}

- (UIViewController *)viewToRequest:(CSRouteRequest *)request
{
    CSRouteType type = request.routeType;
    NSDictionary <NSString *, CSViewRouteHandler> *viewRouteMap = nil;
    CSViewRouteHandler viewRouteHandler  = nil;
    switch (type ) {
        case CSRouteTypeRN:
            viewRouteMap = _rnJumpViewRouterMap;
            viewRouteHandler = viewRouteMap[rnRouter];
            break;
        case CSRouteTypeWeb:
            viewRouteMap = _webJumpViewRouterMap;
            viewRouteHandler = viewRouteMap[webRouter];
            break;
        case CSRouteTypeNative:
        {
            viewRouteMap = _nativeJumpViewRouterMap;
            viewRouteHandler = viewRouteMap[request.path];
        }
            break;
        default:
            break;
    }
    return viewRouteHandler ? viewRouteHandler(request) : nil;
}

- (void)addDynamicRouterSetting:(CSDynamicRouteSettingBlock)dynamicRouterSettingBlock
{
    self.dynamicRouteSetting = dynamicRouterSettingBlock;
}

@end
