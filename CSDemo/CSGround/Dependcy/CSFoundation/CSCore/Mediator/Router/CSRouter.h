//
//  CSRouter.h
//  CSMediator_Example
//
//  Created by joy_yu on 2018/4/7.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSRouterModuleProtocol.h"
#import "CSTransmissionData.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CSRouteType) {
    CSRouteTypeNone = -1,
    CSRouteTypeNative = 0,
    CSRouteTypeWeb,
    CSRouteTypeRN
};

extern NSString *const webRouter;
extern NSString *const nativeRouter;
extern NSString *const rnRouter;

extern NSString *const router;

@class CSRouteRequest;

typedef void (^CSJumpRouteHandler)(CSRouteRequest *request);
typedef void (^CSJumpCallbackHandler)(CSTransmissionData *data);

typedef UIViewController *(^CSViewRouteHandler)(CSRouteRequest *request);
typedef UIViewController *(^CSViewRouteCallbackHandle)(CSRouteRequest *request);

//每个路由执行时进行的预处理逻辑：路由规则解析，路由实现分配：native,web,rn
typedef void (^CSDynamicRouteSettingBlock)(CSRouteRequest *request);

@interface CSRouter : NSObject

+ (instancetype)router;

//register
- (void)registerNativePath:(NSString *)path jump:(CSJumpRouteHandler)handler;

- (void)registerNativePath:(NSString *)path view:(CSViewRouteHandler)handler;

- (void)registerWebPathWithJump:(CSJumpRouteHandler)handler;

- (void)registerWebPathWithView:(CSViewRouteHandler)handler;

- (void)registerRNPathWithJump:(CSJumpRouteHandler)handler;

- (void)registerRNPathWithView:(CSViewRouteHandler)handler;

//jump
- (void)jumpTo:(NSString *)url fromViewController:(UIViewController *)vc;

- (UIViewController *)viewForURL:(NSString *)url;

- (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc;

- (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data;

- (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc jumpCallbackHandler:(CSJumpCallbackHandler)handler;

- (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data viewRouteCallbackHandler:(CSViewRouteHandler)handler;


- (void)addDynamicRouterSetting:(CSDynamicRouteSettingBlock)dynamicRouterSettingBlock;

@end

