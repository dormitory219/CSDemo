//
//  CSRouteRequest.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSRouteRequest.h"

@implementation CSRouteRequest

+ (instancetype)requestWithURL:(NSString *)toURL parameter:(CSTransmissionData *)data callback:(CSJumpCallbackHandler)callback fromVC:(UIViewController *)vc
{
    CSRouteRequest *request = [[self alloc] init];
    request.url = toURL;
    request.data = data;
    request.callbackHandler = callback;
    request.fromVC = vc;
    [request configWithURL:toURL];
    return request;
}

- (void)configWithURL:(NSString *)url
{
    NSURLComponents *urlComponets = [NSURLComponents componentsWithString:url];
    NSString *scheme = urlComponets.scheme;
    NSString *module = urlComponets.host;
    NSString *page = urlComponets.path;
    NSString *path = module;
    if (!scheme.length) {
        NSLog(@"URL : %@ 错误！！",_url);
        _valid = NO;
        _routeType = CSRouteTypeNone;
        return;
    }
    
    //判断路由类型
    if ([scheme isEqualToString:webRouter]) {
        _routeType = CSRouteTypeWeb;
    }else if ([scheme isEqualToString:nativeRouter]){
        _routeType = CSRouteTypeNative;
    }else if ([scheme isEqualToString:rnRouter]){
        _routeType = CSRouteTypeRN;
    }
    
    //定义的需要实现路由规则，但未实现:默认使用本地路由
    if ([scheme isEqualToString:router] && !self.dynamicSetting) {
        NSLog(@"no dynamic setting,default we use native router");
        if (page.length > 1) {
            path = [module stringByAppendingString:page];
            page = [page substringFromIndex:1];
        }
        _module = module;
        _path = path;
        _page = page;
        _routeType = CSRouteTypeNative;
    }
    //通过url获取参数更新model
    NSString *query = urlComponets.query;
    if (query) {
        for (NSURLQueryItem *item in urlComponets.queryItems) {
            NSString *name = item.name;
            NSString *value = item.value;
            //updateModel
            name,value;
        }
    }
}

//block调用的防crash
- (CSJumpCallbackHandler)callbackHandler
{
    if (!_callbackHandler) {
        NSLog(@"error,execute block not initialize:%@",self.currentURL);
        return ^(id parameter){
            
        };
    }else{
        return _callbackHandler;
    }
}



@end
