//
//  CSRouteRequest.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CSRouter.h"

@interface CSRouteRequest : NSObject

@property (nonatomic,copy) NSString *url;
@property (nonatomic,strong) CSTransmissionData *data;
@property (nonatomic,strong) CSJumpCallbackHandler callbackHandler;
@property (nonatomic,strong) UIViewController *fromVC;
@property (nonatomic,assign) CSRouteType routeType;
@property (nonatomic,assign) BOOL valid;


/**
 是否启动动态路由规则
 */
@property (nonatomic,assign) BOOL dynamicSetting;

@property (nonatomic,strong) NSString *module;

@property (nonatomic,strong) NSString *page;

@property (nonatomic,strong) NSString *path;

@property (nonatomic,copy) NSString *currentURL;

+ (instancetype)requestWithURL:(NSString *)toURL
                     parameter:(CSTransmissionData *)data
                      callback:(CSJumpCallbackHandler)callback
                        fromVC:(UIViewController *)vc;

- (void)configWithURL:(NSString *)url;


@end
