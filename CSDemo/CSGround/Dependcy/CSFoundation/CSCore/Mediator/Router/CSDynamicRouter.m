//
//  CSDynamicRouter.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSDynamicRouter.h"
#import "CSRouter.h"
#import "CSRouteRequest.h"

@implementation CSDynamicRouter

+ (instancetype)router
{
    static CSDynamicRouter *dyRouter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dyRouter = [[self alloc] init];
    });
    return dyRouter;
}

- (void)setupDynamicSetting:(NSDictionary *)setting
{
    [[CSRouter router] addDynamicRouterSetting:^(CSRouteRequest * _Nonnull request) {
        
        NSURLComponents *urlComponets = [NSURLComponents componentsWithString:request.url];
        NSString *module = urlComponets.host;
        NSString *scheme = urlComponets.scheme;
        if (!module.length) {
            NSLog(@"当前URL 设置出错！ %@",request.url);
            return;
        }
        
        if ([scheme isEqualToString:router]) {
            NSString *page = urlComponets.path;
            NSString *path = module;
            if (page.length > 1) {
                path = [module stringByAppendingString:page];
                page = [page substringFromIndex:1];
            }
            request.module = module;
            request.path = path;
            request.page = page;
      
            NSString *redirectPath = [setting objectForKey:request.module];
            if (redirectPath) {
                [request configWithURL:redirectPath];
                redirectPath = [redirectPath stringByAppendingString:request.page];
                NSLog(@"动态路由设置匹配 ： %@ => %@", request.url,redirectPath);
                request.currentURL = redirectPath;
            } else {
                NSLog(@"动态路由设置中 %@ 未查找到映射规则！！", request);
                request.valid = NO;
            }
            
        }else{
            NSLog(@"error router url :%@",request.url);
        }
        
    }];
}

@end
