//
//  CSMediator.m
//  CSMediator_Example
//
//  Created by joy_yu on 2018/4/7.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSMediator.h"

@implementation CSMediator

@end

@implementation CSMediator(Router)

//register
+ (void)registerNativePath:(NSString *)path jump:(CSJumpRouteHandler)handler
{
    [[CSRouter router] registerNativePath:path jump:handler];
}

+ (void)registerNativePath:(NSString *)path view:(CSViewRouteHandler)handler
{
    [[CSRouter router] registerNativePath:path view:handler];
}

+ (void)registerWebPathWithJump:(CSJumpRouteHandler)handler
{
    [[CSRouter router] registerWebPathWithJump:handler];
}

+ (void)registerWebPathWithView:(CSViewRouteHandler)handler
{
    [[CSRouter router] registerWebPathWithView:handler];
}

+ (void)registerRNPathWithJump:(CSJumpRouteHandler)handler
{
    [[CSRouter router] registerRNPathWithJump:handler];
}

+ (void)registerRNPathWithView:(CSViewRouteHandler)handler
{
    [[CSRouter router] registerRNPathWithView:handler];
}

//jump
+ (void)jumpTo:(NSString *)url fromViewController:(UIViewController *)vc
{
    [[CSRouter router] jumpTo:url fromViewController:vc];
}

+ (UIViewController *)viewForURL:(NSString *)url
{
   return [[CSRouter router] viewForURL:url];
}

+ (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc
{
    [[CSRouter router] jumpTo:url parameter:data fromViewController:vc];
}

+ (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data
{
   return [[CSRouter router] viewForURL:url parameter:data];
}

+ (void)jumpTo:(NSString *)url parameter:(CSTransmissionData *)data fromViewController:(UIViewController *)vc jumpCallbackHandler:(CSJumpCallbackHandler)handler
{
    [[CSRouter router] jumpTo:url parameter:data fromViewController:vc jumpCallbackHandler:handler];
}

+ (UIViewController *)viewForURL:(NSString *)url parameter:(CSTransmissionData *)data viewRouteCallbackHandler:(CSViewRouteHandler)handler
{
   return  [[CSRouter router] viewForURL:url parameter:data viewRouteCallbackHandler:handler];
}

@end

@implementation CSMediator(Service)

+ (void)registerService:(Protocol *)proto withImpl:(id)impl
{
    [[CSServiceManager sharedManager] registerService:proto withImpl:impl];
}

+ (id)serviceForProtocol:(Protocol *)serviceProtocol
{
    return [[CSServiceManager sharedManager] serviceForProtocol:serviceProtocol];
}

@end
