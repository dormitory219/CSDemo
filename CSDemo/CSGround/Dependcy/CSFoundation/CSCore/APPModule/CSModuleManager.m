//
//  CSModuleManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSModuleManager.h"

static NSMutableDictionary <NSString *, id<CSModuleProtocol>> *registerModules;

@interface CSModuleManager()

@end

@implementation CSModuleManager

static CSModuleManager *manager = nil;
+ (instancetype)shareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

+ (void)registerModule:(id<CSModuleProtocol>)module
{
    NSParameterAssert(module);
    if (![module conformsToProtocol:@protocol(CSModuleProtocol)]){
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"%@ 模块不符合 %@ 协议", NSStringFromClass([module class]), NSStringFromProtocol(@protocol(CSModuleProtocol))] userInfo:nil];
    }
    
    @synchronized(registerModules){
        if (registerModules == nil){
            registerModules = [[NSMutableDictionary alloc] initWithCapacity:1];
        }
        NSAssert(![registerModules objectForKey:NSStringFromClass([module class])], @"该 %@ 模块已经被注册",NSStringFromClass([module class]));
        [registerModules setObject:module forKey:NSStringFromClass([module class])];
    }
}

+ (void)initializeAllModules
{
    if (!registerModules.count) {
        NSLog(@"未注册module!");
    }else{
        for (id<CSModuleProtocol> module in registerModules.allValues) {
            [module moduleInitalize];
        }
    }
}

+ (id<CSModuleProtocol>)getModule:(NSString *)moduleName
{
    return [registerModules objectForKey:moduleName];
}

@end
