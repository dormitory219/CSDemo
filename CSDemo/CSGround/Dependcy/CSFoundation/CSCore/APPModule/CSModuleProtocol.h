//
//  CSModuleProtocol.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2019/3/13.
//  Copyright © 2019 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSModuleProtocol_h
#define CSModuleProtocol_h
#import <UIKit/UIKit.h>

@protocol CSModuleActionProtocol;

@protocol  CSModuleProtocol<NSObject>

- (void)moduleInitalize;

- (void)ff;

- (void)doAction:(id<CSModuleActionProtocol>)action;

@end

#endif /* CSModuleProtocol_h */
