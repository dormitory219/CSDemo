//
//  CSModuleManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSModuleProtocol.h"

#define CSMODULEREGISTER()\
+ (void)load{  \
   [CSModuleManager registerModule:[[self alloc] init]]; \
}

@interface CSModuleManager : NSObject

+ (void)registerModule:(id<CSModuleProtocol>)module;

+ (void)initializeAllModules;

+ (id<CSModuleProtocol>)getModule:(NSString *)moduleName;

@end
