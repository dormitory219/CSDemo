//
//  CSModelTypeData.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSModelTypeData.h"

@implementation CSModelTypeData

+ (instancetype)modelDataWithValue:(id<CSDataModelProtocol>)value {
    CSModelTypeData *data = [CSModelTypeData dataWithValue:value];
    return data;
}

@end
