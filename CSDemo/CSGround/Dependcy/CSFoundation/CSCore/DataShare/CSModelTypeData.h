//
//  CSModelTypeData.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSBasicData.h"

@protocol CSDataModelProtocol <NSObject>

@end

@interface CSModelTypeData : CSBasicData

+ (instancetype)modelDataWithValue:(id<CSDataModelProtocol>)value;;

@end
