//
//  CSBasicTypeData.h
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSBasicData.h"

@class UIImage;
// 基础数据类型， 再继续细分。
typedef NS_ENUM(NSUInteger, CSDataBasicType) {
    CSDataBasicTypeNumber,// 数据类型只能存 number 类型， 其他的 int ,bool,float都需要转换成number.
    CSDataBasicTypeString, // 字符串类型
    CSDataBasicTypeArray, // 数组类型 ,
    CSDataBasicTypeDictionary,// 字典类型
    // 以上是基础类型。
    // 以下是比较特殊的类型。
    CSDataBasicTypeUIImage, // 支持图像类型。
    CSDataBasicTypeBoolean, // bool 类型
    CSDataBasicTypeData , // NSData 类型
    CSDataBasicTypeDate , // NSDate类型
};

@interface CSBasicTypeData : CSBasicData

@property (nonatomic,readonly,assign) CSDataBasicType basicType;

+ (instancetype)basicDataWithNumber:(NSNumber *)number;

+ (instancetype)basicDataWithString:(NSString *)string;

+ (instancetype)basicDataWithArray:(NSArray *)array;

+ (instancetype)basicDataWithDictionary:(NSDictionary *)dictionary;

+ (instancetype)basicDataWithImage:(UIImage *)image;

+ (instancetype)basicDataWithBoolean:(BOOL)boolean;

+ (instancetype)basicDataWithData:(NSData *)data;

+ (instancetype)basicDataWithDate:(NSDate *)date;

@end
