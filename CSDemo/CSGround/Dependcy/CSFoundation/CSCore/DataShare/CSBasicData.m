//
//  CSBasicData.m
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSBasicData.h"

@interface CSBasicData()

@property (nonatomic,strong) NSMutableDictionary<NSString *,id> *storedDatas;

@end

@implementation CSBasicData


- (instancetype)init
{
    self = [super init];
    if (self) {
        _storedDatas = [[NSMutableDictionary alloc] initWithCapacity:10];
    }
    return self;
}

+ (instancetype)dataWithValue:(id)value
{
    CSBasicData *data = [[self alloc] init];
    data.value = value;
    return data;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ , value : %@" , NSStringFromClass([self class]),_value];
}


@end
