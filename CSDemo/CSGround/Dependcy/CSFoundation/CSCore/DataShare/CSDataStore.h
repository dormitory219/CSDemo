//
//  CSDataStore.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSBasicData.h"

@interface CSDataStore : NSObject

@property (nonatomic,strong) NSMutableDictionary<NSString *,CSBasicData *> *storedDatas;

+ (instancetype)sharedData;

/**
 设置数据时， 不需要指定数据类型。
 
 @param data 需要存储的数据 ， 如果是 model类型，就必须要满足 CSDataModelProtocol
 @param key 键值
 */
- (void)setData:(id)data forKey:(NSString *)key;

/**
 删除共享数据
 
 @param key 键值。
 */
- (void)removeDataForKey:(NSString *)key;

- (CSBasicData *)dataForKey:(NSString *)key;


@end
