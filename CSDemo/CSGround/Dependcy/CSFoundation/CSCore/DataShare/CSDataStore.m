//
//  CSDataStore.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSDataStore.h"
#import "CSBasicTypeData.h"
#import "CSModelTypeData.h"

@interface CSDataStore()

@end

@implementation CSDataStore

+ (instancetype)sharedData {
    static CSDataStore *sharedData;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedData = [[CSDataStore alloc] init];
        sharedData.storedDatas = [[NSMutableDictionary alloc] initWithCapacity:10];
    });
    return sharedData;
}

- (void)setData:(id)data forKey:(NSString *)key {
    NSParameterAssert([key isKindOfClass:[NSString class]]);
    NSParameterAssert(data);
    if ([data isKindOfClass:[NSNumber class]]) {
        NSLog(@"存储NSNumber : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithNumber:data] forKey:key];
    }else if ([data isKindOfClass:[NSString class]]) {
        NSLog(@"存储NSString : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithString:data] forKey:key];
    }else if ([data isKindOfClass:[NSArray class]]) {
        NSLog(@"存储NSArray : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithArray:data] forKey:key];
    }else if([data isKindOfClass:[NSDictionary class]]) {
        NSLog(@"存储NSDictionary : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithDictionary:data] forKey:key];
    }else if ([data isKindOfClass:[UIImage class]]) {
        NSLog(@"存储UIImage : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithImage:data] forKey:key];
    }else if ([data isKindOfClass:[NSData class]]) {
        NSLog(@"存储NSData : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithData:data] forKey:key];
    }else if ([data isKindOfClass:[NSDate class]]) {
        NSLog(@"存储NSDate : %@ = %@",key,data);
        [_storedDatas setObject:[CSBasicTypeData basicDataWithDate:data] forKey:key];
    }else if ([data conformsToProtocol:@protocol(CSDataModelProtocol)] ) {
        NSLog(@"存储Model : %@ = %@",key,data);
        [_storedDatas setObject:[CSModelTypeData modelDataWithValue:data] forKey:key];
    }else {
        NSLog(@"检测数据不合法 , %@ : %@！！！",key,data);
    }
}

- (void)removeDataForKey:(NSString *)key {
    NSParameterAssert([key isKindOfClass:[NSString class]]);
    NSLog(@"删除数据 %@ ",key);
    [_storedDatas removeObjectForKey:key];
}

- (CSBasicData *)dataForKey:(NSString *)key {
    NSParameterAssert([key isKindOfClass:[NSString class]]);
    return [_storedDatas objectForKey:key];
}

@end
