//
//  CSTransmissionData.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSDataStore.h"

/**
 该数据类型可用于模块，service之间的值传递
 
 为什么需要将model包装一层？因为这样可以适应多种数据类型，模块之间传递数据，不仅局限于model，也可以是任意其他类型如string,number，通过CSTransmissionData可以将这些类型统一包装成CSTransmissionData，
 
 */

@interface CSTransmissionData : CSDataStore

+ (instancetype)data;

@end
