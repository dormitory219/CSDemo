//
//  CSBasicTypeData.m
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSBasicTypeData.h"

@interface CSBasicTypeData ()
@property (nonatomic,assign) CSDataBasicType basicType;
@end

@implementation CSBasicTypeData

+ (instancetype)basicDataWithNumber:(NSNumber *)number {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:[number copy]];
    data.basicType = CSDataBasicTypeNumber;
    return data;
}


+ (instancetype)basicDataWithString:(NSString *)string {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:[string copy]];
    data.basicType = CSDataBasicTypeString;
    return data;
}

+ (instancetype)basicDataWithArray:(NSArray *)array {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:[array copy]];
    data.basicType = CSDataBasicTypeArray;
    return data;
}

+ (instancetype)basicDataWithDictionary:(NSDictionary *)dictionary {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:[dictionary copy]];
    data.basicType = CSDataBasicTypeDictionary;
    return data;
}


+ (instancetype)basicDataWithImage:(UIImage *)image {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:image];
    data.basicType = CSDataBasicTypeUIImage;
    return data;
}


+ (instancetype)basicDataWithBoolean:(BOOL)boolean {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:@(boolean)];
    data.basicType = CSDataBasicTypeBoolean;
    return data;
}

+ (instancetype)basicDataWithData:(NSData *)data {
    CSBasicTypeData *d = [CSBasicTypeData dataWithValue:data];
    d.basicType = CSDataBasicTypeData;
    return d;
}

+ (instancetype)basicDataWithDate:(NSDate *)date {
    CSBasicTypeData *data = [CSBasicTypeData dataWithValue:date];
    data.basicType = CSDataBasicTypeDate;
    return data;
}

@end
