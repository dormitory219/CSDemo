//
//  CSGroundModule.m
//  CSDemo
//
//  Created by 余强 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSGroundModule.h"
#import "CSGroundAPI.h"

@implementation CSGroundModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    NSLog(@"groundModule moduleInitalize");
    /*
     业务组件之下的组件的初始化，这个gound组件可以为任意app提供基础框架能力，内部封装了业务之下好几层的组件，最基础组件（CSFoundation） -> 公共业务组件 (CSUIKit ,CSUtility)
     
    Gound组件为非上层业务组件的打包体，也作为一个模块，做底层模块的初始化
    一. 基础组件：CSFoundation
     1.基础category
     2.基础依赖第三方，AFNetwork,SDWebimage,Masonry,JSonModel
     3.宏定义，基础常量
     4.CSCore: 支撑组件化的基石，提供模块间三大能力：
         4.1 数据传递Data;
         4.2 路由跳转Router,服务解耦serviceMediator;
         4.3 事件通知EventBus
     
    二. 公共业务组件：CSUtility 和 CSUIKit
     1.UI组件：
     CSUIKit为UI组件，包括通用的跨app的UI组件：CSPublicUIKit，以及以产品相关的定制的UI组件：CSCustomUIKit
     
     2.非UI业务组件
     CSUtility为非UI的公共业务组件,包括CSPopKit,CSNetwork,CSLogAgent,CSJSBridge,CSCore
     
     */
   
    
    //初始化操作
    
    [CSNetworkManager setup];
    
}

- (void)doAction:(id<CSModuleActionProtocol>)action { 
    
}


@end
