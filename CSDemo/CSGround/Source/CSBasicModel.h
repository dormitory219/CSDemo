//
//  CSBasicModel.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/11.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSModelTypeData.h"

/**
 model类型基类 ,业务组件的model类型都以 BasicModel为基类。
 */
@interface CSBasicModel : NSObject<CSDataModelProtocol>

@end
