//
//  ViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "ViewController.h"
#import "CSGroundAPI.h"

#import "CSLoginModuleAPI.h"
#import "CSMainModuleAPI.h"
#import "CSCameraModuleAPI.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginAction:(id)btn
{
    CSTransmissionData *data = [CSTransmissionData data];
    [data setData:@"12345678" forKey:LOGIN_DATA_USERINFO];
    //模块间值传递
    [CSMediator jumpTo:LOGIN_ROUTER_LOGIN parameter:data fromViewController:self jumpCallbackHandler:^(CSTransmissionData * _Nonnull data) {
        
    }];
}

- (IBAction)registerAction:(id)btn
{
    CSLoginInfoModel *model = [[CSLoginInfoModel alloc] init];
    //共享缓存，全局使用
    [[CSDataStore sharedData] setData:model forKey:LOGIN_DATA_USERINFO];
    
    CSTransmissionData *data = [[CSTransmissionData alloc] init];
    [data setData:@"jjj" forKey:LOGIN_DATA_USERINFO];
    //模块间值传递
    [CSMediator jumpTo:LOGIN_ROUTER_REGISTER parameter:data fromViewController:self jumpCallbackHandler:^(CSTransmissionData * _Nonnull data) {
        NSLog(@"callBack,model:%@",data);
    }];
}

- (IBAction)testAction:(id)sender {
    
    [CSMediator jumpTo:MAIN_ROUTER_HOME parameter:nil fromViewController:self jumpCallbackHandler:^(CSTransmissionData * _Nonnull data) {
        NSLog(@"callBack,model:%@",data);
    }];
}

- (IBAction)cameraAction:(id)sender {
    CSTransmissionData *data = [[CSTransmissionData alloc] init];
    [data setData:@(CSCameraModeGroupTypeSignalTake) forKey:CSCameraModuleGroupKey];
    //模块间值传递
    [CSMediator jumpTo:CAMERA_ROUTER_CAMERA parameter:data fromViewController:self jumpCallbackHandler:^(CSTransmissionData * _Nonnull data) {
        NSLog(@"callBack,model:%@",data);
    }];
}

@end
