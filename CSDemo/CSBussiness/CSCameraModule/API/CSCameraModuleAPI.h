//
//  CSCameraModuleAPI.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraModeEvent.h"

#ifndef CSCameraModuleAPI_h
#define CSCameraModuleAPI_h

#define CAMERA_ROUTER_CAMERA             @"route://camera/camera"
#define CAMERA_ROUTER_GREETINGCARD             @"route://camera/greetingCard"
#define CAMERA_ROUTER_CERTIFICATE             @"route://camera/certificate"
#define CAMERA_ROUTER_SINGLETAKE             @"route://camera/singleTake"
#define CAMERA_ROUTER_MULTIPLETAKE             @"route://camera/multipleTake"
#define CAMERA_ROUTER_TEXTRECOGNIZE             @"route://camera/textRecognize"

#define CSCameraModuleGroupKey  @"CSCameraModuleGroupKey"

//相机模式组合
typedef NS_ENUM(NSInteger,CSCameraModeGroupType)
{
    CSCameraModeGroupTypeCommon,
    CSCameraModeGroupTypeSignalTake
};

#endif /* CSCameraModuleAPI_h */
