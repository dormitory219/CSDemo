//
//  CSCertificateCameraMode.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCertificateCameraMode.h"
#import "CSCameraModeManager.h"
#import "CSCertificateModeChildViewController.h"
#import "CSCameraModuleAPI.h"

@implementation CSCertificateCameraMode

//CSCAMERAMODEREGISTER()

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cameraChildViewController = [[CSCertificateModeChildViewController alloc] init];
        self.index = 1;
        self.title = @"证件";
        self.routerUrl = CAMERA_ROUTER_CERTIFICATE;
    }
    return self;
}

@end
