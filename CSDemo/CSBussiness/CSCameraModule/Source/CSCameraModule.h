//
//  CSCameraModule.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSModuleProtocol.h"

@interface CSCameraModule : NSObject<CSModuleProtocol>

@end
