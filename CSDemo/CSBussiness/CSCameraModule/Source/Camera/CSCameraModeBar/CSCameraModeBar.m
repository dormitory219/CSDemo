//
//  CSCameraModeBar.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/26.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraModeBar.h"
#import "CSCameraModeManager.h"

@interface CSCameraModeBar()

@property (weak, nonatomic) IBOutlet UIStackView *modeContainer;

@end

@implementation CSCameraModeBar

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setGroupType:(CSCameraModeGroupType)groupType
{
    _groupType = groupType;
    switch (groupType) {
        case CSCameraModeGroupTypeCommon:
        {
            for (NSInteger i = 0; i < [CSCameraModeManager shareManager].allModes.count; i++) {
                __kindof CSCameraMode *mode = [CSCameraModeManager shareManager].allModes[i];
                UIButton *button = [[UIButton alloc] init];
                [button setTitle:mode.title forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:12];
                button.tag = 100 + i;
                [button addTarget:self action:@selector(cameraModeAction:) forControlEvents:UIControlEventTouchUpInside];
                [self.modeContainer addArrangedSubview:button];
                [button setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
            }
        }
            break;
            
        case CSCameraModeGroupTypeSignalTake:
        {
            UIButton *button = [[UIButton alloc] init];
            __kindof CSCameraMode *mode = [[CSSingleTakeCameraMode alloc] init];
            [button setTitle:mode.title forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:14];
            [self.modeContainer addArrangedSubview:button];
        }
            break;
            
        default:
            break;
    }
    
}

- (void)setSelectType:(CSCameraModeType)selectType
{
    if (_selectType != selectType) {
        for (UIButton *btn in self.modeContainer.subviews) {
            btn.selected = NO;
        }
        UIButton *btn = self.modeContainer.subviews[selectType];
        btn.selected = YES;
    }
    _selectType = selectType;
}

+ (instancetype)modeBar
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (void)cameraModeAction:(UIButton *)sender {
    
    for (UIButton *btn in self.modeContainer.subviews) {
        btn.selected = NO;
    }
    sender.selected = YES;
    NSInteger index = sender.tag - 100;
    if (self.delegate && [self.delegate respondsToSelector:@selector(modeDidSelect:cameraBar:)]) {
        [self.delegate modeDidSelect:index cameraBar:self];
    }
}

@end
