//
//  CSCameraModeBar.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/26.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCameraModuleAPI.h"
#import "CSCameraModeManager.h"

@class CSCameraModeBar;
@protocol CSCameraModeBarProtocol<NSObject>

- (void)modeDidSelect:(NSInteger)index cameraBar:(CSCameraModeBar *)cameraBar;

@end

@interface CSCameraModeBar : UIView

@property (nonatomic,weak) id<CSCameraModeBarProtocol> delegate;

@property (nonatomic,assign) CSCameraModeGroupType groupType;

@property (nonatomic,assign) CSCameraModeType selectType;

+ (instancetype)modeBar;

@end
