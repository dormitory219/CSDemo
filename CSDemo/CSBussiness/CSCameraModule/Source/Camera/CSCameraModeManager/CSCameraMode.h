//
//  CSCameraMode.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSGroundAPI.h"

@interface CSCameraMode : NSObject

@property (nonatomic,strong) __kindof CSViewController *cameraChildViewController;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *routerUrl;

@end
