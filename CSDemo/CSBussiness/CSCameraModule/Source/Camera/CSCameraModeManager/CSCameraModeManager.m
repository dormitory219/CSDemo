//
//  CSCameraModeManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/26.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraModeManager.h"

@interface CSCameraModeManager()

@property (nonatomic,strong) NSMutableArray *allModes;

@end

@implementation CSCameraModeManager

+ (instancetype)shareManager
{
    static CSCameraModeManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void)setGroupType:(CSCameraModeGroupType)groupType
{
    _groupType = groupType;
    [self.allModes removeAllObjects];
    
    switch (groupType) {
        case CSCameraModeGroupTypeCommon:
        {
            __kindof CSCameraMode *cameraMode = [[CSGreetingCardCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
            
            cameraMode = [[CSCertificateCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
            
            cameraMode = [[CSTextRecognizeCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
            
            cameraMode = [[CSSingleTakeCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
            
            cameraMode = [[CSMultipleTakeCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
        }
            break;
            
        case CSCameraModeGroupTypeSignalTake:
        {
            __kindof CSCameraMode *cameraMode = [[CSSingleTakeCameraMode alloc] init];
            [self.allModes addObject:cameraMode];
        }
            break;
            
        default:
            break;
    }
}

+ (void)registerCameraMode:(__kindof CSCameraMode *)cameraMode
{
    NSMutableArray *allModules = [[self shareManager] allModes];
    [allModules addObject:cameraMode];
    [allModules sortUsingComparator:^NSComparisonResult(CSCameraMode * _Nonnull obj1, CSCameraMode * _Nonnull obj2) {
        if (obj1.index > obj2.index) {
            return NSOrderedDescending;
        }else if (obj1.index == obj2.index){
            return NSOrderedSame;
        }else{
            return NSOrderedAscending;
        }
    }];
}

- (NSMutableArray *)allModes
{
    if (!_allModes) {
        _allModes = [NSMutableArray arrayWithCapacity:2];
    }
    return _allModes;
}

@end
