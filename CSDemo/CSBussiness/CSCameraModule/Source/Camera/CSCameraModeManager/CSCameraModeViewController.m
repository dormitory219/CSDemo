//
//  CSCameraModeViewController.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraModeViewController.h"
#import "CSGroundAPI.h"

@interface CSCameraModeViewController ()

@property (nonatomic,strong) UIView *topBar;

@end

@implementation CSCameraModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.topBar];
    [self.topBar mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.top.right;
        make.height.mas_equalTo(44);
    }];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIView *)topBar
{
    if (!_topBar) {
        _topBar = [[UIView alloc] init];
        _topBar.backgroundColor = MAIN_THEME_COLOR;
    }
    return _topBar;
}


@end
