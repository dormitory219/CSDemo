//
//  CSCameraModeManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/26.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSCameraMode.h"
#import "CSCameraModuleAPI.h"
#import "CSCameraModeHeader.h"

typedef NS_ENUM(NSInteger,CSCameraModeType)
{
    CSCameraModeTypeGreetingCard,
    CSCameraModeTypeCertificate,
    CSCameraModeTypeTextRecognize,
    CSCameraModeTypeSingleTake,
    CSCameraModeTypeMultipleTake
};

#define CSCAMERAMODEREGISTER()\
+ (void)load{  \
[CSCameraModeManager registerCameraMode:[[self alloc] init]]; \
}

@interface CSCameraModeManager : NSObject

@property (nonatomic,assign) CSCameraModeGroupType groupType;

+ (instancetype)shareManager;

+ (void)registerCameraMode:(__kindof CSCameraMode *)cameraMode;

- (NSMutableArray *)allModes;

@end
