//
//  CSCameraViewController.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraViewController.h"
#import "CSGroundAPI.h"

#import "CSCameraModeViewController.h"

#import "CSCameraModuleAPI.h"
#import "CSCameraModeBar.h"
#import "CSCameraModeManager.h"

@interface CSCameraViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource,CSCameraModeBarProtocol>

@property (weak, nonatomic) IBOutlet UIView *topContainer;

@property (weak, nonatomic) IBOutlet UIView *cameraModeBarContainer;

@property (nonatomic,strong) UIPageViewController *pageController;

@property (nonatomic,strong) NSMutableArray *cameraModeViewControllers;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) CSCameraModeBar *cameraModeBar;

@property (nonatomic,assign) CSCameraModeGroupType groupType;

@end

@implementation CSCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MAIN_THEME_COLOR;
    
    CSCameraModeGroupType groupType = (CSCameraModeGroupType)[[self.routeRequest.data dataForKey:CSCameraModuleGroupKey].value integerValue];
    [CSCameraModeManager shareManager].groupType = groupType;
    self.groupType = groupType;
    
    [self addChildViewController:self.pageController];
    [self.cameraModeBarContainer addSubview:self.cameraModeBar];
    [self.cameraModeBar mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.top.right.bottom;
    }];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)modeDidSelect:(NSInteger)index cameraBar:(CSCameraModeBar *)cameraBar
{
    self.index = index;
    [self.pageController setViewControllers:@[self.cameraModeViewControllers[index]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (IBAction)takePhotoAction:(id)sender {
    CSCameraMode *mode = [CSCameraModeManager shareManager].allModes[self.index];
    [CSMediator jumpTo:mode.routerUrl fromViewController:self];
}

- (IBAction)dismissAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [self.cameraModeViewControllers indexOfObject:viewController];
    self.index = index;
    self.cameraModeBar.selectType = index;
    if (index == 0) {
        return nil;
    }
    return self.cameraModeViewControllers[index-1];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [self.cameraModeViewControllers indexOfObject:viewController];
    self.index = index;
    self.cameraModeBar.selectType = index;
    if (index + 1 == self.cameraModeViewControllers.count) {
        return nil;
    }
    return self.cameraModeViewControllers[index+1];
}

- (UIPageViewController *)pageController
{
    if (!_pageController) {
        
        _pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:@{ UIPageViewControllerOptionInterPageSpacingKey : @30.f}];
        _pageController.delegate = self;
        _pageController.dataSource = self;

        [_pageController setViewControllers:@[self.cameraModeViewControllers.firstObject] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [_pageController didMoveToParentViewController:self];
        [self.view addSubview:_pageController.view];
        [_pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            (void)make.left.top.right;
            make.bottom.mas_equalTo(-120);
        }];
    }
    return _pageController;
}

- (NSMutableArray *)cameraModeViewControllers
{
    if (!_cameraModeViewControllers) {
        _cameraModeViewControllers = [NSMutableArray arrayWithCapacity:2];
        for (NSInteger i = 0; i < [CSCameraModeManager shareManager].allModes.count; i++) {
            CSCameraMode *mode =  [CSCameraModeManager shareManager].allModes[i];
            [_cameraModeViewControllers addObject:mode.cameraChildViewController];
        }
    }
    return _cameraModeViewControllers;
}

- (CSCameraModeBar *)cameraModeBar
{
    if (!_cameraModeBar) {
        _cameraModeBar = [CSCameraModeBar modeBar];
        _cameraModeBar.groupType = self.groupType;
        _cameraModeBar.delegate = self;
    }
    return _cameraModeBar;
}

@end
