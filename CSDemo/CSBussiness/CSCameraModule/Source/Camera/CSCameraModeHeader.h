//
//  CSCameraModeHeader.h
//  CSDemo
//
//  Created by 余强 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSCameraModeHeader_h
#define CSCameraModeHeader_h

#import "CSGreetingCardCameraMode.h"
#import "CSCertificateCameraMode.h"
#import "CSTextRecognizeCameraMode.h"
#import "CSSingleTakeCameraMode.h"
#import "CSMultipleTakeCameraMode.h"

#endif /* CSCameraModeHeader_h */
