//
//  CSTextRecognizeCameraMode.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSTextRecognizeCameraMode.h"
#import "CSCameraModeManager.h"
#import "CSTextRecognizeModeChildViewController.h"
#import "CSCameraModuleAPI.h"

@implementation CSTextRecognizeCameraMode

//CSCAMERAMODEREGISTER()

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cameraChildViewController = [[CSTextRecognizeModeChildViewController alloc] init];
        self.index = 2;
        self.title = @"拍图识字";
        self.routerUrl = CAMERA_ROUTER_TEXTRECOGNIZE;
    }
    return self;
}

@end
