//
//  CSSingleTakeModeChildViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSingleTakeModeChildViewController.h"

@interface CSSingleTakeModeChildViewController ()

@end

@implementation CSSingleTakeModeChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor orangeColor];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
