//
//  CSSingleTakeViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/26.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSingleTakeViewController.h"
#import "CSGroundAPI.h"
#import "CSCameraModeEvent.h"

@interface CSSingleTakeViewController ()

@end

@implementation CSSingleTakeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"singleTake";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[QTEventBus shared] dispatch:[CSCameraModeEvent new]];
}

@end
