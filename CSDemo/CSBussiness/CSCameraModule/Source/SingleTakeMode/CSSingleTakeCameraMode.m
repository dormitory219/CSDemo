//
//  CSSingleTakeCameraMode.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSingleTakeCameraMode.h"
#import "CSCameraModeManager.h"
#import "CSSingleTakeModeChildViewController.h"
#import "CSCameraModuleAPI.h"

@implementation CSSingleTakeCameraMode

//CSCAMERAMODEREGISTER()

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cameraChildViewController = [[CSSingleTakeModeChildViewController alloc] init];
        self.index = 3;
        self.title = @"单拍";
        self.routerUrl = CAMERA_ROUTER_SINGLETAKE;
    }
    return self;
}

@end
