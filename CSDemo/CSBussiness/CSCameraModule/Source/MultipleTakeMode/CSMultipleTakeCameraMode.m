//
//  CSMultipleTakeCameraMode.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSMultipleTakeCameraMode.h"
#import "CSCameraModeManager.h"
#import "CSMultipleTakeModeChildViewController.h"
#import "CSCameraModuleAPI.h"

@implementation CSMultipleTakeCameraMode

//CSCAMERAMODEREGISTER()

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cameraChildViewController = [[CSMultipleTakeModeChildViewController alloc] init];
        self.index = 4;
        self.title = @"多拍";
        self.routerUrl = CAMERA_ROUTER_MULTIPLETAKE;
    }
    return self;
}

@end
