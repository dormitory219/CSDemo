//
//  CSCameraModule.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSCameraModule.h"
#import "CSCameraViewController.h"
#import "CSNavigationController.h"
#import "CSGreetingCardViewController.h"
#import "CSCertificateViewController.h"
#import "CSSingleTakeViewController.h"
#import "CSMultipleTakeViewController.h"
#import "CSTextRecognizeViewController.h"
#import "CSGroundAPI.h"

@implementation CSCameraModule

CSMODULEREGISTER()
- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"camera/camera" jump:^(CSRouteRequest * _Nonnull request) {
        CSCameraViewController *vc = [[CSCameraViewController alloc] init];
        vc.routeRequest = request;
        CSNavigationController *naviController = [[CSNavigationController alloc] initWithRootViewController:vc];
        [request.fromVC presentViewController:naviController animated:YES completion:^{
            
        }];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/greetingCard" jump:^(CSRouteRequest * _Nonnull request) {
        CSGreetingCardViewController *vc = [[CSGreetingCardViewController alloc] init];
        vc.routeRequest = request;
        [request.fromVC.navigationController pushViewController:vc animated:YES];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/greetingCard" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
        CSGreetingCardViewController *vc = [[CSGreetingCardViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
    
    
    [[CSRouter router] registerNativePath:@"camera/certificate" jump:^(CSRouteRequest * _Nonnull request) {
        CSCertificateViewController *vc = [[CSCertificateViewController alloc] init];
        vc.routeRequest = request;
        [request.fromVC.navigationController pushViewController:vc animated:YES];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/certificate" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
        
        CSCertificateViewController *vc = [[CSCertificateViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
    
    [[CSRouter router] registerNativePath:@"camera/singleTake" jump:^(CSRouteRequest * _Nonnull request) {
        CSSingleTakeViewController *vc = [[CSSingleTakeViewController alloc] init];
        vc.routeRequest = request;
        [request.fromVC.navigationController pushViewController:vc animated:YES];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/singleTake" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
        CSSingleTakeViewController *vc = [[CSSingleTakeViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
    
    [[CSRouter router] registerNativePath:@"camera/multipleTake" jump:^(CSRouteRequest * _Nonnull request) {
        CSMultipleTakeViewController *vc = [[CSMultipleTakeViewController alloc] init];
        vc.routeRequest = request;
        [request.fromVC.navigationController pushViewController:vc animated:YES];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/multipleTake" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
        
        CSMultipleTakeViewController *vc = [[CSMultipleTakeViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
    
    [[CSRouter router] registerNativePath:@"camera/textRecognize" jump:^(CSRouteRequest * _Nonnull request) {
        CSTextRecognizeViewController *vc = [[CSTextRecognizeViewController alloc] init];
        vc.routeRequest = request;
        [request.fromVC.navigationController pushViewController:vc animated:YES];
    }];
    
    [[CSRouter router] registerNativePath:@"camera/textRecognize" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
        CSTextRecognizeViewController *vc = [[CSTextRecognizeViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
}

- (void)doAction:(id<CSModuleActionProtocol>)action
{
    
}

@end
