//
//  CSGreetingCardCameraMode.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/27.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSGreetingCardCameraMode.h"
#import "CSCameraModeManager.h"
#import "CSGreetingCardModeChildViewController.h"
#import "CSCameraModuleAPI.h"

@implementation CSGreetingCardCameraMode

//CSCAMERAMODEREGISTER()

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cameraChildViewController = [[CSGreetingCardModeChildViewController alloc] init];
        self.index = 0;
        self.title = @"贺卡";
        self.routerUrl = CAMERA_ROUTER_GREETINGCARD;
    }
    return self;
}

@end
