//
//  CSLeftModule.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSLeftModule.h"
#import "CSLeftViewController.h"

@implementation CSLeftModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"left/main" view:^UIViewController *(CSRouteRequest *request) {
        CSLeftViewController *vc = [[CSLeftViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
}

- (void)doAction:(id<CSModuleActionProtocol>)action {
    
}


@end
