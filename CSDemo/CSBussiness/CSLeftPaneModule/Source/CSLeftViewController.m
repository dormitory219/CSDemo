//
//  CSLeftViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSLeftViewController.h"
#import "CSGroundAPI.h"
#import "CSSettingModuleAPI.h"
#import "CSLoginModuleAPI.h"

@interface CSLeftViewController ()

@end

@implementation CSLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = MAIN_THEME_COLOR;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClickAction:(UIButton *)sender {
    NSInteger index = sender.tag - 100;
    switch (index) {
        case 0:
        {
            [CSMediator jumpTo:LOGIN_ROUTER_LOGIN fromViewController:self];
        }
            break;
            
        case 8://设置
        {
            [CSMediator jumpTo:SETTING_ROUTER_MAIN fromViewController:self];
        }
            break;
            
        default:
            break;
    }
   
    
}




@end
