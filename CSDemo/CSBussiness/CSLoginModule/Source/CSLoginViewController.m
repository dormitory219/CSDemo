//
//  CSLoginViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSLoginViewController.h"
#import "CSRegisterViewController.h"
#import "CSMediator.h"
#import "CSLoginInfoModel.h"
#import "CSLoginModuleAPI.h"
#import "CSAppDelegateModuleAPI.h"
#import "CSRegisterViewController.h"

#import "CSLoginAPIManager.h"

@interface CSLoginViewController ()

@property (nonatomic,copy) NSString *loginAPIIdentifier;

@end

@implementation CSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"登录";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"thanksgiving_close"] style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss
{
    [self.view endEditing:YES];
    //block未初始化，但调用未crash,内部做了防crash机制
    self.routeRequest.callbackHandler(self.routeRequest.data);
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (self.loginAPIIdentifier.length) {
        [CSLoginAPIManager cancelRequest:self.loginAPIIdentifier];
    }
}
- (IBAction)loginAction:(UIButton *)sender {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Set the determinate mode to show task progress.
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"Loading...";
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
       self.loginAPIIdentifier = [CSLoginAPIManager getLoginInfo:^(CSLoginInfoModel *loginInfo) {
           
            [hud hideAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
           
        } failure:^(NSError *error) {
            
            [hud hideAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
    });
  
}

- (IBAction)registerAction:(UIButton *)sender {
    CSRegisterViewController *registerViewController = [[CSRegisterViewController alloc] init];
    [self.navigationController pushViewController:registerViewController animated:YES];
}

@end
