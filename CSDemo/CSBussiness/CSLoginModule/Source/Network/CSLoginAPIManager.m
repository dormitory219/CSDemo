//
//  CSLoginAPIManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/7.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSLoginAPIManager.h"

@implementation CSLoginAPIManager

+ (NSString *)getLoginInfo:(void(^)(CSLoginInfoModel *loginInfo))success
            failure:(void(^)(NSError *error))failure
{
    NSInteger pageNum = 0;
    NSString *identifier = [XMCenter sendRequest:^(XMRequest * _Nonnull request) {
        
        request.api = @"feed/listAll";
        request.version = @"1.0";
        request.httpMethod = kXMHTTPMethodGET;
        request.parameters = @{@"page": @(pageNum)};
        
    } onSuccess:^(id  _Nullable responseObject) {
    
        CSLoginInfoModel *info = [CSLoginInfoModel new];
        success ? success(info) : nil;

    } onFailure:^(NSError * _Nullable error) {
        
        NSLog(@"[Net Error]: %@", error.localizedDescription);
        failure ? failure(error) : nil;
        
    } onFinished:^(id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSLog(@"onFinished");
        
    }];
    return identifier;
}

@end
