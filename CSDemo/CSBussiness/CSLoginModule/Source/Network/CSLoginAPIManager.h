//
//  CSLoginAPIManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/7.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSAPIManager.h"
#import "CSGroundAPI.h"

#import "CSLoginInfoModel.h"

@interface CSLoginAPIManager : CSAPIManager

+ (NSString *)getLoginInfo:(void(^)(CSLoginInfoModel *loginInfo))success
             failure:(void(^)(NSError *error))error;

@end
