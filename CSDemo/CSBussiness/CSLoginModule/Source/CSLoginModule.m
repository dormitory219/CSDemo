//
//  CSLoginModule.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSLoginModule.h"
#import "CSLoginViewController.h"
#import "CSRegisterViewController.h"
#import "CSGroundAPI.h"
#import "CSNavigationController.h"
#import "CSLoginModuleAction.h"

@implementation CSLoginModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"login/login" jump:^(CSRouteRequest * _Nonnull request) {
        CSLoginViewController *vc = [[CSLoginViewController alloc] init];
        vc.routeRequest = request;
        UINavigationController *naviVc = [[CSNavigationController alloc] initWithRootViewController:vc];
        [request.fromVC presentViewController:naviVc animated:YES completion:^{
            
        }];
    }];
    
    [[CSRouter router] registerNativePath:@"login/register" jump:^(CSRouteRequest * _Nonnull request) {
        
        CSRegisterViewController *vc = [[CSRegisterViewController alloc] init];
        vc.routeRequest = request;
        if ([request.fromVC.navigationController isKindOfClass:[CSNavigationController class]]) {
            
            [request.fromVC.navigationController pushViewController:vc animated:YES];
        }
    }];
    
    
    [[CSRouter router] registerNativePath:@"login/login" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
       
        CSLoginViewController *loginVC = [[CSLoginViewController alloc] init];
        loginVC.routeRequest = request;
        return loginVC;
        
    }];
    
    [[CSRouter router] registerNativePath:@"login/register" view:^UIViewController *(CSRouteRequest * _Nonnull request) {
       
        CSRegisterViewController *registerVC = [[CSRegisterViewController alloc] init];
        registerVC.routeRequest = request;
        return registerVC;
        
    }];
}

- (void)doAction:(CSLoginModuleAction *)action
{
    if (!action) {
        return;
    }
    switch (action.APIType) {
            case CSLoginModuleActionAPITypeGetLoginData:
        {
            NSLog(@"fetching Data");
            action.callBack ? action.callBack(nil): nil;
        }
            break;
            
            case CSLoginModuleActionAPITypePush:
        {
            NSLog(@"go to do push");
            action.callBack ? action.callBack(nil): nil;
        }
            break;
            
        default:
            break;
    }
}

@end
