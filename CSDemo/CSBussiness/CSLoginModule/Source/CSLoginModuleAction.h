//
//  CSLoginModuleAction.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2019/3/13.
//  Copyright © 2019 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSCore.h"

typedef NS_ENUM(NSUInteger, CSLoginModuleActionAPIType) {
   CSLoginModuleActionAPITypeGetLoginData,
   CSLoginModuleActionAPITypePush
};

NS_ASSUME_NONNULL_BEGIN

@interface CSLoginModuleAction : NSObject<CSModuleActionProtocol>

@property (nonatomic,assign) CSLoginModuleActionAPIType APIType;

@property (nonatomic,copy) void (^callBack)(CSTransmissionData *data);
@end

NS_ASSUME_NONNULL_END
