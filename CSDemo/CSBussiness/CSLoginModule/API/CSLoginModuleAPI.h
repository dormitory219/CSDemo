//
//  API.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSLoginModuleAPI_h
#define CSLoginModuleAPI_h

#import "CSLoginInfoModel.h"
#import "CSLoginModuleAction.h"

#define LOGIN_ROUTER_LOGIN                 @"route://login/login"

#define LOGIN_ROUTER_REGISTER              @"route://login/register"

//登录模块存入内存中用来模块共享的数据
// 登录状态， Boolean类型
#define LOGIN_DATA_LOGIN                   @"login"
// 用户信息， Model类型 ， 具体结构 LoginUserInfoModelProtocol
#define LOGIN_DATA_USERINFO                @"userInfo"

#define LOGIN_MODULE_NAME                @"CSLoginModule"

#endif /* API_h */
