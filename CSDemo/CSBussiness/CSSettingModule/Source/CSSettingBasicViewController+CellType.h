//
//  CSSettingBasicViewController+CellType.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingBasicViewController.h"

@interface CSSettingBasicViewController (CellType)

//- (__kindof CSSettingBasicCell *)cellForType:(CSSettingCellType)type;

//- (__kindof CSSettingBasicCell *)cellForType:(__kindof CSSettingBasicCellModel *)cellModel;
//
//- (__kindof CSSettingHeaderView *)headerViewForType:(__kindof CSSettingSectionModel *)sectionModel;
//
//- (__kindof CSSettingFooterView *)footerViewForType:(__kindof CSSettingSectionModel *)sectionModel;

@end
