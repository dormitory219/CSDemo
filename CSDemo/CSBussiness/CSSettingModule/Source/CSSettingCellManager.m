//
//  CSSettingCellManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingCellManager.h"
#import "CSSettingBasicCell.h"

@interface CSSettingCellManager()

@property (nonatomic,strong) NSMutableDictionary <NSString *,NSMutableArray *> *allCellsMap;

@end

@implementation CSSettingCellManager

+ (instancetype)shareManager
{
    static dispatch_once_t onceToken;
    static CSSettingCellManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
        manager.allCellsMap = [NSMutableDictionary dictionaryWithCapacity:2];
    });
    return manager;
}

+ (BOOL)hasRegisterCell:(Class)cellCls viewController:(UIViewController *)viewController
{
    NSMutableDictionary *allCellsMap = [[self shareManager] allCellsMap];
    NSMutableArray *allCellClss = [allCellsMap valueForKey:NSStringFromClass([viewController class])];
    if ([allCellClss containsObject:cellCls]) {
        return YES;
    }
    return NO;
}

+ (void)registerCell:(Class)cellCls viewController:(UIViewController *)viewController
{
    NSMutableDictionary *allCellsMap = [[self shareManager] allCellsMap];
    NSMutableArray *allCellClss = [NSMutableArray arrayWithCapacity:2];
    [allCellClss addObject:cellCls];
    
    [allCellsMap setObject:allCellClss forKey:NSStringFromClass([viewController class])];
}

+ (void)clearRegisterCellWithViewController:(UIViewController *)viewController
{
    NSMutableDictionary *allCellsMap = [[self shareManager] allCellsMap];
    NSMutableArray *allCellClss = [allCellsMap valueForKey:NSStringFromClass([viewController class])];
    [allCellClss removeAllObjects];
}

@end
