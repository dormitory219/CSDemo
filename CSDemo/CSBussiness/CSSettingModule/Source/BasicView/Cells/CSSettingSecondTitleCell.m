//
//  CSSettingSecondTitleCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingSecondTitleCell.h"

@interface CSSettingSecondTitleCell()

@property (weak, nonatomic) IBOutlet UILabel *secondTiltLabel;

@end

@implementation CSSettingSecondTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingSecondTitleCellModel *)cellModel
{
    [super setCellModel:cellModel];
    self.secondTiltLabel.text = cellModel.secondTitle;
    self.secondTiltLabel.textColor = cellModel.secondTitleColor;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
