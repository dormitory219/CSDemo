//
//  CSSettingDefaultCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingDefaultCell.h"

@interface CSSettingDefaultCell()

@end

@implementation CSSettingDefaultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingDefaultCellModel *)cellModel
{
    [super setCellModel:cellModel];
    if (cellModel.trailText.length) {
        self.trailLabel.text = cellModel.trailText;
        self.trailLabel.hidden = NO;
    }else{
        self.trailLabel.hidden = YES;;
    }
    self.arrowButton.hidden = cellModel.hideAccessoryArrow;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
