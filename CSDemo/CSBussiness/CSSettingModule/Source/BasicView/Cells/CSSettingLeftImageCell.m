//
//  CSSettingLeftImageCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingLeftImageCell.h"

@interface CSSettingLeftImageCell ()

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;

@end

@implementation CSSettingLeftImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingLeftImageCellModel*)cellModel
{
    [super setCellModel:cellModel];
    self.leftImageView.image = [UIImage imageNamed:cellModel.imageName];
    if (cellModel.trailText.length) {
         self.trailLabel.text = cellModel.trailText;
         self.trailLabel.hidden = NO;
    }else{
        self.trailLabel.hidden = YES;;
    }
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
