//
//  CSSettingLeftImageSubTitleCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingLeftImageSubTitleCell.h"

@interface CSSettingLeftImageSubTitleCell ()

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@end

@implementation CSSettingLeftImageSubTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingLeftImageSubTitleCellModel*)cellModel
{
    [super setCellModel:cellModel];
    self.leftImageView.image = [UIImage imageNamed:cellModel.imageName];
    self.subTitleLabel.text = cellModel.subTitle;
    if (cellModel.trailText.length) {
        self.trailLabel.text = cellModel.trailText;
        self.trailLabel.hidden = NO;
    }else{
        self.trailLabel.hidden = YES;;
    }
    self.subTitleLabel.textColor = cellModel.subTitleColor;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCell *)model
{
    return 60.f;
}

@end
