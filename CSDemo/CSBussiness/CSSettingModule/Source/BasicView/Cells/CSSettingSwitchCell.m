//
//  CSSettingSwitchCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingSwitchCell.h"
#import "UIView+ViewController.h"

@interface CSSettingSwitchCell()

@property (weak, nonatomic) IBOutlet UIButton *vipIcon;

@property (weak, nonatomic) IBOutlet UISwitch *switchIcon;

@end

@implementation CSSettingSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchAction:(UISwitch *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(switchSlideIsOn:cell:)]) {
        [self.delegate switchSlideIsOn:sender.isOn cell:self];
    }
}

+ (instancetype)dequeueWithTable:(UITableView *)table
{
    NSString *cellIdentifier = [self cellIdentifier];
    if (!CSSETTINGCELLHASREGISTER(table.viewController)) {
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
        CSSETTINGCELLREGISTER(table.viewController)
    }
    CSSettingSwitchCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.delegate = (id<CSSettingSwitchSlideDelegate>)table.viewController;
    return cell;
}

- (void)setCellModel:(CSSettingSwitchCellModel *)cellModel
{
    [super setCellModel:cellModel];
    self.vipIcon.hidden = !cellModel.showIcon;
    self.switchIcon.on = cellModel.isOn;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
