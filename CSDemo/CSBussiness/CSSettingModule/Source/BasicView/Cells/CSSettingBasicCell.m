//
//  CSSettingBasicCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingBasicCell.h"
#import "CSSettingCellManager.h"
@implementation CSSettingBasicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

+ (instancetype)dequeueWithTable:(UITableView *)table
{
    NSString *cellIdentifier = [self cellIdentifier];
    if (!CSSETTINGCELLHASREGISTER(table.viewController)) {
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
        CSSETTINGCELLREGISTER(table.viewController);
    }
   
    CSSettingBasicCell *cell = [table dequeueReusableCellWithIdentifier:cellIdentifier];
    return cell;
}

- (void)setCellModel:(CSSettingBasicCellModel *)cellModel
{
    _cellModel = cellModel;
    self.titleLabel.text = cellModel.title;
    self.titleLabel.textColor = cellModel.titleColor;
    self.trailLabel.textColor = cellModel.trailColor;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    //must impl by subCls
    if ([model.cellCls respondsToSelector:@selector(heightForCellModel:)]) {
       return [model.cellCls heightForCellModel:model];
    }
    return 44.f;
}

@end
