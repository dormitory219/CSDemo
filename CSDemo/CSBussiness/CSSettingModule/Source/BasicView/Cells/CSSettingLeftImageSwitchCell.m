//
//  CSSettingLeftImageSwitchCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingLeftImageSwitchCell.h"

@interface CSSettingLeftImageSwitchCell()

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;

@end

@implementation CSSettingLeftImageSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingLeftImageCellModel*)cellModel
{
    [super setCellModel:cellModel];
    self.leftImageView.image = [UIImage imageNamed:cellModel.imageName];
    self.titleLabel.text = cellModel.title;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCell *)model
{
    return 60.f;
}

@end
