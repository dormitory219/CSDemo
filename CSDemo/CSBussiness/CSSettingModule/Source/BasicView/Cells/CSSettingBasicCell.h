//
//  CSSettingBasicCell.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSettingSectionModel.h"
#import "CSSettingCellManager.h"

@class CSSettingBasicCell;
@protocol CSSettingDelegate <NSObject>

@end

@interface CSSettingBasicCell : UITableViewCell

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *trailLabel;

@property (weak, nonatomic) IBOutlet UIButton *arrowButton;

@property (nonatomic,strong) __kindof CSSettingBasicCellModel *cellModel;

@property (nonatomic,weak) UIViewController *viewController;

+ (instancetype)dequeueWithTable:(UITableView *)table;

+ (NSString *)cellIdentifier;

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model;

@end
