//
//  CSSettingSwitchCell.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingBasicCell.h"

@class CSSettingSwitchCell;
@protocol CSSettingSwitchSlideDelegate <CSSettingDelegate>

- (void)switchSlideIsOn:(BOOL)isOn cell:(CSSettingSwitchCell *)cell;

@end

@interface CSSettingSwitchCell : CSSettingBasicCell

@property (nonatomic,weak) id<CSSettingSwitchSlideDelegate> delegate;

@end
