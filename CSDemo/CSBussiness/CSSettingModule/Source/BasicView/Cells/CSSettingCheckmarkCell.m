//
//  CSSettingCheckmarkCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingCheckmarkCell.h"

@implementation CSSettingCheckmarkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingCheckmarkCellModel*)cellModel
{
    [super setCellModel:cellModel];
    self.titleLabel.text = cellModel.title;
    if (cellModel.isSelected) {
        self.accessoryType =  UITableViewCellAccessoryCheckmark;
    }else{
        self.accessoryType =  UITableViewCellAccessoryNone;
    }
    self.tintColor = cellModel.checkmarkColor;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
