//
//  CSSettingSubTitleCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingSubTitleCell.h"

@interface CSSettingSubTitleCell()

@property (weak, nonatomic) IBOutlet UILabel *subTiltLabel;

@end

@implementation CSSettingSubTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(CSSettingSubTitleCellModel *)cellModel
{
    [super setCellModel:cellModel];
    self.subTiltLabel.text = cellModel.subTitle;
    self.subTiltLabel.textColor = cellModel.subTitleColor;
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCell *)model
{
    return 60.f;
}

@end
