//
//  CSSettingLoadingCell.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingLoadingCell.h"

@interface CSSettingLoadingCell()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end

@implementation CSSettingLoadingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (CGFloat)heightForCellModel:(__kindof CSSettingBasicCellModel *)model
{
    return 44;
}

@end
