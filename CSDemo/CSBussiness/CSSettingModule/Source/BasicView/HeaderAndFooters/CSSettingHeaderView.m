//
//  CSSettingHeaderView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingHeaderView.h"

@interface CSSettingHeaderView()

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSAttributedString *headerAttributeTitle;

@end

@implementation CSSettingHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerSelectAction:)];
    [self addGestureRecognizer:tapGesture];
}

+ (instancetype)header
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].lastObject;
}

+ (UIView *)viewForHeaderSectionModel:(__kindof CSSettingSectionModel *)sectionModel
{
    if (!sectionModel.headerTitle.length && !sectionModel.headerAttributeTitle.length) {
        return nil;
    }
    return [self header];
}

- (void)headerSelectAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerSelect:headerView:)]) {
        [self.delegate headerSelect:self.sectionModel headerView:self];
    }
}

- (void)setSectionModel:(CSSettingSectionModel *)sectionModel
{
    _sectionModel = sectionModel;
    if (sectionModel.headerTitle.length) {
        self.title = sectionModel.headerTitle;
    }
    if (sectionModel.headerAttributeTitle.length) {
        self.headerAttributeTitle = sectionModel.headerAttributeTitle;
    }
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLabel.text = title;
}

- (void)setHeaderAttributeTitle:(NSAttributedString *)headerAttributeTitle
{
    _headerAttributeTitle = headerAttributeTitle;
    self.titleLabel.attributedText = headerAttributeTitle;
}

//默认的header样式，只有标题样式，支持attribute,事件点击，无title时默认隐藏header，高度适应
+ (CGFloat)heightForHeaderSectionModel:(__kindof CSSettingSectionModel *)model
{
    //imp by subClass
    CGFloat height = 0.1f;
    if (model.headerTitle.length) {
        height =  [model.headerTitle boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 10*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17]} context:nil].size.height;
    }
    if (model.headerAttributeTitle.length) {
        height =  [model.headerAttributeTitle boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 10*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    }
    return height + 20;
}

@end
