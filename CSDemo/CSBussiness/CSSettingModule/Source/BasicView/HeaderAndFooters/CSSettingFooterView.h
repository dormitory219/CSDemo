//
//  CSSettingFooterView.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSettingSectionModel.h"
#import "CSSettingHeaderFooterBasicView.h"

@class CSSettingFooterView;
@protocol CSSettingFooterViewDelegate <NSObject>

- (void)footerSelect:(CSSettingSectionModel *)model footerView:(CSSettingFooterView *)footer;

@end

@interface CSSettingFooterView : CSSettingHeaderFooterBasicView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic,strong) CSSettingSectionModel *sectionModel;

@property (nonatomic,weak) id<CSSettingFooterViewDelegate>delegate;

+ (instancetype)footer;

+ (CGFloat)heightForFooterSectionModel:(__kindof CSSettingSectionModel *)model;


@end
