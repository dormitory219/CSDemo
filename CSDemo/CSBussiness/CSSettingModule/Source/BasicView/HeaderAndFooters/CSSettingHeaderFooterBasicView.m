//
//  CSSettingHeaderFooterBasicView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingHeaderFooterBasicView.h"

@implementation CSSettingHeaderFooterBasicView

//header,footer的基类，每种model绑定一种header,footer，根据model获取对应view

+ (CGFloat)heightForHeaderSectionModel:(__kindof CSSettingSectionModel *)model
{
    //must impl by subCls
    if ([model.headerCls respondsToSelector:@selector(heightForHeaderSectionModel:)]) {
        return [model.headerCls heightForHeaderSectionModel:model];
    }
    return 44.f;
}

+ (instancetype)viewForHeaderSectionModel:(__kindof CSSettingSectionModel *)model
{
    //must impl by subCls
    if ([model.headerCls respondsToSelector:@selector(viewForHeaderSectionModel:)]) {
        return [model.headerCls viewForHeaderSectionModel:model];
    }
    return nil;
}

+ (CGFloat)heightForFooterSectionModel:(__kindof CSSettingSectionModel *)model
{
    //must impl by subCls
    if ([model.footerCls respondsToSelector:@selector(heightForFooterSectionModel:)]) {
        return [model.footerCls heightForFooterSectionModel:model];
    }
    return 44.f;
}

+ (instancetype)viewForFooterSectionModel:(__kindof CSSettingSectionModel *)model
{
    //must impl by subCls
    if ([model.footerCls respondsToSelector:@selector(viewForFooterSectionModel:)]) {
        return [model.footerCls viewForFooterSectionModel:model];
    }
    return nil;
}

@end
