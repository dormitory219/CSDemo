//
//  CSSettingWifiPasswordFootView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingWifiPasswordFootView.h"

@implementation CSSettingWifiPasswordFootView

//特殊的footer样式，随意定制，高度自己业务方判定

+ (UIView *)viewForFooterSectionModel:(__kindof CSSettingSectionModel *)sectionModel
{
    return [self footer];
}

+ (CGFloat)heightForFooterSectionModel:(__kindof CSSettingSectionModel *)model
{
    return 120;
}
@end
