//
//  CSSettingHeaderView.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSettingSectionModel.h"
#import "CSSettingHeaderFooterBasicView.h"

@class CSSettingHeaderView;
@protocol CSSettingHeaderViewDelegate <NSObject>

- (void)headerSelect:(CSSettingSectionModel *)model headerView:(CSSettingHeaderView *)header;

@end

@interface CSSettingHeaderView : CSSettingHeaderFooterBasicView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic,strong) CSSettingSectionModel *sectionModel;

@property (nonatomic,weak) id<CSSettingHeaderViewDelegate>delegate;

+ (instancetype)header;

+ (CGFloat)heightForHeaderSectionModel:(__kindof CSSettingSectionModel *)model;

@end
