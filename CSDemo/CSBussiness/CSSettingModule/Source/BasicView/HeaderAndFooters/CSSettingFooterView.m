//
//  CSSettingFooterView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingFooterView.h"

@interface CSSettingFooterView()

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSAttributedString *footerAttributeTitle;

@end

@implementation CSSettingFooterView

- (void)awakeFromNib
{
    [super awakeFromNib];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(footerSelectAction:)];
    [self addGestureRecognizer:tapGesture];
}

+ (instancetype)footer
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].lastObject;
}

+ (UIView *)viewForFooterSectionModel:(__kindof CSSettingSectionModel *)sectionModel
{
    if (!sectionModel.footerTitle.length && !sectionModel.footerAttributeTitle.length) {
        return nil;
    }
    return [self footer];
}

- (void)footerSelectAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(footerSelect:footerView:)]) {
        [self.delegate footerSelect:self.sectionModel footerView:self];
    }
}

- (void)setSectionModel:(CSSettingSectionModel *)sectionModel
{
    _sectionModel = sectionModel;
    if (sectionModel.footerTitle.length) {
        self.title = sectionModel.footerTitle;
    };
    if (sectionModel.footerAttributeTitle.length) {
        self.footerAttributeTitle = sectionModel.footerAttributeTitle;
    }
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    if (title.length) {
      self.titleLabel.text = title;
    }
}

- (void)setFooterAttributeTitle:(NSAttributedString *)footerAttributeTitle
{
    _footerAttributeTitle = footerAttributeTitle;
    if (_footerAttributeTitle.length) {
        self.titleLabel.attributedText = footerAttributeTitle;
    }
}

//默认的footer样式，只有标题样式，支持attribute,事件点击，无title时默认隐藏footer，高度适应
+ (CGFloat)heightForFooterSectionModel:(__kindof CSSettingSectionModel *)model
{
    CGFloat height = 0.1f;
    if (!model.footerTitle.length && !model.footerAttributeTitle.length) {
        return height;
    }
    if (model.footerTitle.length) {
        height = [model.footerTitle boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 10*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]} context:nil].size.height;
    }
    if (model.footerAttributeTitle.length) {
        height =  [model.footerAttributeTitle boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 10*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    }
    return height + 20;
}

@end
