//
//  CSSettingHeaderFooterBasicView.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSettingSectionModel.h"

@interface CSSettingHeaderFooterBasicView : UIView

+ (CGFloat)heightForHeaderSectionModel:(__kindof CSSettingSectionModel *)model;

+ (instancetype)viewForHeaderSectionModel:(__kindof CSSettingSectionModel *)model;

+ (CGFloat)heightForFooterSectionModel:(__kindof CSSettingSectionModel *)model;

+ (instancetype)viewForFooterSectionModel:(__kindof CSSettingSectionModel *)model;

@end
