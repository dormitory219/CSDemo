//
//  CSSettingDefaultCellModel.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSBasicModel.h"
#import <UIKit/UIKit.h>


//每种model绑定一种cell，根据model的cellCls获取对应cell

@interface CSSettingBasicCellModel : CSBasicModel
//主标题
@property (nonatomic,copy,nullable) NSString *title;
//主标题自定义颜色
@property (nonatomic,strong,nullable) UIColor *titleColor;
//尾部标题
@property (nonatomic,copy,nullable) NSString *trailText;
//尾部标题自定义颜色
@property (nonatomic,strong,nullable) UIColor *trailColor;
//cell选中事件
@property (nonnull,assign) SEL selector;

//绑定的cellClass
@property (nonatomic,strong,nonnull) Class cellCls;

//是否可显示右箭头
@property (nonatomic, assign) BOOL hideAccessoryArrow;

//@property (nonatomic,assign) CSSettingCellType cellType;

@end


//默认格式：左主标题，右边箭头,对应 CSSettingDefaultCell
@interface CSSettingDefaultCellModel : CSSettingBasicCellModel

@end

//subTitle格式：左主标题，下方副标题，右边箭头，对应 CSSettingSubTitleCell
@interface CSSettingSubTitleCellModel : CSSettingBasicCellModel
//副标题
@property (nonatomic,copy,nonnull) NSString *subTitle;
//副标题自定义颜色
@property (nonatomic,strong,nonnull) UIColor *subTitleColor;

@end

//secondTitle格式：左主标题，紧挨着二级小标题，右边箭头，对应 CSSettingSecondTitleCell
@interface CSSettingSecondTitleCellModel : CSSettingBasicCellModel

//二级标题
@property (nonatomic,copy,nonnull) NSString *secondTitle;
//二级标题自定义颜色
@property (nonatomic,strong,nonnull) UIColor *secondTitleColor;

@end

//右侧switch格式：左主标题，右边switch，对应 CSSettingSwitchCell
@interface CSSettingSwitchCellModel : CSSettingBasicCellModel
//slide 对应的action事件
@property (nonnull,assign) SEL sliderSelector;
//是否显示vip标记
@property (nonatomic,assign) BOOL showIcon;
//slide开关状态
@property (nonatomic,assign) BOOL isOn;

@end

//leftImage格式：左图标，紧挨着主标题，右边箭头，对应 CSSettingLeftImageCell
@interface CSSettingLeftImageCellModel : CSSettingBasicCellModel

@property (nonatomic,copy,nonnull) NSString *imageName;

@end

//leftImage格式：左图标，紧挨着主标题，主标题下副标题，对应 CSSettingLeftImageSubTitleCell
@interface CSSettingLeftImageSubTitleCellModel : CSSettingBasicCellModel

@property (nonatomic,copy,nonnull) NSString *imageName;
//二级标题
@property (nonatomic,copy,nonnull) NSString *subTitle;
//二级标题自定义颜色
@property (nonatomic,strong,nonnull) UIColor *subTitleColor;

@end

//leftImage格式：左图标，紧挨着主标题，右边switch，对应 CSSettingLeftImageSwitchCell
@interface CSSettingLeftImageSwitchCellModel : CSSettingSwitchCellModel

@property (nonatomic,copy,nonnull) NSString *imageName;

@end

@interface CSSettingLoadingCellModel : CSSettingBasicCellModel

@end

//单选，多选checkmark样式
@interface CSSettingCheckmarkCellModel : CSSettingBasicCellModel

//cell选中状态
@property (nonatomic,assign) BOOL isSelected;
//checkmark自定义颜色
@property (nonatomic,strong,nonnull) UIColor *checkmarkColor;

@end

@interface CSSettingGroupCellModel : CSSettingBasicCellModel


@end

