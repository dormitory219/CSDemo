//
//  CSSettingModule.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingModule.h"
#import "CSSettingMainViewController.h"
#import "CSNavigationController.h"

@implementation CSSettingModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"setting/main" jump:^(CSRouteRequest * _Nonnull request) {
        
        CSSettingMainViewController *vc = [[CSSettingMainViewController alloc] init];
        UINavigationController *naviVc = [[CSNavigationController alloc] initWithRootViewController:vc];
        vc.routeRequest = request;
        [request.fromVC presentViewController:naviVc animated:YES completion:nil];
        
    }];
}

- (void)doAction:(id<CSModuleActionProtocol>)action
{
    
}

@end
