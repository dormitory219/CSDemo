//
//  CSSettingSectionModel.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingSectionModel.h"
#import "CSSettingViewHeader.h"

@implementation CSSettingSectionModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.headerCls = [CSSettingHeaderView class];
        self.footerCls = [CSSettingFooterView class];
    }
    return self;
}

@end

@implementation CSSettingWifiPasswordSectionModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.headerCls = [CSSettingHeaderView class];
        self.footerCls = [CSSettingWifiPasswordFootView class];
    }
    return self;
}

@end


