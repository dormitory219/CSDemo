//
//  CSSettingSectionModel.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSBasicModel.h"
#import "CSSettingCellModel.h"


//每种model绑定一种header,footer，根据model的headerCls，footerCls获取对应view

@class CSSettingDefaultCellModel;
@interface CSSettingSectionModel : CSBasicModel

@property (nonatomic,copy) NSString * _Nullable headerTitle;
@property (nonatomic,assign) SEL _Nullable headerSelectSelector;
@property (nonatomic,copy) NSAttributedString * _Nullable headerAttributeTitle;
@property (nonatomic,copy) NSString * _Nullable footerTitle;
@property (nonatomic,copy) NSAttributedString * _Nullable footerAttributeTitle;
@property (nonatomic,assign) SEL _Nonnull footerSelectSelector;
@property (nonatomic,assign) BOOL canMultipleSelect;

//绑定的headerClass
@property (nonatomic,strong,nonnull) Class headerCls;

//绑定的footerClass
@property (nonatomic,strong,nonnull) Class footerCls;

@property (nonatomic,strong) NSArray <__kindof CSSettingBasicCellModel *> * _Nullable cellModels;

@end


//自定义sectionModel,有特殊样式的footer
@interface CSSettingWifiPasswordSectionModel : CSSettingSectionModel

@end





