//
//  CSSettingDefaultCellModel.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingCellModel.h"
#import "CSSettingViewHeader.h"
#import "CSGroundAPI.h"

@implementation CSSettingBasicCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.titleColor = UIColorFromRGB(0x04211D);
        self.trailColor = UIColorFromRGB(0x686868);
    }
    return self;
}

@end

@implementation CSSettingDefaultCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingDefaultCell class];
    }
    return self;
}

@end

@implementation CSSettingSubTitleCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingSubTitleCell class];
        self.subTitleColor = UIColorFromRGB(0x686868);
    }
    return self;
}

@end

@implementation CSSettingSecondTitleCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingSecondTitleCell class];
        self.secondTitleColor = UIColorFromRGB(0x686868);
    }
    return self;
}

@end

@implementation CSSettingSwitchCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingSwitchCell class];
    }
    return self;
}

@end

@implementation CSSettingLeftImageCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingLeftImageCell class];
    }
    return self;
}

@end

@implementation CSSettingLeftImageSubTitleCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingLeftImageSubTitleCell class];
        self.subTitleColor = UIColorFromRGB(0x686868);
    }
    return self;
}

@end


@implementation CSSettingLeftImageSwitchCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingLeftImageSwitchCell class];
    }
    return self;
}

@end

@implementation CSSettingLoadingCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingLoadingCell class];
    }
    return self;
}

@end

@implementation CSSettingCheckmarkCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingCheckmarkCell class];
        self.checkmarkColor = [UIColor blueColor];
    }
    return self;
}

@end

@implementation CSSettingGroupCellModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellCls = [CSSettingGroupCell class];
    }
    return self;
}

@end

