//
//  CSSettingDocScanViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingDocScanViewController.h"

@interface CSSettingDocScanViewController ()

@end

@implementation CSSettingDocScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"文档扫描";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scanQuality
{
    NSLog(@"scanQuality");
}

- (void)ocrLanguage
{
    NSLog(@"ocrLanguage");
}

- (void)antiShake
{
    NSLog(@"antiShakeSelect");
}

- (void)antiShakeSlide:(NSNumber *)isOn
{
    NSLog(@"antiShakeSlide:%ld",isOn.integerValue);
}

- (void)saveToAblum
{
    NSLog(@"saveToAblumSelect");
}

- (void)saveToAblumSlide:(NSNumber *)isOn
{
    NSLog(@"saveToAblumSlide:%ld",isOn.integerValue);
}

- (void)launchTakePhoto
{
    NSLog(@"launchTakePhotoSelect");
}

- (void)launchTakePhotoSlide:(NSNumber *)isOn
{
    NSLog(@"launchTakePhotoSlide:%ld",isOn.integerValue);
}

- (void)singleTakePhoto
{
    NSLog(@"singleTakePhoto");
}

- (void)multipleTakePhoto
{
    NSLog(@"multipleTakePhotoSelect");
}

- (void)multipleTakePhotoSlide:(NSNumber *)isOn
{
    NSLog(@"multipleTakePhoto:%ld",isOn.integerValue);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *sectionModels = [NSMutableArray arrayWithCapacity:1];
    
        //section1
        CSSettingSectionModel *sectionModel = [CSSettingSectionModel new];
        NSMutableArray *sections = [NSMutableArray arrayWithCapacity:1];
        __kindof CSSettingBasicCellModel *cellModel = [CSSettingLeftImageCellModel new];
        cellModel.title = @"扫描质量与大小";
        cellModel.hideAccessoryArrow = YES;
        ((CSSettingLeftImageCellModel *)cellModel).imageName = @"au_scanner_en";
        cellModel.selector = @selector(scanQuality);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"OCR识别语言";
        cellModel.selector = @selector(ocrLanguage);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingLeftImageSwitchCellModel new];
        cellModel.title = @"防抖拍摄";
        ((CSSettingLeftImageSwitchCellModel *)cellModel).imageName = @"feedback_bg";
        cellModel.selector = @selector(antiShake);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(antiShakeSlide:);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"自动保存到相册";
        cellModel.selector = @selector(saveToAblum);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(saveToAblumSlide:);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"启动就拍照";
        cellModel.selector = @selector(launchTakePhoto);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(launchTakePhotoSlide:);
        [sections addObject:cellModel];
        
        sectionModel.cellModels = sections;
        [sectionModels addObject:sectionModel];
        
        //section2
        sectionModel = [CSSettingSectionModel new];
        NSMutableArray *sections2 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"单拍设置";
        cellModel.selector = @selector(singleTakePhoto);
        [sections2 addObject:cellModel];
        
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"多拍设置";
        ((CSSettingSwitchCellModel *)cellModel).isOn = YES;
        cellModel.selector = @selector(multipleTakePhoto);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(multipleTakePhotoSlide:);
        [sections2 addObject:cellModel];

        sectionModel.cellModels = sections2;
        [sectionModels addObject:sectionModel];
        
        self.sectionModels = sectionModels;
    }
    return self;
}

@end
