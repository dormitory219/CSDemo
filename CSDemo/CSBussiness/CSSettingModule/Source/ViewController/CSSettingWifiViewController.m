//
//  CSSettingWifiViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/5.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingWifiViewController.h"

@interface CSSettingWifiViewController ()

@end

@implementation CSSettingWifiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"wifi热点";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)personWifiSlide:(NSNumber *)isOn
{
    NSLog(@"personWifiSlide:%ld",isOn.integerValue);
}

- (void)wifiPassword
{
    NSLog(@"wifiPassword");
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *sectionModels = [NSMutableArray arrayWithCapacity:1];
        
        //section1
        CSSettingSectionModel *sectionModel = [CSSettingSectionModel new];
        sectionModel.footerTitle = @"打开个人热点以共享iPhone的互联网连接，这可能要求您支付额外费用。已登陆您iCloud账户的其他设备无需手动打开即可食用个人热点";
        NSMutableArray *section1 = [NSMutableArray arrayWithCapacity:1];
        __kindof CSSettingBasicCellModel *cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"自动上传";
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(personWifiSlide:);
        [section1 addObject:cellModel];
        
        sectionModel.cellModels = section1;
        [sectionModels addObject:sectionModel];
        
        //section2
        sectionModel = [CSSettingWifiPasswordSectionModel new];
        NSMutableArray *section2 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"Wi-Fi密码";
        cellModel.trailText = @"19902901";
        cellModel.selector = @selector(wifiPassword);
        [section2 addObject:cellModel];

        sectionModel.cellModels = section2;
        [sectionModels addObject:sectionModel];
        
        self.sectionModels = sectionModels;
    }
    return self;
}

@end
