//
//  CSSettingMainViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingMainViewController.h"
#import "CSSettingDocScanViewController.h"
#import "CSSettingDocManagerViewController.h"
#import "CSSettingDocExportViewController.h"
#import "CSSettingWifiViewController.h"
#import "CSGroundAPI.h"

@interface CSSettingMainViewController ()

@property (nonatomic,strong) CSSettingSectionModel *receiveSection;

@end

@implementation CSSettingMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"thanksgiving_close"] style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma selector

- (void)headerSelectSelector:(CSSettingSectionModel *)model
{
    NSLog(@"headerSelect:%ld",[self.sectionModels indexOfObject:model]);
}

- (void)footerSelectSelector:(CSSettingSectionModel *)model
{
    NSLog(@"footerSelect:%ld",[self.sectionModels indexOfObject:model]);
}

- (void)group
{
    NSLog(@"group");
}

- (void)loginAndRegister
{
    NSLog(@"loginAndRegister");
}

- (void)docScan
{
    NSLog(@"docScan");
    CSSettingDocScanViewController *docScanVc = [[CSSettingDocScanViewController alloc] init];
    [self.navigationController pushViewController:docScanVc animated:YES];
}

- (void)docManager
{
    NSLog(@"docManager");
    CSSettingDocManagerViewController *docManagerVc = [[CSSettingDocManagerViewController alloc] init];
    [self.navigationController pushViewController:docManagerVc animated:YES];
}

- (void)docExport
{
    NSLog(@"docExport");
    CSSettingDocExportViewController *docExportVc = [[CSSettingDocExportViewController alloc] init];
    [self.navigationController pushViewController:docExportVc animated:YES];
}

- (void)backup
{
    NSLog(@"backup");
}

- (void)cleanSpace
{
    NSLog(@"cleanSpace");
}

- (void)help
{
    NSLog(@"help");
}

- (void)about
{
    NSLog(@"about");
}

- (void)wifi
{
    CSSettingWifiViewController *wifiViewController = [[CSSettingWifiViewController alloc] init];
    [self.navigationController pushViewController:wifiViewController animated:YES];
}

- (void)suggestAndFeedback
{
    NSLog(@"suggestAndFeedback");
}

- (void)deviceSpace:(NSIndexPath *)indexPath
{
    NSLog(@"deviceSpace:%ld",indexPath.row);
}

- (void)receiveClose:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"receiveClose,selected:%d",model.isSelected);
    self.receiveSection.footerTitle = nil;
    NSInteger section = 0;
    if (!model.isSelected) {
        self.receiveSection.footerTitle = nil;
        self.receiveSection.footerAttributeTitle = nil;
    }else{
        section = [self.sectionModels indexOfObject:self.receiveSection];
        NSString *title = @"打开接收关闭后，会对当前产生影响,如果当前用户不喜欢该设置，可使用其他设置来进行随意更改，具体操作请参考。";
        NSString *middleTitle = @"会对当前产生影响";
        NSRange range = [title rangeOfString:middleTitle];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor grayColor]}];
        
        [attributeString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:range];
        [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        self.receiveSection.footerAttributeTitle = attributeString;
    }

    [self.table reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)onlyContact:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"onlyContact,selected:%d",model.isSelected);
    self.receiveSection.footerAttributeTitle = nil;
    NSInteger section = 0;
    if (!model.isSelected) {
        self.receiveSection.footerTitle = nil;
        self.receiveSection.footerAttributeTitle = nil;
    }else{
        section = [self.sectionModels indexOfObject:self.receiveSection];
        self.receiveSection.footerTitle = @"打开仅限联系人后，会对当前产生影响";
    }

    [self.table reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)all:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"all,selected:%d",model.isSelected);
    self.receiveSection.footerAttributeTitle = nil;
    NSInteger section = 0;
    if (!model.isSelected) {
        self.receiveSection.footerTitle = nil;
        self.receiveSection.footerAttributeTitle = nil;
    }else{
        section = [self.sectionModels indexOfObject:self.receiveSection];
        self.receiveSection.footerTitle = @"打开所有后，不会对当前产生影响";
    }
    [self.table reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)receiveClose1:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"receiveClose1,selected:%d",model.isSelected);
}

- (void)onlyContact1:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"onlyContact1,selected:%d",model.isSelected);
}

- (void)all1:(CSSettingCheckmarkCellModel *)model
{
    NSLog(@"all1,selected:%d",model.isSelected);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *sectionModels = [NSMutableArray arrayWithCapacity:1];
        
        //section0
        CSSettingSectionModel *sectionModel = [CSSettingSectionModel new];
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        __kindof CSSettingBasicCellModel *cellModel = [CSSettingGroupCellModel new];
        cellModel.title = @"登录／注册";
        cellModel.selector = @selector(group);
        
        sectionModel.cellModels = @[cellModel];
        [sectionModels addObject:sectionModel];
        
        //section1
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"扫描全能王账号";
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"登录／注册";
        cellModel.hideAccessoryArrow = YES;
        cellModel.selector = @selector(loginAndRegister);
        
        sectionModel.cellModels = @[cellModel];
        [sectionModels addObject:sectionModel];
        
        //section2
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"通用设置";
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        sectionModel.footerSelectSelector = @selector(footerSelectSelector:);
        NSMutableArray *sections = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"文档扫描";
        cellModel.titleColor = UIColorFromRGB(0x3f9f01);
        cellModel.selector = @selector(docScan);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingLeftImageCellModel new];
        cellModel.title = @"文档管理";
        ((CSSettingLeftImageCellModel *)cellModel).imageName = @"au_scanner_en";
        cellModel.selector = @selector(docManager);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"文档导出";
        cellModel.selector = @selector(docExport);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"安全备份";
        cellModel.selector = @selector(backup);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"清除存储空间";
        cellModel.selector = @selector(cleanSpace);
        cellModel.trailText = @"未连接";
        [sections addObject:cellModel];
        
        sectionModel.cellModels = sections;
        [sectionModels addObject:sectionModel];
        
        //section3
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        sectionModel.footerSelectSelector = @selector(footerSelectSelector:);
        NSMutableArray *sections3 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"帮助";
        cellModel.selector = @selector(help);
        [sections3 addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"建议与反馈";
        cellModel.trailText = @"关闭";
        cellModel.trailColor = UIColorFromRGB(0xFF3CFF);
        cellModel.selector = @selector(suggestAndFeedback);
        [sections3 addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"关于";
        cellModel.hideAccessoryArrow = YES;
        cellModel.selector = @selector(about);
        [sections3 addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"个人热点";
        cellModel.selector = @selector(wifi);
        [sections3 addObject:cellModel];
        
        sectionModel.cellModels = sections3;
        [sectionModels addObject:sectionModel];
        
        //section4
        CSSettingSectionModel *sectionModel4 = [CSSettingSectionModel new];
        sectionModel4.headerSelectSelector = @selector(headerSelectSelector:);
        sectionModel.footerSelectSelector = @selector(footerSelectSelector:);
        sectionModel4.headerTitle = @"使用存储空间";
        
        cellModel = [CSSettingLoadingCellModel new];
        sectionModel4.cellModels = @[cellModel];
        [sectionModels addObject:sectionModel4];
        

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
            NSMutableArray *sections = [NSMutableArray arrayWithCapacity:2];
            CSSettingDefaultCellModel * cellModel = [CSSettingDefaultCellModel new];
            cellModel.selector = @selector(deviceSpace:);
            cellModel.title = @"QQ";
            [sections addObject:cellModel];
            
            cellModel = [CSSettingDefaultCellModel new];
            cellModel.title = @"WeiXin";
            cellModel.selector = @selector(deviceSpace:);
            [sections addObject:cellModel];
            
            cellModel = [CSSettingDefaultCellModel new];
            cellModel.title = @"AiQiYi";
            cellModel.selector = @selector(deviceSpace:);
            [sections addObject:cellModel];
            
            cellModel = [CSSettingDefaultCellModel new];
            cellModel.title = @"DouYin";
            cellModel.selector = @selector(deviceSpace:);
            [sections addObject:cellModel];

            NSInteger index = [self.sectionModels indexOfObject:sectionModel4];
            CSSettingSectionModel *section = self.sectionModels[index];
            section.cellModels = sections;
            [self.table reloadSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationFade];
        });
        
        //section5
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"隔空投送(单选)";
        sectionModel.footerTitle = @"打开仅限联系人后，会对当前产生影响";
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        sectionModel.footerSelectSelector = @selector(footerSelectSelector:);
        NSMutableArray *sections5 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"接收关闭";
        cellModel.selector = @selector(receiveClose:);
        [sections5 addObject:cellModel];
        
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"仅限联系人";
        cellModel.selector = @selector(onlyContact:);
        ((CSSettingCheckmarkCellModel *)cellModel).isSelected = YES;
        [sections5 addObject:cellModel];
        
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"所有人";
        cellModel.selector = @selector(all:);
        [sections5 addObject:cellModel];
        
        self.receiveSection = sectionModel;
        sectionModel.cellModels = sections5;
        [sectionModels addObject:sectionModel];
        
        //section6
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerSelectSelector = @selector(headerSelectSelector:);
        sectionModel.footerSelectSelector = @selector(footerSelectSelector:);
        NSString *mainTitle = @"隔空投送(多选)\n\n";
        NSMutableAttributedString *mainAttributeString = [[NSMutableAttributedString alloc] initWithString:mainTitle attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],NSForegroundColorAttributeName:[UIColor redColor]}];
        NSString *subTitle = @"该选区没多大作用，只是为了区分上一个分区，实现多选功能。";
        NSMutableAttributedString *subAttributeString = [[NSMutableAttributedString alloc] initWithString:subTitle attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor orangeColor]}];
        NSMutableAttributedString *appString = [[NSMutableAttributedString alloc] init];
        [appString appendAttributedString:mainAttributeString];
        [appString appendAttributedString:subAttributeString];
        sectionModel.headerAttributeTitle = [appString copy];
        sectionModel.canMultipleSelect = YES;
        NSMutableArray *sections6 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"接收关闭1";
         ((CSSettingCheckmarkCellModel *)cellModel).checkmarkColor = [UIColor greenColor];
        cellModel.selector = @selector(receiveClose1:);
        [sections6 addObject:cellModel];
        
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"仅限联系人1";
        cellModel.selector = @selector(onlyContact1:);
        ((CSSettingCheckmarkCellModel *)cellModel).isSelected = YES;
         ((CSSettingCheckmarkCellModel *)cellModel).checkmarkColor = [UIColor greenColor];
        [sections6 addObject:cellModel];
        
        cellModel = [CSSettingCheckmarkCellModel new];
        cellModel.title = @"所有人1";
        ((CSSettingCheckmarkCellModel *)cellModel).checkmarkColor = [UIColor greenColor];
        cellModel.selector = @selector(all1:);
        [sections6 addObject:cellModel];
        
        sectionModel.cellModels = sections6;
        [sectionModels addObject:sectionModel];
        
        self.sectionModels = [NSMutableArray arrayWithCapacity:2];
        [self.sectionModels addObjectsFromArray:sectionModels];
    }
    return self;
}

@end
