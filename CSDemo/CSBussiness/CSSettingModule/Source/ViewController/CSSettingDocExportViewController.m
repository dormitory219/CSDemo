//
//  CSSettingDocExportViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingDocExportViewController.h"

@interface CSSettingDocExportViewController ()

@end

@implementation CSSettingDocExportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"文档导出";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)switchSlideIsOn:(BOOL)isOn cell:(CSSettingSwitchCell *)cell
//{
//    
//}

- (void)autoUpload
{
    NSLog(@"autoUpload");
}

- (void)uploadAccount
{
    NSLog(@"uploadAccount");
}

- (void)exportItunes
{
    NSLog(@"exportItunes");
}

- (void)uploadToPC
{
    NSLog(@"uploadToPC");
}

- (void)emailToSelf
{
    NSLog(@"emailToSelf");
}

- (void)email
{
    NSLog(@"email");
}

- (void)uploadRecord
{
    NSLog(@"uploadRecord");
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *sectionModels = [NSMutableArray arrayWithCapacity:1];
        
        //section1
        CSSettingSectionModel *sectionModel = [CSSettingSectionModel new];
        NSMutableArray *sections = [NSMutableArray arrayWithCapacity:1];
        __kindof CSSettingBasicCellModel *cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"自动上传";
        cellModel.selector = @selector(autoUpload);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"上传账号";
        cellModel.selector = @selector(uploadAccount);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"导出PDF到iTunes共享";
        cellModel.selector = @selector(exportItunes);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"传文档到电脑";
        cellModel.selector = @selector(uploadToPC);
    
        [sections addObject:cellModel];
        
        cellModel = [CSSettingSubTitleCellModel new];
        cellModel.title = @"邮件发送给自己";
        ((CSSettingSubTitleCellModel *)cellModel).subTitle = @"填写我的邮箱";
        cellModel.selector = @selector(emailToSelf);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingSubTitleCellModel new];
        cellModel.title = @"邮件发送";
        ((CSSettingSubTitleCellModel *)cellModel).subTitle = @"填写邮箱";
        cellModel.selector = @selector(email);
        [sections addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"上传／打印／传真记录";
        cellModel.selector = @selector(uploadRecord);
    
        [sections addObject:cellModel];
        
        sectionModel.cellModels = sections;
        [sectionModels addObject:sectionModel];
        
        self.sectionModels = sectionModels;
    }
    return self;
}

@end
