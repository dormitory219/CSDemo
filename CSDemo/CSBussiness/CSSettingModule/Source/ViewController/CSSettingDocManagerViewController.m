//
//  CSSettingDocManagerViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingDocManagerViewController.h"

@interface CSSettingDocManagerViewController ()

@end

@implementation CSSettingDocManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"文档管理";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)newDoc
{
    NSLog(@"newDoc");
}

- (void)showPhotoSize
{
    NSLog(@"showPhotoSizeSelect");
}

- (void)showPhotoSizeSlide:(NSNumber *)isOn
{
    NSLog(@"showPhotoSizeSlide:%ld",isOn.integerValue);
}

- (void)alwaysShowTag
{
    NSLog(@"alwaysShowTagSelect");
}

- (void)alwaysShowTagSlide:(NSNumber *)isOn
{
    NSLog(@"alwaysShowTagSlide:%ld",isOn.integerValue);
}

- (void)resetWarning
{
    NSLog(@"resetWarning");
}

- (void)singleTake
{
    NSLog(@"singleTake");
}

- (void)enhance
{
    NSLog(@"enhance");
}

- (void)pdfSetting
{
    NSLog(@"pdfSetting");
}

- (void)pdfNote
{
    NSLog(@"pdfNoteSelect");
}
- (void)pdfNoteSlide:(NSNumber *)isOn
{
    NSLog(@"pdfNoteSlide:%ld",isOn.integerValue);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *sectionModels = [NSMutableArray arrayWithCapacity:1];
        
        //section1
        CSSettingSectionModel *sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"新文档命名方式";
        NSMutableArray *sections = [NSMutableArray arrayWithCapacity:1];
        __kindof CSSettingBasicCellModel *cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"新文档";
        cellModel.selector = @selector(newDoc);
        [sections addObject:cellModel];
    
        sectionModel.cellModels = sections;
        [sectionModels addObject:sectionModel];
        
        //section2
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"显示设置";
        sectionModel.footerTitle = @"无论iPhone是否锁定，都会显示通知预览";
        NSMutableArray *sections2 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"显示图片的创建日期及大小";
        cellModel.selector = @selector(showPhotoSize);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(showPhotoSizeSlide:);
        [sections2 addObject:cellModel];
        
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"始终显示标签栏";
        cellModel.selector = @selector(alwaysShowTag);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(alwaysShowTagSlide:);
        ((CSSettingSwitchCellModel *)cellModel).showIcon = YES;
        ((CSSettingSwitchCellModel *)cellModel).isOn = YES;
        [sections2 addObject:cellModel];
        
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"重置所有警告信息";
        cellModel.selector = @selector(resetWarning);
        [sections2 addObject:cellModel];
        
        cellModel = [CSSettingLeftImageCellModel new];
        cellModel.title = @"单拍设置";
        ((CSSettingLeftImageCellModel *)cellModel).imageName = @"au_scanner_en";
        ((CSSettingLeftImageCellModel *)cellModel).trailText = @"关闭";
        cellModel.selector = @selector(singleTake);
        [sections2 addObject:cellModel];
        
        cellModel = [CSSettingLeftImageSubTitleCellModel new];
        cellModel.title = @"增强重置";
        ((CSSettingLeftImageSubTitleCellModel *)cellModel).subTitle = @"重置信息";
        ((CSSettingLeftImageCellModel *)cellModel).imageName = @"feedback_bg";
        cellModel.selector = @selector(enhance);
        [sections2 addObject:cellModel];
        
        sectionModel.cellModels = sections2;
        [sectionModels addObject:sectionModel];
        
        //section3
        sectionModel = [CSSettingSectionModel new];
        sectionModel.headerTitle = @"PDF设置";
        sectionModel.footerTitle = @"当该选项启动时，生成的PDF中将带有您添加的备注";
        NSMutableArray *sections3 = [NSMutableArray arrayWithCapacity:1];
        cellModel = [CSSettingDefaultCellModel new];
        cellModel.title = @"PDF尺寸";
        cellModel.selector = @selector(pdfSetting);
        [sections3 addObject:cellModel];
        
        cellModel = [CSSettingSwitchCellModel new];
        cellModel.title = @"带备注的PDF";
        cellModel.selector = @selector(pdfNote);
        ((CSSettingSwitchCellModel *)cellModel).sliderSelector = @selector(pdfNoteSlide:);
        ((CSSettingSwitchCellModel *)cellModel).isOn = YES;
        [sections3 addObject:cellModel];
        
        sectionModel.cellModels = sections3;
        [sectionModels addObject:sectionModel];
        
        self.sectionModels = sectionModels;
    }
    return self;
}

@end
