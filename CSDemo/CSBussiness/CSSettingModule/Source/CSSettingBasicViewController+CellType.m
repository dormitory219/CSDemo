//
//  CSSettingBasicViewController+CellType.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingBasicViewController+CellType.h"
#import "CSSettingViewHeader.h"

@implementation CSSettingBasicViewController (CellType)

//- (__kindof CSSettingBasicCell *)cellForType:(CSSettingCellType)type
//{
//    __kindof CSSettingBasicCell *cell = nil;
//    switch (type) {
//        case CSSettingCellTypeDefault:
//            cell = [CSSettingDefaultCell dequeueWithTable:self.table];
//            break;
//
//        case CSSettingCellTypeSwitch:
//            cell = [CSSettingSwitchCell dequeueWithTable:self.table];
//            ((CSSettingSwitchCell *)cell).delegate = self;
//            break;
//
//        case CSSettingCellTypeSubTitle:
//            cell = [CSSettingSubTitleCell dequeueWithTable:self.table];
//            break;
//
//        case CSSettingCellTypeSeondTitle:
//            cell = [CSSettingSecondTitleCell dequeueWithTable:self.table];
//            break;
//
//        case CSSettingCellTypeLeftImage:
//            cell = [CSSettingLeftImageCell dequeueWithTable:self.table];
//            break;
//
//        case CSSettingCellTypeLeftImageSubTitle:
//            cell = [CSSettingLeftImageSubTitleCell dequeueWithTable:self.table];
//            break;
//
//        case CSSettingCellTypeLeftImageSwitch:
//            cell = [CSSettingLeftImageSwitchCell dequeueWithTable:self.table];
//            ((CSSettingSwitchCell *)cell).delegate = self;
//            break;
//
//        default:
//            break;
//    }
//    return cell;
//}



//- (__kindof CSSettingBasicCell *)cellForType:(__kindof CSSettingBasicCellModel *)cellModel
//{
//    __kindof CSSettingBasicCell *cell = nil;
//    if ([cellModel isKindOfClass:[CSSettingDefaultCellModel class]]) {
//         cell = [CSSettingDefaultCell dequeueWithTable:self.table];
//    }else if ([cellModel isKindOfClass:[CSSettingSwitchCellModel class]]){
//        cell = [CSSettingSwitchCell dequeueWithTable:self.table];
//        ((CSSettingSwitchCell *)cell).delegate = self;
//    }else if ([cellModel isKindOfClass:[CSSettingSubTitleCellModel class]]){
//        cell = [CSSettingSubTitleCell dequeueWithTable:self.table];
//    }else if ([cellModel isKindOfClass:[CSSettingSecondTitleCellModel class]]){
//        cell = [CSSettingSecondTitleCell dequeueWithTable:self.table];
//    }else if ([cellModel isKindOfClass:[CSSettingLeftImageCellModel class]]){
//        cell = [CSSettingLeftImageCell dequeueWithTable:self.table];
//    }else if ([cellModel isKindOfClass:[CSSettingLeftImageSubTitleCellModel class]]){
//        cell = [CSSettingLeftImageSubTitleCell dequeueWithTable:self.table];
//    } else if ([cellModel isKindOfClass:[CSSettingLeftImageSwitchCellModel class]]){
//        cell = [CSSettingLeftImageSwitchCell dequeueWithTable:self.table];
//        ((CSSettingSwitchCell *)cell).delegate = self;
//    }
//    return cell;
//}

//- (__kindof CSSettingBasicCell *)cellForType:(__kindof CSSettingBasicCellModel *)cellModel
//{
//    Class cellCls = cellModel.cellCls;
//    __kindof CSSettingBasicCell *cell =  [cellCls dequeueWithTable:self.table];
//    return cell;
//}

//- (__kindof CSSettingBasicCell *)cellForType:(__kindof CSSettingBasicCellModel *)cellModel
//{
//    Class cellCls = cellModel.cellCls;
//    __kindof CSSettingBasicCell *cell =  [cellCls dequeueWithTable:self.table];
//    return cell;
//}


@end
