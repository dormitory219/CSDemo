//
//  CSSettingBasicViewController.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSViewController.h"
#import "CSSettingSectionModel.h"
#import "CSSettingViewHeader.h"

@interface CSSettingBasicViewController : CSViewController

@property (nonatomic,strong) NSMutableArray <CSSettingSectionModel *>*sectionModels;

@property (nonatomic,strong) UITableView *table;

@end
