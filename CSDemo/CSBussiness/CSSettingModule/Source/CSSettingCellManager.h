//
//  CSSettingCellManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/10/4.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSGroundAPI.h"

//对对应的viewcontroller中所有cell的注册进行管理，不希望被重复注册，有效率的注册

#define CSSETTINGCELLREGISTER(vc) \
[CSSettingCellManager registerCell:[self class] viewController:vc];

#define CSSETTINGCELLHASREGISTER(vc) \
[CSSettingCellManager hasRegisterCell:[self class] viewController:vc]

#define CSSETTINGCELLREMOVEALLREGISTER(vc) \
[CSSettingCellManager clearRegisterCellWithViewController:vc];

@interface CSSettingCellManager : NSObject

+ (BOOL)hasRegisterCell:(Class)cellCls viewController:(UIViewController *)viewController;

+ (void)registerCell:(Class)cellCls viewController:(UIViewController *)viewController;

+ (void)clearRegisterCellWithViewController:(UIViewController *)viewController;

@end
