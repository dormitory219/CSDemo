//
//  CSSettingBasicViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSettingBasicViewController.h"
#import "CSGroundAPI.h"

#import "CSSettingBasicViewController+CellType.h"

@interface CSSettingBasicViewController ()<UITableViewDelegate,UITableViewDataSource,CSSettingSwitchSlideDelegate,CSSettingHeaderViewDelegate,CSSettingFooterViewDelegate>

@end

@implementation CSSettingBasicViewController

- (void)dealloc
{
    CSSETTINGCELLREMOVEALLREGISTER(self);
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"setting";
    [self.view addSubview:self.table];
    [self.table mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.top.right.bottom;
    }];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --tableview delegate/dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionModels.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CSSettingSectionModel *sectionModel = self.sectionModels[section];
    return sectionModel.cellModels.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    __kindof CSSettingSectionModel *sectionModel = self.sectionModels[section];
     //根据model.headerCls获取对应headerView
     __kindof CSSettingHeaderView *header = (CSSettingHeaderView *)[CSSettingHeaderFooterBasicView viewForHeaderSectionModel:sectionModel];
    header.delegate = self;
    header.sectionModel = sectionModel;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CSSettingSectionModel *sectionModel = self.sectionModels[section];
    return [CSSettingHeaderFooterBasicView heightForHeaderSectionModel:sectionModel];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSettingSectionModel *sectionModel = self.sectionModels[indexPath.section];
    CSSettingBasicCellModel *cellModel = sectionModel.cellModels[indexPath.row];
    return [CSSettingBasicCell heightForCellModel:cellModel];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CSSettingSectionModel *sectionModel = self.sectionModels[section];
    return [CSSettingHeaderFooterBasicView heightForFooterSectionModel:sectionModel];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    __kindof CSSettingSectionModel *sectionModel = self.sectionModels[section];
    //根据model.footerCls获取对应footerView
    __kindof CSSettingFooterView *footer = (CSSettingFooterView *)[CSSettingHeaderFooterBasicView viewForFooterSectionModel:sectionModel];
    footer.sectionModel = sectionModel;
    footer.delegate = self;
    return footer;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSettingSectionModel *sectionModel = self.sectionModels[indexPath.section];
    CSSettingBasicCellModel *cellModel = sectionModel.cellModels[indexPath.row];
    
    //根据model.cellCls获取对应cell
    __kindof CSSettingBasicCell *cell = [self cellForType:cellModel];
    cell.viewController = self;
    cell.cellModel = cellModel;
    cell.indexPath = indexPath;
    return cell;
}

- (__kindof CSSettingBasicCell *)cellForType:(__kindof CSSettingBasicCellModel *)cellModel
{
    Class cellCls = cellModel.cellCls;
    __kindof CSSettingBasicCell *cell =  [cellCls dequeueWithTable:self.table];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CSSettingSectionModel *sectionModel = self.sectionModels[indexPath.section];
    __kindof CSSettingBasicCellModel *cellModel = sectionModel.cellModels[indexPath.row];
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if ([cellModel isKindOfClass:[CSSettingCheckmarkCellModel class]]) {
        [self updateCheckmarkCellSelected:cellModel sectionModel:sectionModel];
        [self performSelector:cellModel.selector withObject:cellModel];
    }else{
        [self performSelector:cellModel.selector withObject:indexPath];
    }
}

#pragma  mark --selector


#pragma mark --lazy loading

- (UITableView *)table
{
    if (!_table) {
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        table.delegate = self;
        table.dataSource = self;
   
        /*
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([CSSettingBasicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CSSettingBasicCell class])];
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([CSSettingDefaultCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CSSettingDefaultCell class])];
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([CSSettingSwitchCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CSSettingSwitchCell class])];
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([CSSettingSubTitleCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CSSettingSubTitleCell class])];
        [table registerNib:[UINib nibWithNibName:NSStringFromClass([CSSettingSecondTitleCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CSSettingBasicCell class])];
         */
        _table = table;
    }
    return _table;
}

#pragma mark -- cellDelegate
- (void)headerSelect:(CSSettingSectionModel *)model headerView:(CSSettingHeaderView *)header
{
     [self performSelector:model.headerSelectSelector withObject:model];
}

- (void)footerSelect:(CSSettingSectionModel *)model footerView:(CSSettingFooterView *)footer
{
     [self performSelector:model.footerSelectSelector withObject:model];
}

- (void)switchSlideIsOn:(BOOL)isOn cell:(CSSettingSwitchCell *)cell
{
    CSSettingSwitchCellModel *model = cell.cellModel;
    [self performSelector:model.sliderSelector withObject:@(isOn)];
}

- (void)updateCheckmarkCellSelected:(CSSettingCheckmarkCellModel *)selectCellModel sectionModel:(CSSettingSectionModel *)sectionModel
{
    selectCellModel.isSelected ^= 1;
    if (sectionModel.canMultipleSelect) {
      //doNothing
    }else{
        NSArray<CSSettingCheckmarkCellModel *> *cellModels = sectionModel.cellModels;
        [cellModels enumerateObjectsUsingBlock:^(CSSettingCheckmarkCellModel * _Nonnull cellModel, NSUInteger idx, BOOL * _Nonnull stop) {
            if (cellModel != selectCellModel) {
                cellModel.isSelected = NO;
            }
        }];
    }
    [self.table reloadData];
}

@end
