//
//  CSMainModuleAPI.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSMainModuleAPI_h
#define CSMainModuleAPI_h

#define MAIN_ROUTER_HOME              @"route://main/home"
#define MAIN_ROUTER_DIR              @"route://main/directoryList"
#define MAIN_ROUTER_PAGELIST              @"route://main/pageList"
#define MAIN_ROUTER_PAGEPREVIEW             @"route://main/pagePreview"

#endif /* CSMainModuleAPI_h */
