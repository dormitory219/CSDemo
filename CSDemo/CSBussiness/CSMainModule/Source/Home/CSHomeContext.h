//
//  CSHomeContext.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/24.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSGroundAPI.h"

#import "CSHomeStore.h"
#import "CSHomeToolBar.h"

#import "CSHomeListSectionManager.h"

@interface CSHomeContext : CSContext

@property (nonatomic,weak) CSViewController *viewController;

@property (nonatomic,weak) CSHomeToolBar *toolbar;

@property (nonatomic,weak) CSHomeListSectionManager *listManager;

//模块共享状态树的管理对象 store,非redux设计
@property (nonatomic,strong) CSHomeStore *store;

@end
