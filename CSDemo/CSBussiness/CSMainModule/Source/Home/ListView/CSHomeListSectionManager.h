//
//  CSHomeListSectionManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSListSectionManager.h"
#import "CSHomeCollectionView.h"
#import "CSHomeState.h"

@interface CSHomeListSectionManager : CSListSectionManager

@property (nonatomic,assign) CSListShowType showType;

@property (nonatomic,strong) CSHomeCollectionView *listView;
@property (nonatomic,strong) CSHomeCollectionView *thumnailView;

- (void)loadData;

@end
