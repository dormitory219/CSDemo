//
//  CSHomeDocThumbnailSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDocThumbnailSection.h"
#import "CSHomeDocThumbnailCell.h"
#import "CSHomeADThumbnailCell.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"
#import "CSHomeADModel.h"
#import "CSHomeDocModel.h"

@implementation CSHomeDocThumbnailSection

- (CGFloat)minimumInteritemSpacing
{
    return 10;
}

- (CGFloat)minimumLineSpacing
{
    return 10;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeDocThumbnailCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeDocThumbnailCell reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeADThumbnailCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeADThumbnailCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    if (context.store.state.showTagView) {
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width - KTagListViewWidth - KTagSpace - self.minimumLineSpacing*3)/2.f, 115);
    }else{
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width-self.minimumLineSpacing*4)/3.f, 115);
    }
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    CSBasicModel *model = self.items[indexPath.row];
    if ([model isKindOfClass:[CSHomeDocModel class]]) {
        cell = [CSHomeDocThumbnailCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    }else{
        cell = [CSHomeADThumbnailCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    }
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    CSBasicModel *model = self.items[index];
    CSHomeContext *context = (CSHomeContext *)self.context;
    if ([model isKindOfClass:[CSHomeDocModel class]]) {
        if (context.store.state.isEdit) {
            
        }else{
            [CSMediator jumpTo:MAIN_ROUTER_PAGELIST fromViewController:context.viewController];
        }
    }else{
        
    }
}

@end
