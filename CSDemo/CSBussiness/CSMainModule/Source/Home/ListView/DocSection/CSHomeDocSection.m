//
//  CSHomeDocSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDocSection.h"
#import "CSHomeDocListSection.h"
#import "CSHomeDocThumbnailSection.h"

@implementation CSHomeDocSection

- (__kindof CSHomeCellSection *)currentSection
{
    if (!self.listSection) {
        self.listSection  = [CSHomeDocListSection sectionWithData:self.items];
        [self.subSections addObject:self.listSection];
    }
    if (!self.thumbnailSection) {
        self.thumbnailSection = [CSHomeDocThumbnailSection sectionWithData:self.items];
        [self.subSections addObject:self.thumbnailSection];
    }
    if (self.showType == CSListShowTypeTable) {
        return self.listSection;
    } else {
        return self.thumbnailSection;
    }
}

@end
