//
//  CSHomeDocListSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDocListSection.h"
#import "CSHomeDocListCell.h"
#import "CSHomeADListCell.h"
#import "CSHomeDocModel.h"
#import "CSHomeADModel.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"

@implementation CSHomeDocListSection

- (CGFloat)minimumInteritemSpacing
{
    return 0;
}

- (CGFloat)minimumLineSpacing
{
    return 1;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 0, 5, 0);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeDocListCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeDocListCell reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeADListCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeADListCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    CGFloat width;
    CSHomeContext *context = (CSHomeContext *)self.context;
    context.store.state.showTagView ? (width = [UIScreen mainScreen].bounds.size.width - KTagListViewWidth):(width = [UIScreen mainScreen].bounds.size.width - 20);
    return CGSizeMake(width, 60);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    CSBasicModel *model = self.items[indexPath.row];
    if ([model isKindOfClass:[CSHomeDocModel class]]) {
        cell = [CSHomeDocListCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    }else{
        cell = [CSHomeADListCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    }
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    CSBasicModel *model = self.items[index];
    CSHomeContext *context = (CSHomeContext *)self.context;
    if ([model isKindOfClass:[CSHomeDocModel class]]) {
        if (context.store.state.isEdit) {
            
        }else{
            [CSMediator jumpTo:MAIN_ROUTER_PAGELIST fromViewController:context.viewController];
        }
    }else{
        
    }
}

@end
