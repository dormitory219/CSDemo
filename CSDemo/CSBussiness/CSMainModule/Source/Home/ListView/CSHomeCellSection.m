//
//  CSHomeCellSection.m
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeCellSection.h"
#import "CSGroundAPI.h"

#import "CSBubbleEvent.h"
#import "CSNavigationItemEvent.h"

@interface CSHomeCellSection()

@end


@implementation CSHomeCellSection

- (instancetype)init
{
    self = [super init];
    if (self) {
    
    }
    return self;
}

- (__kindof CSHomeCellSection *)currentSection
{
    //impl by sub
    return nil;
}

- (CGFloat)minimumInteritemSpacing
{
    return self.currentSection.minimumInteritemSpacing;
}

- (CGFloat)minimumLineSpacing
{
    return self.currentSection.minimumLineSpacing;
}

- (UIEdgeInsets)inset
{
    return self.currentSection.inset;
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    (void)self.currentSection;
    [self.subSections enumerateObjectsUsingBlock:^(__kindof CSHomeCellSection * _Nonnull section, NSUInteger idx, BOOL * _Nonnull stop) {
        [section setCollectionView:collectionView];
        [section setContext:self.context];
    }];
}

- (NSInteger)numberOfItems
{
    return self.items.count;
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return [self.currentSection sizeForItemAtIndex:index];
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.currentSection cellForItemAtIndexPath:indexPath];
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return [self.currentSection sizeForSupplementaryViewOfKind:elementKind atIndex:index];
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    [self.currentSection didSelectItemAtIndex:index];
}

- ( NSMutableArray <__kindof CSHomeCellSection *> *)subSections
{
    if (!_subSections) {
        _subSections = [NSMutableArray arrayWithCapacity:2];
    }
    return _subSections;
}


@end
