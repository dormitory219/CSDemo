//
//  CSHomeListSectionManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeListSectionManager.h"
#import "CSGroundAPI.h"

#import "CSHomeDocSection.h"
#import "CSHomeDocModel.h"
#import "CSHomeADModel.h"

#import "CSHomeDirSection.h"
#import "CSHomeDirModel.h"

#import "CSHomeTeamSection.h"
#import "CSHomeTeamModel.h"

#import "CSListHeaderEvent.h"
#import "CSNavigationItemEvent.h"

@implementation CSHomeListSectionManager

- (instancetype)init
{
    self = [super init];
    if (self) {
//        CSHomeContext *context = (CSHomeContext *)self.context;
//        self.showType = context.state.showType;
        
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            if (event.index == 0) {
                
                self.showType = context.store.state.showType;
                
                [self loadData];
                
            }else if (event.index == 1){
                [self.thumnailView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (event.showTagView) {
                        make.left.mas_offset(KTagListViewWidth + KTagSpace);
                    }else{
                        make.left.mas_offset(0);
                    }
                }];
                [self.listView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (event.showTagView) {
                        make.left.mas_offset(KTagListViewWidth + KTagSpace);
                    }else{
                        make.left.mas_offset(0);
                    }
                }];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.thumnailView.superview layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [self.thumnailView reloadData];
                    [self.listView reloadData];
                }];
            }else if (event.index == 3){
                [self updateEditState];
            }
        }];
        
        [QTSub(self, CSBubbleEvent) next:^(CSBubbleEvent *event) {
            [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                (void)make.top;
            }];
            [self.listView mas_updateConstraints:^(MASConstraintMaker *make) {
                (void)make.top;
            }];
            [UIView animateWithDuration:0.3 animations:^{
                [self.thumnailView.superview layoutIfNeeded];
                [self.listView.superview layoutIfNeeded];
            }];
        }];
        
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
            [self updateEditState];
        }];
    }
    return self;
}

- (void)updateEditState
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    BOOL isEdit = context.store.state.isEdit;
   
    [self.thumnailView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (isEdit) {
            (void)make.top.offset(-KListHeaderViewHeight);
            self.thumnailView.mj_header.ignoredScrollViewContentInsetTop = 0;
        }else{
            if (context.store.state.bubbleShouldShow) {
                make.top.mas_offset(KTopBubbleViewHeight);
            }else{
                make.top.mas_offset(0);
            }
            self.thumnailView.mj_header.ignoredScrollViewContentInsetTop = KListHeaderViewHeight;
        }
    }];
    [self.listView mas_updateConstraints:^(MASConstraintMaker *make) {
        CSHomeContext *context = (CSHomeContext *)self.context;
        if (isEdit) {
            (void)make.top.offset(-KListHeaderViewHeight);
            self.listView.mj_header.ignoredScrollViewContentInsetTop = 0;
        }else{
            if (context.store.state.bubbleShouldShow) {
                make.top.mas_offset(KTopBubbleViewHeight);
            }else{
                make.top.mas_offset(0);
            }
            self.listView.mj_header.ignoredScrollViewContentInsetTop = KListHeaderViewHeight;
        }
    }];
    [UIView animateWithDuration:0.3 animations:^{
        [self.thumnailView.superview layoutIfNeeded];
        [self.listView.superview layoutIfNeeded];
    }];
}

- (void)setShowType:(CSListShowType)showType
{
    _showType = showType;
    if (showType == CSListShowTypeTable) {
        self.thumnailView.hidden = YES;
        self.listView.hidden = NO;
    }else{
        self.listView.hidden = YES;
        self.thumnailView.hidden = NO;
    }
}

- (void)setThumnailView:(CSHomeCollectionView *)thumnailView
{
    if (_thumnailView != thumnailView)
    {
        _thumnailView = thumnailView;
        //bind delegate and dataSource
        thumnailView.delegate = self;
        thumnailView.dataSource = self;
        
        //set listSection collection/controller
        if (self.sectionMap.count)
        {
            for (CSListSection *section in self.sectionMap)
            {
                section.collectionView = thumnailView;
                section.viewController = self.viewController;
            }
        }
    }
}

- (void)setListView:(CSHomeCollectionView *)listView
{
    if (_listView != listView)
    {
        _listView = listView;
        //bind delegate and dataSource
        listView.delegate = self;
        listView.dataSource = self;
        
        //set listSection collection/controller
        if (self.sectionMap.count)
        {
            for (CSListSection *section in self.sectionMap)
            {
                section.collectionView = listView;
                section.viewController = self.viewController;
            }
        }
    }
}

- (void)loadData
{
    [self.sectionMap removeAllObjects];
    {
        CSHomeTeamSection *section = [CSHomeTeamSection sectionWithData:nil];
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        for (NSInteger i = 0; i< 1; i++) {
            CSHomeTeamModel *model = [[CSHomeTeamModel alloc] init];
            [items addObject:model];
        }
        
        section.items = items;
        [self addListSection:section];
    }
    
    {
        CSHomeDirSection *section = [CSHomeDirSection sectionWithData:nil];
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        for (NSInteger i = 0; i< 4; i++) {
            CSHomeDirModel *model = [[CSHomeDirModel alloc] init];
            [items addObject:model];
        }
        
        section.items = items;
        [self addListSection:section];
    }
    
    {
        CSHomeDocSection *section = [CSHomeDocSection sectionWithData:nil];
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        CSHomeDocModel *model = [[CSHomeDocModel alloc] init];
        [items addObject:model];
        
        model = [[CSHomeDocModel alloc] init];
        [items addObject:model];
        
        CSHomeADModel *adModel = [[CSHomeADModel alloc] init];
        [items addObject:adModel];
        
        model = [[CSHomeDocModel alloc] init];
        [items addObject:model];
        
        adModel = [[CSHomeADModel alloc] init];
        [items addObject:adModel];
        
        section.items = items;
        [self addListSection:section];
    }
     CSHomeContext *context = (CSHomeContext *)self.context;
    [self.sectionMap enumerateObjectsUsingBlock:^(__kindof CSHomeCellSection * _Nonnull section, NSUInteger idx, BOOL * _Nonnull stop) {
        section.section = idx;
        section.showType = self.showType;
        section.context = context;
        if (self.showType == CSListShowTypeTable) {
            section.collectionView = self.listView;
        }else{
            section.collectionView = self.thumnailView;
        }
    }];

    [self reloadView];
}

- (void)reloadView
{
    if (self.showType == CSListShowTypeTable) {
        self.listView.hidden = NO;
        self.thumnailView.hidden = YES;
        [self.listView reloadData];
    }else{
        self.listView.hidden = YES;
        self.thumnailView.hidden = NO;
        [self.thumnailView reloadData];
    }
}

@end
