//
//  CSHomeTeamThumbnailSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeTeamThumbnailSection.h"
#import "CSHomeTeamThumbnailCell.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"

@implementation CSHomeTeamThumbnailSection

- (CGFloat)minimumInteritemSpacing
{
    return 10;
}

- (CGFloat)minimumLineSpacing
{
    return 10;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeTeamThumbnailCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeTeamThumbnailCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
      return CGSizeMake([UIScreen mainScreen].bounds.size.width-20, 80);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    cell = [CSHomeTeamThumbnailCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    if (context.store.state.isEdit) {
        
    }else{
        [CSMediator jumpTo:MAIN_ROUTER_DIR fromViewController:context.viewController];
    }
}

@end
