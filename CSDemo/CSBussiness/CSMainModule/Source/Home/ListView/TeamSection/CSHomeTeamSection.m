//
//  CSHomeTeamSection.m
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeTeamSection.h"
#import "CSHomeTeamListSection.h"
#import "CSHomeTeamThumbnailSection.h"

@implementation CSHomeTeamSection

- (__kindof CSHomeCellSection *)currentSection
{
    if (!self.listSection) {
        self.listSection  = [CSHomeTeamListSection sectionWithData:self.items];
        [self.subSections addObject:self.listSection];
    }
    if (!self.thumbnailSection) {
        self.thumbnailSection = [CSHomeTeamThumbnailSection sectionWithData:self.items];
        [self.subSections addObject:self.thumbnailSection];
    }
    if (self.showType == CSListShowTypeTable) {
        return self.listSection;
    } else {
        return self.thumbnailSection;
    }
}

@end
