//
//  CSHomeDirThumbnailSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDirThumbnailSection.h"
#import "CSHomeDirThumbnailCell.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"

@implementation CSHomeDirThumbnailSection

- (CGFloat)minimumInteritemSpacing
{
    return 10;
}

- (CGFloat)minimumLineSpacing
{
    return 10;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeDirThumbnailCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeDirThumbnailCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    if (context.store.state.showTagView) {
          return CGSizeMake(([UIScreen mainScreen].bounds.size.width - KTagListViewWidth - KTagSpace - self.minimumLineSpacing*3)/2.f, 100);
    }else{
          return CGSizeMake(([UIScreen mainScreen].bounds.size.width-self.minimumLineSpacing*4)/3.f, 100);
    }
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    cell = [CSHomeDirThumbnailCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    if (context.store.state.isEdit) {
        
    }else{
        [CSMediator jumpTo:MAIN_ROUTER_DIR fromViewController:context.viewController];
    }
}

@end
