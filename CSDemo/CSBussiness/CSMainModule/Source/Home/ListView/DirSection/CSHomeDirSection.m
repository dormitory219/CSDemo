//
//  CSHomeDirSection.m
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDirSection.h"
#import "CSHomeDirListSection.h"
#import "CSHomeDirThumbnailSection.h"

@implementation CSHomeDirSection

- (__kindof CSHomeCellSection *)currentSection
{
    if (!self.listSection) {
        self.listSection  = [CSHomeDirListSection sectionWithData:self.items];
        [self.subSections addObject:self.listSection];
    }
    if (!self.thumbnailSection) {
        self.thumbnailSection = [CSHomeDirThumbnailSection sectionWithData:self.items];
        [self.subSections addObject:self.thumbnailSection];
    }
    if (self.showType == CSListShowTypeTable) {
        return self.listSection;
    } else {
        return self.thumbnailSection;
    }
}

@end
