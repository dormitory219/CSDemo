//
//  CSHomeDirListSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeDirListSection.h"
#import "CSHomeDirListCell.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"

@implementation CSHomeDirListSection

- (CGFloat)minimumInteritemSpacing
{
    return 0;
}

- (CGFloat)minimumLineSpacing
{
    return 1;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 0, 5, 0);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSHomeDirListCell class]) bundle:nil] forCellWithReuseIdentifier:[CSHomeDirListCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width - 20, 80);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    cell = [CSHomeDirListCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    if (context.store.state.isEdit) {
        
    }else{
        [CSMediator jumpTo:MAIN_ROUTER_DIR fromViewController:context.viewController];
    }
}

@end
