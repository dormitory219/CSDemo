//
//  CSHomeCellSection.h
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSListSection.h"
#import "CSHomeViewController+Private.h"
#import "CSHomeState.h"
#import "CSHomeListSectionManager.h"
#import "NSObject+Context.h"

@interface CSHomeCellSection : CSListSection

@property (nonatomic,assign) CSListShowType showType;

@property (nonatomic,strong) __kindof CSHomeCellSection *listSection;

@property (nonatomic,strong) __kindof CSHomeCellSection *thumbnailSection;

@property (nonatomic,strong) NSMutableArray <__kindof CSHomeCellSection *> *subSections;

@property (nonatomic,strong) __kindof CSHomeCellSection *currentSection;

@end
