//
//  CSTagListSectionManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSTagListSectionManager.h"
#import "CSGroundAPI.h"

#import "CSTagSection.h"
#import "CSTagModel.h"
#import "CSHomeEventHeader.h"
#import "CSHomeViewController+Private.h"

@implementation CSTagListSectionManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            if (event.index == 1) {
                self.collectionView.hidden = NO;
                [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (event.showTagView) {
                        make.left.mas_offset(0);
                    }else{
                        make.left.mas_offset(-KTagListViewWidth);
                    }
                }];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.collectionView.superview layoutIfNeeded];
                }];
                [self loadData];
            }
        }];
    }
    return self;
}

- (void)loadData
{
    [self.sectionMap removeAllObjects];
    {
        CSTagSection *section = [CSTagSection sectionWithData:nil];
        section.viewController = self.viewController;
        section.collectionView = self.collectionView;
        section.section = 0;
        
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        for (NSInteger i = 0; i < 10; i++) {
            CSTagModel *model = [[CSTagModel alloc] init];
            [items addObject:model];
        }
        section.items = items;
        
        [self addListSection:section];
    }
    
    [self.collectionView reloadData];
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
}


@end
