//
//  CSTagSection.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSTagSection.h"
#import "CSTagCell.h"

@implementation CSTagSection

- (CGFloat)minimumInteritemSpacing
{
    return 0;
}

- (CGFloat)minimumLineSpacing
{
    return 1;
}

- (UIEdgeInsets)inset
{
    return UIEdgeInsetsMake(5, 0, 5, 0);
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSTagCell class]) bundle:nil] forCellWithReuseIdentifier:[CSTagCell reuseIdentifier]];
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return CGSizeMake(120, 60);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    __kindof CSListCell *cell = nil;
    cell = [CSTagCell cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    return cell;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeZero;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    
}

@end
