//
//  CSHomeAblumView.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeAblumView.h"

@implementation CSHomeAblumView

+ (instancetype)ablumView
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (IBAction)ablumClickAction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(ablumClickAction:)]) {
        [self.delegate ablumClickAction:self];
    }
}

@end
