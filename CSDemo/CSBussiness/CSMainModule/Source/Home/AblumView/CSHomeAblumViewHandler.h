//
//  CSHomeAblumViewHandler.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeHandler.h"
#import "CSHomeAblumView.h"

@interface CSHomeAblumViewHandler : CSHomeHandler<CSHomeAblumViewProtocol>

@property (nonatomic,strong) CSHomeAblumView *ablumView;

@end
