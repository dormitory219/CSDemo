//
//  CSHomeAblumView.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CSHomeAblumView;
@protocol CSHomeAblumViewProtocol<NSObject>

- (void)ablumClickAction:(CSHomeAblumView *)ablumView;

@end

@interface CSHomeAblumView : UIView

@property (nonatomic,weak) id<CSHomeAblumViewProtocol> delegate;

+ (instancetype)ablumView;

@end
