//
//  CSHomeAblumViewHandler.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeAblumViewHandler.h"

@implementation CSHomeAblumViewHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
            self.ablumView.hidden = event.isEdit;
        }];
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            switch (event.index) {
                case 3:
                    self.ablumView.hidden = context.store.state.isEdit;
                    break;
                    
                default:
                    break;
            }
        }];
    }
    return self;
}

- (void)ablumClickAction:(CSHomeAblumView *)ablumView
{
    
}


@end
