//
//  CSHomeHandler.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CSHomeContext.h"

/*
 home模块内所有view handler的基类，该类用来分解controller的视图处理逻辑，
 每个view有一个单独的handler,用于处理view本身的逻辑，比如网络请求，数据处理，界面跳转，通知状态到其它模块视图

 */

@interface CSHomeHandler : NSObject

@end
