//
//  CSHomeViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeViewController.h"
#import "CSHomeViewController+Private.h"
#import "CSLoginModuleAPI.h"
#import "CSGroundAPI.h"
#import "CSLoginModuleAPI.h"

@implementation CSHomeViewController

+ (void)load
{
    [CSServiceManager sharedManager];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.serachViewController = [[CSHomeSearchViewController alloc] init];
        self.serachViewController.context = self.context;
    }
    return self;
}

#pragma life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"home";

    self.view.backgroundColor = MAIN_THEME_COLOR;

    [self.view addSubview:self.listView];
    [self.view addSubview:self.thumbnailView];
   
    [self.view addSubview:self.topBubbleView];
    [self.listView addSubview:self.listHeaderView];
    [self.thumbnailView addSubview:self.thumbnailHeaderView];
    [self.view addSubview:self.tagListView];
    [self.view addSubview:self.cameraView];
    [self.view addSubview:self.ablumView];
    [self.view addSubview:self.toolBar];
    
    [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.top.mas_offset(KTopBubbleViewHeight);
        (void)make.left.right.bottom;
    }];
    
    [self.thumbnailView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.top.mas_offset(KTopBubbleViewHeight);
        (void)make.left.right.bottom;
    }];
    
    [self.topBubbleView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.right.top;
        make.height.mas_equalTo(KTopBubbleViewHeight);
    }];
    
    [self.cameraView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.centerX.bottom;
        make.width.mas_equalTo(KCameraViewWidth);
        make.height.mas_equalTo(KCameraViewHeight);
    }];
    
    [self.ablumView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.bottom.mas_offset(-15);
        (void)make.right.mas_offset(-15);
        make.width.mas_equalTo(KAblumViewWidth);
        make.height.mas_equalTo(KAblumViewHeight);
    }];
    
    [self.toolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left;
        make.bottom.mas_equalTo(-0.25);
        make.width.mas_equalTo(self.view.bounds.size.width);
        make.height.mas_equalTo(KHomeToolBarHeight);
    }];
    
    [self.listHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left;
        make.width.mas_equalTo(self.view.frame.size.width);
        (void)make.top.mas_offset(-KListHeaderViewHeight);
        make.height.mas_equalTo(KListHeaderViewHeight);
    }];
    
    [self.thumbnailHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.right;
        make.width.mas_equalTo(self.view.frame.size.width);
        (void)make.top.mas_offset(-KListHeaderViewHeight);
        make.height.mas_equalTo(KListHeaderViewHeight);
    }];
    
    [self.tagListView mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.bottom.top;
        make.width.mas_equalTo(KTagListViewWidth);
    }];
    
    self.navigationItem.leftBarButtonItem = self.homeLeftNavigationItem;
    self.navigationItem.rightBarButtonItem = self.homeRightNavigationItem;
    
    self.homeListSectionManager.thumnailView = self.thumbnailView;
    self.homeListSectionManager.listView = self.listView;
    [self.homeListSectionManager loadData];
    
    self.tagListSectionManager.collectionView = self.tagListView;
    [self.tagListSectionManager loadData];
    
    [self.manager viewDidLoad];
    
#pragma mark --组件间的交互
    id <CSModuleProtocol> module = [CSModuleManager getModule:LOGIN_MODULE_NAME];
    CSLoginModuleAction *action = [CSLoginModuleAction new];
    action.callBack = ^(CSTransmissionData *data) {
        
    };
    action.APIType = CSLoginModuleActionAPITypeGetLoginData;
    [module doAction:action];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.manager viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.manager viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.manager viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.manager viewDidDisappear:animated];
}

#pragma getterView
- (CSHomeCollectionView *)listView
{
    if (!_listView)
    {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _listView = [[CSHomeCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _listView.backgroundColor = SECOND_THEME_COLOR;
        _listView.delegate = self.homeListSectionManager;
        _listView.dataSource = self.homeListSectionManager;
        _listView.contentInset = UIEdgeInsetsMake(KListHeaderViewHeight , 0, 0, 0);
        _listView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 结束刷新
                [_listView.mj_header endRefreshing];
            });
        }];
        _listView.mj_header.ignoredScrollViewContentInsetTop = KListHeaderViewHeight;
    }
    return _listView;
}

- (CSHomeCollectionView *)thumbnailView
{
    if (!_thumbnailView)
    {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _thumbnailView = [[CSHomeCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _thumbnailView.backgroundColor = SECOND_THEME_COLOR;
        _thumbnailView.delegate = self.homeListSectionManager;
        _thumbnailView.dataSource = self.homeListSectionManager;
        _thumbnailView.contentInset = UIEdgeInsetsMake(KListHeaderViewHeight , 0, 0, 0);
        _thumbnailView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 结束刷新
                [_thumbnailView.mj_header endRefreshing];
            });
        }];
        _thumbnailView.mj_header.ignoredScrollViewContentInsetTop = KListHeaderViewHeight;
    }
    return _thumbnailView;
}

- (CSHomeListSectionManager *)homeListSectionManager
{
    if (!_homeListSectionManager)
    {
        _homeListSectionManager = [[CSHomeListSectionManager alloc] init];
        _homeListSectionManager.viewController = self;
        _homeListSectionManager.context = self.context;
    }
    return _homeListSectionManager;
}

- (CSTopBubbleView *)topBubbleView
{
    if (!_topBubbleView) {
        _topBubbleView = [CSTopBubbleView bubbleView];
        _topBubbleView.delegate = self.bubbleViewHandler;
        self.bubbleViewHandler.bubbleView = _topBubbleView;
    }
    return _topBubbleView;
}

- (CSBubbleViewHandler *)bubbleViewHandler
{
    if (!_bubbleViewHandler) {
        _bubbleViewHandler = [[CSBubbleViewHandler alloc] init];
        _bubbleViewHandler.context = self.context;
    }
    return _bubbleViewHandler;
}

- (CSListHeaderView *)listHeaderView
{
    if (!_listHeaderView) {
        _listHeaderView = [CSListHeaderView headerView];
        _listHeaderView.delegate = self.listHeaderViewHandler;
        self.listHeaderViewHandler.headerView = _listHeaderView;
    }
    return _listHeaderView;
}

- (CSListHeaderView *)thumbnailHeaderView
{
    if (!_thumbnailHeaderView) {
        _thumbnailHeaderView = [CSListHeaderView headerView];
        _thumbnailHeaderView.delegate = self.thumbnailHeaderViewHandler;
        self.thumbnailHeaderViewHandler.headerView = _thumbnailHeaderView;
    }
    return _thumbnailHeaderView;
}

- (CSListHeaderViewHandler *)listHeaderViewHandler
{
    if (!_listHeaderViewHandler) {
        _listHeaderViewHandler = [[CSListHeaderViewHandler alloc] init];
        _listHeaderViewHandler.context = self.context;
    }
    return _listHeaderViewHandler;
}

- (CSListHeaderViewHandler *)thumbnailHeaderViewHandler
{
    if (!_thumbnailHeaderViewHandler) {
        _thumbnailHeaderViewHandler = [[CSListHeaderViewHandler alloc] init];
        _thumbnailHeaderViewHandler.context = self.context;
    }
    return _thumbnailHeaderViewHandler;
}

- (CSHomeNavigationItem *)homeLeftNavigationItem
{
    if (!_homeLeftNavigationItem) {
        _homeLeftNavigationItem = [CSHomeNavigationItem leftHomeItemWithHandler:self.homeNavigationItemHandler];
        self.homeNavigationItemHandler.homeItem = _homeLeftNavigationItem;
    }
    return _homeLeftNavigationItem;
}

- (CSHomeNavigationItem *)homeRightNavigationItem
{
    if (!_homeRightNavigationItem) {
        _homeRightNavigationItem = [CSHomeNavigationItem rightHomeItemWithHandler:self.homeNavigationItemHandler];
        self.homeNavigationItemHandler.homeItem = _homeRightNavigationItem;
    }
    return _homeRightNavigationItem;
}

- (CSHomeNavigationItemHandler *)homeNavigationItemHandler
{
    if (!_homeNavigationItemHandler) {
        _homeNavigationItemHandler = [[CSHomeNavigationItemHandler alloc] init];
        _homeNavigationItemHandler.context = self.context;
    }
    return _homeNavigationItemHandler;
}

- (CSHomeCameraView *)cameraView
{
    if (!_cameraView) {
        _cameraView = [CSHomeCameraView cameraView];
        _cameraView.layer.cornerRadius = KCameraViewHeight/2.f;
        self.cameraView.delegate = self.cameraHandler;
        self.cameraHandler.cameraView = _cameraView;
    }
    return _cameraView;
}

- (CSHomeCameraViewHandler *)cameraHandler
{
    if (!_cameraHandler) {
        _cameraHandler = [[CSHomeCameraViewHandler alloc] init];
        _cameraHandler.context = self.context;
    }
    return _cameraHandler;
}

- (CSHomeAblumView *)ablumView
{
    if (!_ablumView) {
        _ablumView = [CSHomeAblumView ablumView];
        _ablumView.layer.cornerRadius = KAblumViewWidth/2.f;
        _ablumView.delegate = self.ablumHandler;
        self.ablumHandler.ablumView = _ablumView;
    }
    return _ablumView;
}

- (CSHomeAblumViewHandler *)ablumHandler
{
    if (!_ablumHandler) {
        _ablumHandler = [[CSHomeAblumViewHandler alloc] init];
        _ablumHandler.context = self.context;
    }
    return _ablumHandler;
}

- (CSHomeToolBar *)toolBar
{
    if (!_toolBar) {
        _toolBar = [CSHomeToolBar toolbar];
        _toolBar.hidden = YES;
        self.toolBarHandler.toolbar = _toolBar;
    }
    return _toolBar;
}

- (CSHomeToolBarHandler *)toolBarHandler
{
    if (!_toolBarHandler) {
        _toolBarHandler = [[CSHomeToolBarHandler alloc] init];
        _toolBarHandler.context = self.context;
    }
    return _toolBarHandler;
}

- (CSTagListView *)tagListView
{
    if (!_tagListView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _tagListView = [[CSTagListView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _tagListView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tagListView.delegate = self.tagListSectionManager;
        _tagListView.dataSource = self.tagListSectionManager;
        _tagListView.hidden = YES;
    }
    return _tagListView;
}

- (CSTagListSectionManager *)tagListSectionManager
{
    if (!_tagListSectionManager)
    {
        _tagListSectionManager = [[CSTagListSectionManager alloc] init];
        _tagListSectionManager.context = self.context;
    }
    return _tagListSectionManager;
}

- (CSHomeViewControllerServiceManager *)manager
{
    if (!_manager) {
        _manager = [CSHomeViewControllerServiceManager shareManager];
        _manager.context = self.context;
    }
    return _manager;
}

- (CSHomeContext *)context
{
    if (!_context) {
        _context = [[CSHomeContext alloc] init];
        _context.viewController = self;
    }
    return _context;
}

@end
