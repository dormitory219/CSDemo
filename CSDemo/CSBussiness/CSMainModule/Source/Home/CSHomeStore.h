//
//  CSHomeStore.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSStore.h"
#import "CSHomeState.h"
#import "CSHomeEventHeader.h"

@interface CSHomeStore : CSStore

@property (nonatomic,strong) CSHomeState *state;

- (void)updateStateWithBubbleEvent:(CSBubbleEvent *)event;

- (void)updateStateWithNavigationItemEvent:(CSNavigationItemEvent *)event;

- (void)updateStateWithListHeaderEvent:(CSListHeaderEvent *)event;

- (void)updateStateWithSearchEvent:(CSSearchEvent *)event;

@end
