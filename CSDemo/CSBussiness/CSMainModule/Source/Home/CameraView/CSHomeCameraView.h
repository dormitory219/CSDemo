//
//  CSHomeCameraView.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CSHomeCameraView;
@protocol CSHomeCameraViewProtocol<NSObject>

- (void)cameraClickAction:(CSHomeCameraView *)cameraView;

@end

@interface CSHomeCameraView : UIView

@property (nonatomic,weak) id<CSHomeCameraViewProtocol> delegate;

+ (instancetype)cameraView;

@end
