//
//  CSHomeCameraViewHandler.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeCameraViewHandler.h"
#import "CSGroundAPI.h"
#import "CSCameraModuleAPI.h"
#import "CSMainModuleAPI.h"

@implementation CSHomeCameraViewHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
            self.cameraView.hidden = event.isEdit;
        }];
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            switch (event.index) {
                case 3:
                    self.cameraView.hidden = context.store.state.isEdit;
                    break;
                    
                default:
                    break;
            }
        }];
        [QTSub(self, CSCameraModeEvent) next:^(CSCameraModeEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            [CSMediator jumpTo:MAIN_ROUTER_PAGELIST fromViewController:context.viewController];
        }];
    }
    return self;
}

- (void)cameraClickAction:(CSHomeCameraView *)cameraView
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    CSTransmissionData *data = [[CSTransmissionData alloc] init];
    [data setData:@(CSCameraModeGroupTypeCommon) forKey:CSCameraModuleGroupKey];
    [CSMediator jumpTo:CAMERA_ROUTER_CAMERA parameter:data fromViewController:context.viewController];
}

@end
