//
//  CSHomeCameraView.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeCameraView.h"
#import "CSGroundAPI.h"

@implementation CSHomeCameraView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderColor = MAIN_THEME_COLOR.CGColor;
}

+ (instancetype)cameraView
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (IBAction)cameraClickAction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraClickAction:)]) {
        [self.delegate cameraClickAction:self];
    }
}
@end
