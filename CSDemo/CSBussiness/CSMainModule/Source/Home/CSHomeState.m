//
//  CSHomeModel.m
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeState.h"

@implementation CSHomeState

- (instancetype)init
{
    self = [super init];
    if (self) {
        _bubbleShouldShow = YES;
        _isEdit = NO;
        _showTagView = NO;
        _showType = CSListShowTypeTable;
    }
    return self;
}

- (void)setBubbleShouldShow:(BOOL)bubbleShouldShow
{
    _bubbleShouldShow = bubbleShouldShow;
}

- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
}

- (void)setShowTagView:(BOOL)showTagView
{
    _showTagView = showTagView;
}

- (void)setShowType:(CSListShowType)showType
{
    _showType = showType;
}

@end
