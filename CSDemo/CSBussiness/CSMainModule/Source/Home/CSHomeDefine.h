//
//  CSHomeDefine.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSHomeDefine_h
#define CSHomeDefine_h

#define KTopBubbleViewHeight 44
#define KListHeaderViewHeight 50

#define KCameraViewWidth 60
#define KCameraViewHeight 60

#define KAblumViewWidth 40
#define KAblumViewHeight 40

#define KHomeToolBarHeight 44

#define KTagListViewWidth 100

#define KTagSpace 5

#endif /* CSHomeDefine_h */
