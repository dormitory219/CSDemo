//
//  CSHomeViewControllerPrivate.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSHomeViewControllerPrivate_h
#define CSHomeViewControllerPrivate_h
#import "CSGroundAPI.h"

#import "CSHomeDefine.h"

#import "CSHomeViewController.h"
#import "CSHomeSearchViewController.h"
#import "CSHomeViewControllerServiceManager.h"

#import "CSHomeContext.h"
#import "CSHomeState.h"

#import "CSHomeListSectionManager.h"

#import "CSTagListSectionManager.h"

#import "CSSearchListSectionManager.h"

#import "CSHomeEventHeader.h"
#import "CSHomeViewHeader.h"
#import "CSHomeHandlerHeader.h"

@interface CSHomeViewController ()

@property (nonatomic,strong) CSHomeCollectionView *listView;
@property (nonatomic,strong) CSHomeCollectionView *thumbnailView;
@property (nonatomic,strong) CSHomeListSectionManager *homeListSectionManager;

@property (nonatomic,strong) CSTopBubbleView *topBubbleView;
@property (nonatomic,strong) CSBubbleViewHandler *bubbleViewHandler;

@property (nonatomic,strong) CSListHeaderView *listHeaderView;
@property (nonatomic,strong) CSListHeaderViewHandler *listHeaderViewHandler;

@property (nonatomic,strong) CSListHeaderView *thumbnailHeaderView;
@property (nonatomic,strong) CSListHeaderViewHandler *thumbnailHeaderViewHandler;

@property (nonatomic,strong) CSHomeNavigationItem *homeLeftNavigationItem;
@property (nonatomic,strong) CSHomeNavigationItem *homeRightNavigationItem;
@property (nonatomic,strong) CSHomeNavigationItemHandler *homeNavigationItemHandler;

@property (nonatomic,strong) CSHomeCameraView *cameraView;
@property (nonatomic,strong) CSHomeCameraViewHandler *cameraHandler;

@property (nonatomic,strong) CSHomeAblumView *ablumView;
@property (nonatomic,strong) CSHomeAblumViewHandler *ablumHandler;

@property (nonatomic,strong) CSHomeToolBar *toolBar;
@property (nonatomic,strong) CSHomeToolBarHandler *toolBarHandler;

@property (nonatomic,strong) CSTagListView *tagListView;
@property (nonatomic,strong) CSTagListSectionManager *tagListSectionManager;

@property (nonatomic,strong) CSHomeSearchViewController *serachViewController;

@property (nonatomic,strong) CSHomeViewControllerServiceManager *manager;

@property (nonatomic,strong) CSHomeContext *context;


@end

#endif /* CSHomeViewControllerPrivate_h */
