//
//  CSToolBar.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeToolBar.h"

@implementation CSHomeToolBar

+ (instancetype)toolbar
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

@end
