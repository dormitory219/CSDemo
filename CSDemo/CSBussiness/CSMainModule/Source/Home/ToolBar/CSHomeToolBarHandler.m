//
//  CSHomeToolBarHandler.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeToolBarHandler.h"

@implementation CSHomeToolBarHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
            self.toolbar.hidden = !event.isEdit;
        }];
    }
    return self;
}

@end
