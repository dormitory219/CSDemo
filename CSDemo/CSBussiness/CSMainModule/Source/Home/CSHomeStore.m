//
//  CSHomeStore.m
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeStore.h"
#import "CSHomeStore.h"

@interface CSHomeStore()

@property (nonatomic,strong) dispatch_semaphore_t semaphore;

@end

@implementation CSHomeStore

- (instancetype)init
{
    self = [super init];
    if (self) {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
        self.semaphore = semaphore;
    }
    return self;
}

+ (instancetype)store
{
    CSHomeStore *store = [super store];
    store.state = [CSHomeState new];
    return store;
}

- (void)updateStateWithBubbleEvent:(CSBubbleEvent *)event
{
    dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
    self.state.bubbleShouldShow = NO;
    dispatch_semaphore_signal(self.semaphore);
}

- (void)updateStateWithNavigationItemEvent:(CSNavigationItemEvent *)event
{
    dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
    self.state.isEdit ^= 1;
    dispatch_semaphore_signal(self.semaphore);
}

- (void)updateStateWithListHeaderEvent:(CSListHeaderEvent *)event
{
    dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
    switch (event.index) {
        case 0:
        {
            if (self.state.showType == CSListShowTypeTable) {
                self.state.showType = CSListShowTypeCollection;
            }else{
                self.state.showType = CSListShowTypeTable;
            }
        }
            break;
        case 1:
        {
            self.state.showTagView ^= 1;
        }
            break;
        case 2:
        {
            self.state.showSearch = YES;
        }
            break;
            
        default:
            break;
    }
    dispatch_semaphore_signal(self.semaphore);
}

- (void)updateStateWithSearchEvent:(CSSearchEvent *)event
{
    dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
    self.state.showSearch = event.showSearch;
    dispatch_semaphore_signal(self.semaphore);
}

@end
