//
//  CSNavigationItemEvent.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QTEventBus.h"

typedef NS_ENUM(NSInteger,CSNavigationItemType) {
    CSNavigationItemTypeLeft,
    CSNavigationItemTypeRight
};

@interface CSNavigationItemEvent : NSObject<QTEvent>

@property (nonatomic,assign) BOOL isEdit;

@property (nonatomic,assign) BOOL finishEvent;

@property (nonatomic,assign) CSNavigationItemType itemType;

@end
