//
//  CSHomeNavigationItem.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeNavigationItem.h"

@interface CSHomeNavigationItem ()

@end

@implementation CSHomeNavigationItem

+ (instancetype)leftHomeItemWithHandler:(id<CSHomeItemProtocol>)delegate
{
    CSHomeNavigationItem *item = [[self alloc] initWithImage:[UIImage imageNamed:@"nav_leftbar"] style:UIBarButtonItemStylePlain target:delegate action:@selector(itemDidClickAction:)];
    item.itemType = CSNavigationItemTypeLeft;
    item.delegate = delegate;
    return item;
}

+ (instancetype)rightHomeItemWithHandler:(id<CSHomeItemProtocol>)delegate
{
    CSHomeNavigationItem *item = [[self alloc] initWithImage:[UIImage imageNamed:@"nav_rightbar"] style:UIBarButtonItemStylePlain target:delegate action:@selector(itemDidClickAction:)];
    item.itemType = CSNavigationItemTypeRight;
    item.delegate = delegate;
    return item;
}

@end
