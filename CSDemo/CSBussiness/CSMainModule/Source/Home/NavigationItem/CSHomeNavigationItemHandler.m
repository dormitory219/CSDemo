//
//  CSNavigationItemHandler.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeNavigationItemHandler.h"
#import "CSNavigationItemEvent.h"
#import "CSHomeEventHeader.h"

@implementation CSHomeNavigationItemHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            switch (event.index) {
                case 3:
                {
                    CSHomeContext *context = (CSHomeContext *)self.context;
                    BOOL isEdit = context.store.state.isEdit;
                    [self updateItemState:isEdit];
                }
                    break;
                    
                default:
                    break;
            }
        }];
    }
    return self;
}

- (void)itemDidClickAction:(CSHomeNavigationItem *)homeItem
{
    CSNavigationItemEvent *event = [CSNavigationItemEvent new];
    event.itemType = homeItem.itemType;
    
    CSHomeContext *context = (CSHomeContext *)self.context;
    //右侧栏：编辑状态更新编辑状态，非编辑状态打开右侧栏
    if (event.itemType == CSNavigationItemTypeRight) {
        if (context.store.state.isEdit) {
            [context.store updateStateWithNavigationItemEvent:event];
            event.isEdit = context.store.state.isEdit;
            event.finishEvent = YES;
            [self updateItemState:event.isEdit];
        }
    }
    [[QTEventBus shared] dispatch:event];
}

- (void)updateItemState:(BOOL)isEdit
{
    if (isEdit) {
        self.homeItem.title = @"编辑中";
        self.homeItem.tintColor = [UIColor greenColor];
    }else{
        self.homeItem.title = @"编辑";
        self.homeItem.tintColor = [UIColor whiteColor];
    }
}

@end
