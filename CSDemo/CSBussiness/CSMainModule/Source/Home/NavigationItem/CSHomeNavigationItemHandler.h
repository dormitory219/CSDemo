//
//  CSNavigationItemHandler.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSHomeNavigationItem.h"
#import "CSHomeHandler.h"

@interface CSHomeNavigationItemHandler : CSHomeHandler<CSHomeItemProtocol>

@property (nonatomic,strong) CSHomeNavigationItem *homeItem;

@end
