//
//  CSHomeNavigationItem.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSNavigationItemEvent.h"

@class CSHomeNavigationItem;
@protocol CSHomeItemProtocol<NSObject>

- (void)itemDidClickAction:(CSHomeNavigationItem *)homeItem;
@end

@interface CSHomeNavigationItem : UIBarButtonItem

@property (nonatomic,weak) id<CSHomeItemProtocol> delegate;

@property (nonatomic,assign) CSNavigationItemType itemType;

+ (instancetype)leftHomeItemWithHandler:(id<CSHomeItemProtocol>)delegate;

+ (instancetype)rightHomeItemWithHandler:(id<CSHomeItemProtocol>)delegate;

@end
