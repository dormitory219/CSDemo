//
//  CSSearchListSectionManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSSearchListSectionManager.h"

#import "CSHomeDocSection.h"
#import "CSHomeDocModel.h"

#import "CSHomeDirSection.h"
#import "CSHomeDirModel.h"

#import "CSHomeTeamSection.h"
#import "CSHomeTeamModel.h"

#import "CSListHeaderEvent.h"

#import "CSNavigationItemEvent.h"

@implementation CSSearchListSectionManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        CSHomeContext *context = (CSHomeContext *)self.context;
        self.showType = context.store.state.showType;
        
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            if (event.index == 0) {
            
            }else if (event.index == 1){

            }else if (event.index == 2){
                //搜索
                if (context.store.state.showType == CSListShowTypeTable) {
                    self.thumnailView.hidden = YES;
                    self.listView.hidden = NO;
                }else{
                    self.thumnailView.hidden = NO;
                    self.listView.hidden = YES;
                }
                self.showType = context.store.state.showType;
                [self loadDataWithSearchString:self.searchString];
                [self reloadView];
            }
        }];
        
        [QTSub(self, CSBubbleEvent) next:^(CSBubbleEvent *event) {
            [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                (void)make.top;
            }];
            [self.listView mas_updateConstraints:^(MASConstraintMaker *make) {
                (void)make.top;
            }];
            [UIView animateWithDuration:0.3 animations:^{
                [self.collectionView.superview layoutIfNeeded];
                [self.listView.superview layoutIfNeeded];
            }];
        }];
        
    }
    return self;
}

- (void)setShowType:(CSListShowType)showType
{
    _showType = showType;
    if (showType == CSListShowTypeTable) {
        self.collectionView.hidden = YES;
        self.listView.hidden = NO;
    }else{
        self.listView.hidden = YES;
        self.collectionView.hidden = NO;
    }
}

- (void)setThumnailView:(CSSearchCollectionView *)thumnailView
{
    if (_thumnailView != thumnailView)
    {
        _thumnailView = thumnailView;
        //bind delegate and dataSource
        thumnailView.delegate = self;
        thumnailView.dataSource = self;
        
        //set listSection collection/controller
        if (self.sectionMap.count)
        {
            for (CSListSection *section in self.sectionMap)
            {
                section.collectionView = thumnailView;
                section.viewController = self.viewController;
            }
        }
    }
}

- (void)setListView:(CSSearchCollectionView *)listView
{
    if (_listView != listView)
    {
        _listView = listView;
        //bind delegate and dataSource
        listView.delegate = self;
        listView.dataSource = self;
        
        //set listSection collection/controller
        if (self.sectionMap.count)
        {
            for (CSListSection *section in self.sectionMap)
            {
                section.collectionView = listView;
                section.viewController = self.viewController;
            }
        }
    }
}

- (void)loadDataWithSearchString:(NSString *)searchString
{
    self.searchString = searchString;
    [self.sectionMap removeAllObjects];
    {
        CSHomeTeamSection *section = [CSHomeTeamSection sectionWithData:nil];
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        NSInteger sum;
        if (!searchString.length) {
            sum = 1;
        }else{
            sum = 1 + arc4random_uniform(3);
        }
        for (NSInteger i = 0; i< sum; i++) {
            CSHomeTeamModel *model = [[CSHomeTeamModel alloc] init];
            [items addObject:model];
        }
        
        section.items = items;
        [self addListSection:section];
    }
    
    {
        CSHomeDirSection *section = [CSHomeDirSection sectionWithData:nil];
        
        NSInteger sum;
        if (!searchString.length) {
            sum = 4;
        }else{
            sum = 1 + arc4random_uniform(4);
        }
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];
        for (NSInteger i = 0; i< sum; i++) {
            CSHomeDirModel *model = [[CSHomeDirModel alloc] init];
            [items addObject:model];
        }
        
        section.items = items;
        [self addListSection:section];
    }
    
    {
        CSHomeDocSection *section = [CSHomeDocSection sectionWithData:nil];
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:1];

        NSInteger sum;
        if (!searchString.length) {
            sum = 10;
        }else{
            sum = 1 + arc4random_uniform(10);
        }
        for (NSInteger i = 0; i< sum; i++) {
            CSHomeDocModel *model = [[CSHomeDocModel alloc] init];
            [items addObject:model];
        }
        
        section.items = items;
        [self addListSection:section];
    }
     CSHomeContext *context = (CSHomeContext *)self.context;
    [self.sectionMap enumerateObjectsUsingBlock:^(__kindof CSHomeCellSection * _Nonnull section, NSUInteger idx, BOOL * _Nonnull stop) {
        section.section = idx;
        section.showType = self.showType;
        section.context = context;
        if (self.showType == CSListShowTypeTable) {
            section.collectionView = self.listView;
        }else{
            section.collectionView = self.thumnailView;
        }
    }];
    [self reloadView];
}

- (void)reloadView
{
    if (self.showType == CSListShowTypeTable) {
        self.listView.hidden = NO;
        self.thumnailView.hidden = YES;
        [self.listView reloadData];
    }else{
        self.listView.hidden = YES;
        self.thumnailView.hidden = NO;
        [self.thumnailView reloadData];
    }
}

@end
