//
//  CSHomeSearchViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeSearchViewController.h"
#import "CSGroundAPI.h"

#import "CSSearchCollectionView.h"
#import "CSSearchListSectionManager.h"
#import "CSHomeEventHeader.h"
#import "NSObject+Context.h"
#import "CSHomeContext.h"

@interface CSHomeSearchViewController ()<UISearchBarDelegate>

@property (nonatomic,strong) CSHomeState *state;

@property (nonatomic,strong) UISearchBar *searchBar;
@property (nonatomic,strong) CSSearchCollectionView *searchListView;
@property (nonatomic,strong) CSSearchCollectionView *searchThumbnailView;
@property (nonatomic,strong) CSSearchListSectionManager *searchListSectionManager;

@property (nonatomic,copy) NSString *searchString;

@end

@implementation CSHomeSearchViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            CSHomeContext *context = (CSHomeContext *)self.context;
            if (event.index == 2) {
                self.searchBar.text = nil;
                [self.searchBar becomeFirstResponder];
                
                [context.viewController.navigationController addChildViewController:self];
                [context.viewController.navigationController.view addSubview:self.view];
                [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
                    (void)make.left.right.top.bottom;
                }];
                self.view.alpha = 0.f;
                [UIView animateWithDuration:0.45 animations:^{
                    self.view.alpha = 1.f;
                }];
            }
        }];
        
        [QTSub(self, CSSearchEvent) next:^(CSSearchEvent *event) {
            if (!event.showSearch) {
                [UIView animateWithDuration:0.45 animations:^{
                    self.view.alpha = 0.f;
                } completion:^(BOOL finished) {
                    [self.view removeFromSuperview];
                    [self removeFromParentViewController];
                }];
            }
        }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.searchListView];
    [self.view addSubview:self.searchThumbnailView];

    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        (void)make.left.right;
        make.top.mas_offset(20);
        make.height.mas_equalTo(44);
    }];
    
    [self.searchListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.searchBar.mas_bottom).offset(5);
        (void)make.left.right.bottom;
    }];
    
    [self.searchThumbnailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.searchBar.mas_bottom).offset(5);
        (void)make.left.right.bottom;
    }];
    
    self.searchListSectionManager.thumnailView = self.searchThumbnailView;
    self.searchListSectionManager.listView = self.searchListView;
    [self.searchListSectionManager loadDataWithSearchString:self.searchString];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchString = searchText;
    [self.searchListSectionManager loadDataWithSearchString:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    CSHomeContext *context = (CSHomeContext *)self.context;
    CSSearchEvent *event = [[CSSearchEvent alloc] init];
    event.showSearch = NO;
    [context.store updateStateWithSearchEvent:event];
    [[QTEventBus shared] dispatch:event];
}

- (CSSearchCollectionView *)searchListView
{
    if (!_searchListView)
    {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _searchListView = [[CSSearchCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _searchListView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _searchListView.delegate = self.searchListSectionManager;
        _searchListView.dataSource = self.searchListSectionManager;
    }
    return _searchListView;
}

- (CSSearchCollectionView *)searchThumbnailView
{
    if (!_searchThumbnailView)
    {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _searchThumbnailView = [[CSSearchCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _searchThumbnailView.backgroundColor = [UIColor groupTableViewBackgroundColor];
       // _searchThumbnailView.hidden = YES;
//        _searchThumbnailView.alpha = 0.75;
        _searchThumbnailView.delegate = self.searchListSectionManager;
        _searchThumbnailView.dataSource = self.searchListSectionManager;
    }
    return _searchThumbnailView;
}

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.placeholder = @"search";
        _searchBar.showsCancelButton = YES;
        _searchBar.delegate = self;
        [_searchBar becomeFirstResponder];
    }
    return _searchBar;
}

- (CSSearchListSectionManager *)searchListSectionManager
{
    if (!_searchListSectionManager)
    {
        _searchListSectionManager = [[CSSearchListSectionManager alloc] init];
        _searchListSectionManager.context = self.context;
    }
    return _searchListSectionManager;
}
@end
