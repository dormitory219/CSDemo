//
//  CSSearchListSectionManager.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSListSectionManager.h"
#import "CSSearchCollectionView.h"
#import "CSHomeState.h"
#import "CSHomeListSectionManager.h"

@interface CSSearchListSectionManager : CSListSectionManager

@property (nonatomic,assign) CSListShowType showType;

@property (nonatomic,strong) CSSearchCollectionView *listView;
@property (nonatomic,strong) CSSearchCollectionView *thumnailView;

@property (nonatomic,copy) NSString *searchString;

- (void)loadDataWithSearchString:(NSString *)searchString;

@end
