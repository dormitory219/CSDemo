//
//  CSSearchEvent.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSGroundAPI.h"

@interface CSSearchEvent : NSObject<QTEvent>

@property (nonatomic,assign) BOOL showSearch;

@end
