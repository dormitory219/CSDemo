//
//  CSTopBubbleView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSTopBubbleView.h"

@interface CSTopBubbleView()

@property (weak, nonatomic) IBOutlet UIButton *tipLabel;


@end

@implementation CSTopBubbleView

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewAction:)];
    [self addGestureRecognizer:tapGesture];
    self.tipLabel.titleLabel.numberOfLines = 2;
    self.tipLabel.titleLabel.adjustsFontSizeToFitWidth = YES;
}

+ (instancetype)bubbleView
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}
- (IBAction)closeAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(bubbleCloseAction:)]) {
        [self.delegate bubbleCloseAction:self];
    }
}
- (IBAction)viewAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(bubbleViewAction:)]) {
        [self.delegate bubbleViewAction:self];
    }
}

@end
