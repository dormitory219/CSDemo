//
//  CSBubbleEvent.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QTEventBus.h"

@interface CSBubbleEvent : NSObject<QTEvent>

@property (nonatomic,assign) BOOL dismiss;

@end
