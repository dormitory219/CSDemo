//
//  CSBubbleViewHandler.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSBubbleViewHandler.h"
#import "CSGroundAPI.h"

#import "CSHomeEventHeader.h"
#import "CSHomeViewController+Private.h"

@implementation CSBubbleViewHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
     
            [UIView animateWithDuration:0.3 animations:^{
                if (event.isEdit) {
                    self.bubbleView.alpha = 0.f;
                }else{
                    self.bubbleView.alpha = 1.f;
                }
            }];
        }];
        
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            
            if (event.index == 1) {
                
                [self.bubbleView mas_updateConstraints:^(MASConstraintMaker *make) {
                    if (event.showTagView) {
                        make.left.mas_offset(KTagListViewWidth + KTagSpace);
                    }else{
                        make.left.mas_offset(0);
                    }
                }];
                [UIView animateWithDuration:0.3 animations:^{
                    [self.bubbleView.superview layoutIfNeeded];
                }];
            }
            else if (event.index == 3){
                [UIView animateWithDuration:0.3 animations:^{
                    CSHomeContext *context = (CSHomeContext *)self.context;
                    if (context.store.state.isEdit) {
                        self.bubbleView.alpha = 0.f;
                    }else{
                        self.bubbleView.alpha = 1.f;
                    }
                }];
            };
        }];
    }
    return self;
}

- (void)bubbleViewAction:(CSTopBubbleView *)bubbleView
{
    CSBubbleEvent *event = [CSBubbleEvent new];
    event.dismiss = YES;
    CSHomeContext *context = (CSHomeContext *)self.context;
    [context.store updateStateWithBubbleEvent:event];
    [[QTEventBus shared] dispatch:event];
    [UIView animateWithDuration:0.3 animations:^{
        bubbleView.alpha = 0.f;
    } completion:^(BOOL finished) {
         [bubbleView removeFromSuperview];
    }];
}

- (void)bubbleCloseAction:(CSTopBubbleView *)bubbleView
{
    CSBubbleEvent *event = [CSBubbleEvent new];
    event.dismiss = YES;
    CSHomeContext *context = (CSHomeContext *)self.context;
    [context.store updateStateWithBubbleEvent:event];
    
    [[QTEventBus shared] dispatch:event];
    [UIView animateWithDuration:0.3 animations:^{
        bubbleView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [bubbleView removeFromSuperview];
    }];
}

@end
