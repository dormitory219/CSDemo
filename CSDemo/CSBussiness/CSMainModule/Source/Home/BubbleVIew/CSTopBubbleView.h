//
//  CSTopBubbleView.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CSTopBubbleView;
@protocol CSTopBubbleViewProtocol<NSObject>

- (void)bubbleViewAction:(CSTopBubbleView *)bubbleView;

- (void)bubbleCloseAction:(CSTopBubbleView *)bubbleView;

@end

@interface CSTopBubbleView : UIView

@property (nonatomic,weak) id<CSTopBubbleViewProtocol> delegate;

+ (instancetype)bubbleView;

@end
