//
//  CSHomeContext.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/24.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeContext.h"

@implementation CSHomeContext

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.store = [CSHomeStore store];
    }
    return self;
}

@end
