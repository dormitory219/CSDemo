//
//  CSListHeaderView.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CSListHeaderView;
@protocol CSListHeaderViewProtocol<NSObject>

- (void)btnDidClick:(NSInteger)index headerView:(CSListHeaderView *)headerView;

@end

@interface CSListHeaderView : UIView

@property (nonatomic,weak) id<CSListHeaderViewProtocol> delegate;

+ (instancetype)headerView;

@end
