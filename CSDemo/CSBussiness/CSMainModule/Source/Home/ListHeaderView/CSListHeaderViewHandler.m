//
//  CSListHeaderViewHandler.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSListHeaderViewHandler.h"
#import "CSBubbleEvent.h"
#import "CSHomeEventHeader.h"
#import "Masonry.h"
#import "CSHomeViewController+Private.h"

@implementation CSListHeaderViewHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [QTSub(self, CSBubbleEvent) next:^(CSBubbleEvent *event) {
          
        }];
        
        //因为在list样式时的更改（比如显示左侧栏时listheader要缩短）也需要通知到未显示的thumb样式，所以需要监听自己
        [QTSub(self, CSListHeaderEvent) next:^(CSListHeaderEvent *event) {
            if (event.index == 1) {
                [self.headerView mas_updateConstraints:^(MASConstraintMaker *make) {
                    CGFloat width;
                    CSHomeContext *context = (CSHomeContext *)self.context;
                    if (context.store.state.showTagView) {
                        width = [UIScreen mainScreen].bounds.size.width - KTagListViewWidth -KTagSpace;
                    }else{
                        width = [UIScreen mainScreen].bounds.size.width;
                    }
                    make.width.mas_equalTo(width);
                }];
            }
        }];
        
        [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
            CGFloat delayTime;
            event.isEdit ?  (delayTime = 0.3f) : (delayTime = 0.f);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                event.isEdit ?  (self.headerView.alpha = 0.f) : (self.headerView.alpha = 1.f);
            });;
        }];
    }
    return self;
}

- (void)btnDidClick:(NSInteger)index headerView:(CSListHeaderView *)headerView
{
    CSListHeaderEvent *event = [CSListHeaderEvent new];
    event.index = index;
    CSHomeContext *context = (CSHomeContext *)self.context;
    switch (index) {
        case 0:
            [context.store updateStateWithListHeaderEvent:event];
            break;
        case 1:
        {
            [context.store updateStateWithListHeaderEvent:event];
            event.showTagView = context.store.state.showTagView;
            [self.headerView mas_updateConstraints:^(MASConstraintMaker *make) {
                CGFloat width;
                if (context.store.state.showTagView) {
                    width = [UIScreen mainScreen].bounds.size.width - KTagListViewWidth -KTagSpace;
                }else{
                    width = [UIScreen mainScreen].bounds.size.width;
                }
                make.width.mas_equalTo(width);
            }];
        }
            break;
        case 3:
        {
            CSHomeContext *context = (CSHomeContext *)self.context;
            context.store.state.isEdit ^= 1;
        }
            break;
        default:
            break;
    }
    [[QTEventBus shared] dispatch:event];
}

@end
