//
//  CSListHeaderViewHandler.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSListHeaderView.h"
#import "CSHomeHandler.h"

@interface CSListHeaderViewHandler : CSHomeHandler<CSListHeaderViewProtocol>

@property (nonatomic,strong) CSListHeaderView *headerView;

@end
