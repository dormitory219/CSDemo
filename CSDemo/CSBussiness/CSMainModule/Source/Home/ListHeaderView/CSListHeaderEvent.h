//
//  CSListHeaderEvent.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/16.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSBubbleEvent.h"

typedef NS_ENUM(NSUInteger, CSHeaderFunctionType) {
    CSHeaderFunctionTypeChangeViewModel = 0,
    CSHeaderFunctionTypeShowTag
};

@interface CSListHeaderEvent : NSObject<QTEvent>

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,assign) BOOL showTagView;

@end
