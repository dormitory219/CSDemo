//
//  CSListHeaderView.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSListHeaderView.h"

@implementation CSListHeaderView

+ (instancetype)headerView
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (IBAction)selectBtnAction:(UIButton *)sender
{
    NSInteger index = sender.tag - 100;
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnDidClick:headerView:)]) {
        [self.delegate btnDidClick:index headerView:self];
    }
}

@end
