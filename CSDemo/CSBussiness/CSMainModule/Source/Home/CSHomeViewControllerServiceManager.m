//
//  CSHomeViewControllerServiceManager.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeViewControllerServiceManager.h"
#import "CSHomeViewControllerPopViewService.h"
#import "CSHomeViewControllerGuideViewService.h"
#import "CSHomeViewControllerRewardFloatVideoViewService.h"
#import "CSHomeViewControllerNotificationService.h"
#import "CSHomeViewControllerScreenShotService.h"

#import "CSHomeViewControllerServiceProtocol.h"
#import "MTStackViewController.h"

@interface CSHomeViewControllerServiceManager()

@property (nonatomic,strong) NSMutableArray <id<CSHomeViewControllerServiceProtocol>>*serviceArray;
@property (nonatomic,strong) dispatch_queue_t serviceQueue;

@end

@implementation CSHomeViewControllerServiceManager

+ (instancetype)shareManager
{
    static CSHomeViewControllerServiceManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _serviceQueue = dispatch_queue_create("com.camscannner.homeServiceQueue",
                                              DISPATCH_QUEUE_CONCURRENT);
        
        [self registerAllService];
        
        [self addObserver];
    }
    return self;
}

- (void)addObserver
{
    [QTSub(self, CSNavigationItemEvent) next:^(CSNavigationItemEvent *event) {
        CSHomeContext *context = (CSHomeContext *)self.context;
        MTStackViewController *stackViewController = context.viewController.stackViewController;
        switch (event.itemType) {
            case CSNavigationItemTypeLeft:
            {
                if (stackViewController.leftViewControllerVisible)
                {
                    [stackViewController hideLeftViewControllerAnimated:YES];
                }else{
                    [stackViewController revealLeftViewControllerAnimated:YES];
                }
            }
                break;
                
            case CSNavigationItemTypeRight:
            {
                if (event.finishEvent) {
                    return;
                }
                if (stackViewController.rightViewControllerVisible)
                {
                    [stackViewController hideRightViewControllerAnimated:YES];
                }else{
                    [stackViewController revealRightViewControllerAnimated:YES];
                }
            }
            default:
                break;
        }
    }];
}

- (void)registerAllService
{
    dispatch_barrier_async(_serviceQueue, ^{
        id service = [CSHomeViewControllerPopViewService new];
        [self.serviceArray addObject:service];
        
        service = [CSHomeViewControllerRewardFloatVideoViewService new];
        [self.serviceArray addObject:service];
        
        service = [CSHomeViewControllerScreenShotService new];
        [self.serviceArray addObject:service];
        
        service = [CSHomeViewControllerNotificationService new];
        [self.serviceArray addObject:service];
        
        service = [CSHomeViewControllerGuideViewService new];
        [self.serviceArray addObject:service];
    });
}

- (void)removeService:(id)service
{
    dispatch_barrier_async(_serviceQueue, ^{
        if ([self.serviceArray containsObject:service]) {
            [self.serviceArray removeObject:service];
        }
    });
}


- (void)viewDidLoad
{
    return;
    for (id service in self.serviceArray)
    {
        if ([service respondsToSelector:@selector(viewDidLoad)])
        {
            [service viewDidLoad];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    for (id service in self.serviceArray)
    {
        if ([service respondsToSelector:@selector(viewWillAppear:)])
        {
            [service viewWillAppear:animated];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    for (id service in self.serviceArray)
    {
        if ([service respondsToSelector:@selector(viewDidAppear:)])
        {
            [service viewDidAppear:animated];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    for (id service in self.serviceArray)
    {
        if ([service respondsToSelector:@selector(viewWillDisappear:)])
        {
            [service viewWillDisappear:animated];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    for (id service in self.serviceArray)
    {
        if ([service respondsToSelector:@selector(viewDidDisappear:)])
        {
            [service viewDidDisappear:animated];
        }
    }
}

- (NSMutableArray <id<CSHomeViewControllerServiceProtocol>>*)serviceArray
{
    if (!_serviceArray)
    {
        _serviceArray = [NSMutableArray arrayWithCapacity:2];
    }
    return _serviceArray;
}


@end
