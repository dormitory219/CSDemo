//
//  CSHomeModel.h
//  CSDemo
//
//  Created by 余强 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSState.h"

//全局state状态树，存储公共状态，多个模块都需要被共享的数据

typedef NS_ENUM(NSInteger,CSListShowType)
{
    CSListShowTypeTable,
    CSListShowTypeCollection
};

@interface CSHomeState : CSState

//顶部泡泡是否展示
@property (nonatomic,assign) BOOL bubbleShouldShow;

//编辑状态
@property (nonatomic,assign) BOOL isEdit;

//标签栏显示
@property (nonatomic,assign) BOOL showTagView;

/**
 展示类型，listview,thumbnail
 */
@property (nonatomic,assign) CSListShowType showType;

/**
 是否开始搜索
 */
@property (nonatomic,assign) BOOL showSearch;

@end
