//
//  CSHomeViewControllerServiceProtocol.h
//  CSDemo
//
//  Created by 余强 on 2018/9/25.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef CSHomeViewControllerServiceProtocol_h
#define CSHomeViewControllerServiceProtocol_h

@protocol CSHomeViewControllerServiceProtocol <NSObject>

@optional;
- (void)viewDidLoad;

- (void)viewWillAppear:(BOOL)animated;

- (void)viewDidAppear:(BOOL)animated;

- (void)viewWillDisappear:(BOOL)animated;

- (void)viewDidDisappear:(BOOL)animated;

@end


#endif /* CSHomeViewControllerServiceProtocol_h */
