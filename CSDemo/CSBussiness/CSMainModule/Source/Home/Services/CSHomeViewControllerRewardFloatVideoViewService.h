//
//  CSHomeViewControllerRewardFloatVideoViewService.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSHomeHandler.h"
#import "CSHomeViewControllerServiceProtocol.h"

@interface CSHomeViewControllerRewardFloatVideoViewService : NSObject<CSHomeViewControllerServiceProtocol>

@end
