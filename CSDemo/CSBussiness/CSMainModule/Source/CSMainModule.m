//
//  CSHomeModule.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/15.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSMainModule.h"
#import "CSHomeViewController.h"
#import "CSDirectoryListViewController.h"
#import "CSPageListViewController.h"
#import "CSPagePreviewViewController.h"
#import "CSNavigationController.h"

@implementation CSMainModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"main/home" jump:^(CSRouteRequest * _Nonnull request) {
        CSHomeViewController *vc = [[CSHomeViewController alloc] init];
        vc.routeRequest = request;
        UINavigationController *naviVc = (CSNavigationController *)request.fromVC.navigationController;
        if (naviVc && [naviVc isKindOfClass:[CSNavigationController class]]) {
            [naviVc pushViewController:vc animated:YES];
        }
    }];
    
    [[CSRouter router] registerNativePath:@"main/home" view:^UIViewController *(CSRouteRequest *request) {
        CSHomeViewController *vc = [[CSHomeViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
    [[CSRouter router] registerNativePath:@"main/directoryList" jump:^(CSRouteRequest * _Nonnull request) {
        CSDirectoryListViewController *vc = [[CSDirectoryListViewController alloc] init];
        vc.routeRequest = request;
        UINavigationController *naviVc = (UINavigationController *)request.fromVC.navigationController;
        if (naviVc && [naviVc isKindOfClass:[CSNavigationController class]]) {
            [naviVc pushViewController:vc animated:YES];
        }
    }];
    
    [[CSRouter router] registerNativePath:@"main/pageList" jump:^(CSRouteRequest * _Nonnull request) {
        CSPageListViewController *vc = [[CSPageListViewController alloc] init];
        vc.routeRequest = request;
        UINavigationController *naviVc = (UINavigationController *)request.fromVC.navigationController;
        if (naviVc && [naviVc isKindOfClass:[CSNavigationController class]]) {
            [naviVc pushViewController:vc animated:YES];
        }
    }];
    
    [[CSRouter router] registerNativePath:@"main/pagePreview" jump:^(CSRouteRequest * _Nonnull request) {
        CSPagePreviewViewController *vc = [[CSPagePreviewViewController alloc] init];
        vc.routeRequest = request;
        UINavigationController *naviVc = (CSNavigationController *)request.fromVC.navigationController;
        if (naviVc && [naviVc isKindOfClass:[CSNavigationController class]]) {
            [naviVc pushViewController:vc animated:YES];
        }
    }];
}

- (void)doAction:(id<CSModuleActionProtocol>)action
{
    
}

@end
