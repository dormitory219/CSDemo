//
//  CSPageListViewController.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/21.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSPageListViewController.h"
#import "CSMediator.h"
#import "CSMainModuleAPI.h"

@interface CSPageListViewController ()

@end

@implementation CSPageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"pagelist";
    self.view.backgroundColor = [UIColor yellowColor];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toPagePreviewAction:(id)sender {
    [CSMediator jumpTo:MAIN_ROUTER_PAGEPREVIEW fromViewController:self];
}


@end
