//
//  CSRightModule.m
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/29.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#import "CSRightModule.h"
#import "CSRightViewController.h"

@implementation CSRightModule

CSMODULEREGISTER()

- (void)moduleInitalize
{
    [[CSRouter router] registerNativePath:@"right/main" view:^UIViewController *(CSRouteRequest *request) {
        CSRightViewController *vc = [[CSRightViewController alloc] init];
        vc.routeRequest = request;
        return vc;
    }];
}

- (void)doAction:(id<CSModuleActionProtocol>)action
{
    
}

@end
