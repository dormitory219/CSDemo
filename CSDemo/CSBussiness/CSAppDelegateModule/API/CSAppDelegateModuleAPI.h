//
//  API.h
//  CSDemo
//
//  Created by 韩小猫爱吃鱼 on 2018/9/8.
//  Copyright © 2018年 韩小猫爱吃鱼. All rights reserved.
//

#ifndef API_h
#define API_h

@protocol LoginInfoModelProtocol <NSObject>
@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *name;
@end

@protocol CSAppDelegateProxyProtocol <UIApplicationDelegate>

@end

#endif /* API_h */
