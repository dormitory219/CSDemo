//
//  CSAppDelegateModule.m
//  CSAppDelegateModule
//
//  Created by 余强 on 2018/5/10.
//  Copyright © 2018年 IntSig Information Co., Ltd. All rights reserved.
//

#import "CSAppDelegateModule.h"
#import "CSModuleManager.h"
#import "CSGroundAPI.h"
#import "CSMainModuleAPI.h"
#import "CSLeftModuleAPI.h"
#import "CSRightModuleAPI.h"
#import "CSAppDelegateModulePublicProtocol.h"
#import "CSAppDelegatePrivateServiceProtocol.h"
#import "CSAppDelegateServiceManager.h"
#import "MTStackViewController.h"
#import "ViewController.h"

@implementation CSAppDelegateModule

//CSMODULEREGISTER()

+(void)load
{
    CSAppDelegateModule *module = [[self alloc] init];
    [CSMediator registerService:@protocol(CSAppDelegateModuleProtocol) withImpl: module];
    [CSMediator registerService:@protocol(CSAppDelegateProxyProtocol) withImpl:module];
    [CSModuleManager registerModule:module];
}

- (void)moduleInitalize
{
    
}

#pragma mark CSAppDelegateProxyProtocol

- (BOOL)application:(UIApplication *) application didFinishLaunchingWithOptions:(NSDictionary *) launchOptions
{
    NSLog(@"CSAppDelegateModule: app did finish launching with option:%@",launchOptions);
    /*
     step1:
     创建app的window,内容viewController
     */
    UIWindow *window = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    /*
     step2:
     注册CSAppDelegateModule模块内需要加载的services服务列表
     */
    [[CSAppDelegateServiceManager shareManager] registerAllService];
    
    /*
     step3:
     将viewController,window注入到CSAppDelegateModule中，之后作为模块public访问
     */
    id<CSAppDelegateModuleProtocol>service = [CSMediator serviceForProtocol:@protocol(CSAppDelegateModuleProtocol)];
    service.window = window;
    
    
    /*
     step4:
     调用didFinishLaunchingWithOptions services服务
     */
    BOOL launch = [[CSAppDelegateServiceManager shareManager] application:application didFinishLaunchingWithOptions:launchOptions];
    if (!launch)
    {
        return NO;
    }
    
    /*
     step5:
     初始化可视窗口
     */
    
    MTStackViewController *stackViewController = [[MTStackViewController alloc] init];
    [stackViewController setAnimationDurationProportionalToPosition:YES];
    stackViewController.slideOffset = [UIScreen mainScreen].bounds.size.width *0.45;
    
    CSViewController* leftViewController = (CSViewController *)[CSMediator viewForURL:LEFT_ROUTER_MAIN];
    [stackViewController setLeftViewController:leftViewController];
    
     CSViewController* rightViewController = (CSViewController *)[CSMediator viewForURL:Right_ROUTER_MAIN];
    [stackViewController setRightViewController:rightViewController];
    stackViewController.rightViewControllerEnabled = YES;
    
    CSViewController *homeViewController = (CSViewController *)[CSMediator viewForURL:MAIN_ROUTER_HOME];
    UINavigationController *homeNavigationController = [[CSNavigationController alloc] initWithRootViewController:homeViewController];
    CGRect foldFrame = CGRectMake(0, 0, stackViewController.slideOffset,
                                  CGRectGetHeight(self.window.bounds));
    homeViewController.view.frame = foldFrame;
    [stackViewController setContentViewController:homeNavigationController];
    
    window.rootViewController = stackViewController;
    [window makeKeyAndVisible];
    
    
    /*
    ViewController *viewController = [[ViewController alloc] init];
    UINavigationController *navi =[[UINavigationController alloc] initWithRootViewController:viewController];
    window.rootViewController = navi;
     */
    [window makeKeyAndVisible];
    return YES;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"CSAppDelegateModule: application:%@, openURL: %@, sourceApplication: %@.", [application description], [url description], sourceApplication);
    BOOL open = [[CSAppDelegateServiceManager shareManager] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    return open;
}
#pragma clang diagnostic pop

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray * __nullable restorableObjects))restorationHandler
{
    NSLog(@"CSAppDelegateModule: application: %@.", [application description]);
    BOOL open = [[CSAppDelegateServiceManager shareManager] application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
    return open;
}

- (void)application:(UIApplication *) application performActionForShortcutItem:(UIApplicationShortcutItem *) shortcutItem completionHandler:(void (^)(BOOL succeeded)) completionHandler
{
    NSLog(@"CSAppDelegateModule: application: %@.", [application description]);
    //performActionForShortcutItem:(UIApplicationShortcutItem *) shortcutItem completionHandler进行优先级预处理
    [[CSAppDelegateServiceManager shareManager] application:application performActionForShortcutItem:shortcutItem completionHandler:completionHandler];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (void)application:(UIApplication *) application didReceiveRemoteNotification:(NSDictionary *) userInfo
{
    NSLog(@"CSAppDelegateModule: application didReceiveRemoteNotification:%@", userInfo);
    [[CSAppDelegateServiceManager shareManager] application:application didReceiveRemoteNotification:userInfo];
}
#pragma clang diagnostic pop

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken
{
    NSLog(@"CSAppDelegateModule: did register deviceToken");
    
    [[CSAppDelegateServiceManager shareManager] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)applicationDidBecomeActive:(UIApplication *) application
{
    NSLog(@"CSAppDelegateModule: Application: %@ did become active", [application description]);
    [[CSAppDelegateServiceManager shareManager] applicationDidBecomeActive:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"CSAppDelegateModule: Application: %@ will enter foreground", [application description]);
    //applicationWillEnterForeground:(UIApplication *)application进行优先级预处理
    //    [CSAppDelegateServiceManager shareManager].sortServices = @[
    //                  [CSAppDelegateDataBaseMigrationService class]
    //                                                                ];
    [[CSAppDelegateServiceManager shareManager] applicationWillEnterForeground:application];
}

- (void)applicationWillResignActive:(UIApplication *) application
{
    NSLog(@"CSAppDelegateModule: Application: %@ will resign active", [application description]);
    [[CSAppDelegateServiceManager shareManager] applicationWillResignActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"CSAppDelegateModule: application: %@ did enter background", [application description]);
    //applicationDidEnterBackground:(UIApplication *)application进行优先级预处理
    [CSAppDelegateServiceManager shareManager].sortServices = @[
                                                                ];
    [[CSAppDelegateServiceManager shareManager] applicationDidEnterBackground:application];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"CSAppDelegateModule: application: %@ will resign terminate", [application description]);
    [[CSAppDelegateServiceManager shareManager] applicationWillTerminate:application];
}


/*
 CSAppDelegateModule模块业务防crash机制,目前主要针对CSAppDelegateModule类：
 原则上是为防止测试环境case未被全部覆盖，导致线上crash而作用，所以debug环境下尽管crash让我修bug吧，线上就强行不挂
 使用一个桩对象CSAppDelegateModuleProxy来转发方法
 */

- (id)forwardingTargetForSelector:(SEL)aSelector
{
#ifdef DEBUG
    NSLog(@"CSAppDelegateModule did crash for selector:<<%@>>!!!!!",NSStringFromSelector(aSelector));
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"CSAppDelegateModule crash for selector:<<%@>>",NSStringFromSelector(aSelector)] userInfo:nil];
    return nil;
#else
    ISError(@"CSAppDelegateModule will crash for<< %@ >>,CSAppDelegateModuleProxy will handle this to prevent crash!!!!!",NSStringFromSelector(aSelector));
    return [CSAppDelegateModuleProxy ProxyforwardingTargetForSelector:aSelector];
#endif
}

#pragma mark CSAppDelegateServiceManagerProtocol

- (void)setWindow:(UIWindow *)window
{
    id <CSAppDelegateServiceManagerProtocol> service = [CSMediator serviceForProtocol:@protocol(CSAppDelegateServiceManagerProtocol)];
    [service setWindow:window];
}

- (UIWindow *)window
{
    id <CSAppDelegateServiceManagerProtocol> service = [CSMediator serviceForProtocol:@protocol(CSAppDelegateServiceManagerProtocol)];
    return service.window;
}

- (void)doAction:(id<CSModuleActionProtocol>)action
{
    
}

@end
