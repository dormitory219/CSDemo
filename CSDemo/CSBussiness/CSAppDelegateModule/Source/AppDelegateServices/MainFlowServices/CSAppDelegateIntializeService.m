//
//  CSAppDelegateIntializeService.m
//  CSAppDelegateModule
//
//  Created by 余强 on 2018/5/7.
//  Copyright © 2018年 IntSig Information Co., Ltd. All rights reserved.
//

#import "CSAppDelegateIntializeService.h"
#import "CSGroundAPI.h"

@interface CSAppDelegateIntializeService()<CSAppDelegateServiceProtocol>


@end

@implementation CSAppDelegateIntializeService
@synthesize priority = _priority;


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [CSMediator registerService:@protocol(CSAppDelegateIntializeServiceProtocol) withImpl:self];
    }
    return self;
}

- (void)initialize
{
    [CSModuleManager initializeAllModules];
    
    /*
     路由规则下发:
     key为模块，value为该模块下的匹配规则，naive路由实现为native:/module/，路由跳转时进行预处理，会根据这格式加上路由所在的页面（如login下的register）则完整路由路径为native://module/page，即native://login/register
     */
    NSDictionary *setting = @{
                              @"login":@"native://login/"
                              };
    
    [[CSDynamicRouter router] setupDynamicSetting:setting];
}


@end
