//
//  CSAppDelegatePrivateServiceProtocol.h
//  CSAppDelegateModule
//
//  Created by 余强 on 2018/8/8.
//  Copyright © 2018年 IntSig Information Co., Ltd. All rights reserved.
//

#ifndef CSAppDelegatePrivateServiceProtocol_h
#define CSAppDelegatePrivateServiceProtocol_h
#import <UIKit/UIKit.h>

@class CSAppServiceModel;
@protocol CSAppDelegateServiceProtocol <NSObject>

@optional

@property (nonatomic,assign) NSInteger priority;

- (BOOL)CSApplication:(UIApplication *) application didFinishLaunchingWithOptions:(NSDictionary *) launchOptions;

- (CSAppServiceModel *)CSApplication:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

- (CSAppServiceModel *)CSApplication:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray * restorableObjects))restorationHandler;

- (CSAppServiceModel *)CSApplication:(UIApplication *) application performActionForShortcutItem:(UIApplicationShortcutItem *) shortcutItem completionHandler:(void (^)(BOOL succeeded)) completionHandler;

- (void)CSApplication:(UIApplication *) application didReceiveRemoteNotification:(NSDictionary *) userInfo;

//- (void)CSApplication:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;

- (void)CSApplication:(UIApplication *) app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken;

- (void)CSApplicationDidBecomeActive:(UIApplication *) application;

- (void)CSApplicationWillResignActive:(UIApplication *) application;

- (void)CSApplicationWillTerminate:(UIApplication *)application;

- (BOOL)CSApplicationDidEnterBackground:(UIApplication *)application;

- (BOOL)CSApplicationWillEnterForeground:(UIApplication *)application;

@end

@protocol CSAppDelegateMainFlowServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateForceUpdateServiceProtocol <CSAppDelegateServiceProtocol>


@end

@protocol CSAppDelegateCrashCollectionServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateDataBaseMigrationServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateRecoveryServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDeleagateUploadAppListServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateApperanceServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateExtensionServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateIntializeServiceProtocol <CSAppDelegateServiceProtocol>

@optional
- (void)initialize;

@end


@protocol CSAppDelegateLaunchADServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateLaunchScreenServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateLogServiceProtocol <CSAppDelegateServiceProtocol>

@end


@protocol CSAppDelegateNotificationServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegatePasswordProtectServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateShortcutOperationServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegateViewControllerServiceProtocol <CSAppDelegateServiceProtocol>



@end


@protocol CSAppDelegateFinishStartupServiceProtocol <CSAppDelegateServiceProtocol>


@end

@protocol CSAppDelegateOpenPDFServiceProtocol <CSAppDelegateServiceProtocol>


@end


@protocol CSAppDelegatePrivateServiceProtocol <CSAppDelegateMainFlowServiceProtocol,CSAppDelegateIntializeServiceProtocol,CSAppDelegateForceUpdateServiceProtocol,CSAppDelegateCrashCollectionServiceProtocol,CSAppDelegateDataBaseMigrationServiceProtocol,CSAppDelegateRecoveryServiceProtocol,CSAppDeleagateUploadAppListServiceProtocol,CSAppDelegateApperanceServiceProtocol,CSAppDelegateExtensionServiceProtocol,CSAppDelegateLaunchADServiceProtocol,CSAppDelegateLaunchScreenServiceProtocol,CSAppDelegateLogServiceProtocol,CSAppDelegateNotificationServiceProtocol,CSAppDelegatePasswordProtectServiceProtocol,CSAppDelegateShortcutOperationServiceProtocol,CSAppDelegateViewControllerServiceProtocol,CSAppDelegateOpenPDFServiceProtocol>
@end



#endif /* CSAppDelegatePrivateServiceProtocol_h */
