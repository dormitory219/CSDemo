//
//  AppDelegate.h
//  CSAppDelegateModule
//
//  Created by 余强 on 2018/5/10.
//  Copyright © 2018年 余强. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

